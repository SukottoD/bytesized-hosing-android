package com.bytesizedhosting.api.android.darthcrap.v3.sync.service.impl;

import com.bytesizedhosting.api.android.darthcrap.v3.exceptions.ApiErrorException;
import com.bytesizedhosting.api.android.darthcrap.v3.model.external.ExternalError;
import com.bytesizedhosting.api.android.darthcrap.v3.model.external.ExternalUser;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.IBytesizedAPIService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class BytesizedAPIService implements IBytesizedAPIService {
    private static final String URL_FORMAT_STRING = "https://bytesized-hosting.com/api/v1/user.json?all=true&api_key=%1$s&_=%2$d";
    private static final int HTTP_STATUS_OK = 200;

    @Override
    public boolean isAPIKeyCorrect(String apiKey) throws ApiErrorException, IOException {
        try {
            this.fetch(apiKey);
            return true;
        }
        catch (ApiErrorException exApiError) {
            if (exApiError.isApiKeyError()) {
                return false;
            }
            else {
                throw exApiError;
            }
        }
    }

    @Override
    public ExternalUser fetch(String apiKey) throws ApiErrorException, IOException {
        String url = String.format(BytesizedAPIService.URL_FORMAT_STRING,apiKey,System.currentTimeMillis());

        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        String responseString = EntityUtils.toString(httpResponse.getEntity());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        JsonNode rootNode;
        try {
            rootNode = objectMapper.readTree(responseString);
        }
        catch (IOException exIO) {
            throw new IOException("Unable to read Bytesized API result.");
        }

        if (rootNode.has("error")) {
            try {
                ExternalError externalError = objectMapper.treeToValue(rootNode,ExternalError.class);
                throw new ApiErrorException(externalError);
            }
            catch (JsonProcessingException exJSON) {
                throw new IOException("Unable to convert response to " + ExternalError.class.getName(),exJSON);
            }
        }
        else {
            try {
                return objectMapper.treeToValue(rootNode, ExternalUser.class);
            }
            catch (JsonProcessingException exJSON) {
                throw new IOException("Unable to convert response to " + ExternalUser.class.getName(),exJSON);
            }
        }
    }
}
