package com.bytesizedhosting.api.android.darthcrap.v3.exceptions;

import com.bytesizedhosting.api.android.darthcrap.v3.model.external.ExternalError;

import java.util.Locale;

public class ApiErrorException extends Exception {
    private static final String NOT_FOUND_SNIPPET = "not found";

    private ExternalError externalError;
    private boolean isApiKeyError;

    public ApiErrorException(
        ExternalError externalError
    ) {
        super("The bytesized API endpoint returned an error.");
        this.externalError = externalError;
        this.isApiKeyError = false;
        if (externalError != null) {
            String errorMessage = externalError.getErrorMessage();
            if (errorMessage != null) {
                if (errorMessage.toLowerCase(Locale.ENGLISH).contains(ApiErrorException.NOT_FOUND_SNIPPET)) {
                    this.isApiKeyError = true;
                }
            }
        }
    }

    public ExternalError getExternalError() {
        return this.externalError;
    }

    public boolean isApiKeyError() {
        return this.isApiKeyError;
    }
}
