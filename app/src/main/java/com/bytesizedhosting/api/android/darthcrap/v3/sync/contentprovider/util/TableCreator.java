package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class TableCreator {
    private static class Column {
        private String columnName;
        private String columnType;

        public Column(
            String columnName,
            String columnType
        ) {
            this.columnName = columnName;
            this.columnType = columnType;
        }

        public String getColumnName() {
            return this.columnName;
        }

        public String getColumnType() {
            return this.columnType;
        }
    }

    private String tableName = null;
    private List<Column> Spec = new ArrayList<>();
    private List<String> additionalSQL = new ArrayList<>();

    public TableCreator(
        String tableName
    ) {
        this.tableName = tableName;
    }

    public TableCreator addColumn(
        String columnName,
        String columnType
    ) {
        this.Spec.add(new Column(columnName,columnType));
        return this;
    }

    public TableCreator addAdditionalSQL(
        String additionalSQL
    ) {
        this.additionalSQL.add(additionalSQL);
        return this;
    }

    public TableCreator addForeignKey(
        String thisTableColumnName,
        String otherTableName,
        String otherTableColumnName
    ) {
        return this.addAdditionalSQL("FOREIGN KEY (" + thisTableColumnName + ") REFERENCES " + otherTableName + "(" + otherTableColumnName + ") ON UPDATE RESTRICT ON DELETE CASCADE");
    }

    public TableCreator addUniqueConstraint(
        String... columnNames
    ) {
        if (columnNames != null && columnNames.length > 0) {
            StringBuilder additionalSqlForUniqueConstraint = new StringBuilder();
            additionalSqlForUniqueConstraint
                .append("UNIQUE (");
            for (int columnNameIndex=0; columnNameIndex<columnNames.length; columnNameIndex++) {
                if (columnNameIndex > 0) {
                    additionalSqlForUniqueConstraint
                        .append(", ");
                }
                additionalSqlForUniqueConstraint
                    .append(columnNames[columnNameIndex]);
            }
            additionalSqlForUniqueConstraint
                .append(")");
            return this.addAdditionalSQL(additionalSqlForUniqueConstraint.toString());
        }
        else {
            return this;
        }
    }

    public String build() {
        if (this.tableName == null) {
            throw new IllegalStateException("The table name must not be NULL.");
        }
        else if (this.Spec.isEmpty()) {
            throw new IllegalStateException("There are no column names.");
        }
        else {
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder
                .append("CREATE TABLE ")
                .append(this.tableName)
                .append(" (");
            boolean first = true;
            for (Column column : Spec) {
                if (first) {
                    first = false;
                }
                else {
                    sqlBuilder
                        .append(", ");
                }
                sqlBuilder
                    .append(column.getColumnName())
                    .append(" ")
                    .append(column.getColumnType());
            }
            for (String additionalSQLEntry : additionalSQL) {
                sqlBuilder
                    .append(", ")
                    .append(additionalSQLEntry);
            }
            sqlBuilder
                .append(");");
            return sqlBuilder.toString();
        }
    }

    public void execute(
        SQLiteDatabase db
    ) {
        db.execSQL(this.build());
    }
}
