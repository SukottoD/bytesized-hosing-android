package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;

import android.app.Activity;

public abstract class AbstractFragmentInteractionBaseFragment<FragmentInteractionListenerType> extends AbstractBaseFragment {
    private FragmentInteractionListenerType fragmentInteractionListener;
    private Class<FragmentInteractionListenerType> fragmentInteractionListenerClass;

    public AbstractFragmentInteractionBaseFragment(
        String fragmentMenuType,
        Class<FragmentInteractionListenerType> fragmentInteractionListenerClass,
        String... requiredArgumentKeys
    ) {
        super(fragmentMenuType,requiredArgumentKeys);
        this.fragmentInteractionListenerClass = fragmentInteractionListenerClass;
    }

    @Override
    public void onAttach(
        Activity activity
    ) {
        super.onAttach(activity);
        this.fragmentInteractionListener = this.castActivity(activity,this.fragmentInteractionListenerClass);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.fragmentInteractionListener = null;
    }

    public FragmentInteractionListenerType getFragmentInteractionListener() {
        return this.getActivityDependantField(this.fragmentInteractionListener);
    }
}
