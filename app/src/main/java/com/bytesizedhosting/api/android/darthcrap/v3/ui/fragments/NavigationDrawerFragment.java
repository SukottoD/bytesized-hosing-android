package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bytesizedhosting.api.android.darthcrap.v3.BuildConfig;
import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.UserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.ContentResolverContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Account;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.LogUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.NumericConstants;
import com.joanzapata.android.iconify.Iconify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NavigationDrawerFragment extends AbstractBaseFragment {
    public static final String FRAGMENT_MENU_TYPE = "NAVIGATION_DRAWER";
    private static final String PREF_KEY_HAS_USER_LEARNED_NAVIGATION_DRAWER = "has_user_learned_navigation_drawer";

    private static interface IMenuItemAction {
        public Fragment createNewFragment();
    }

    private static class Item {
        private String fragmentMenuType;
        private Iconify.IconValue icon;
        private String text;
        private IMenuItemAction action;
        private List<SubItem> subItems;
        private final boolean hasSubItems;

        public Item(
            String fragmentMenuType,
            Iconify.IconValue icon,
            String text,
            IMenuItemAction action
        ) {
            this.init(fragmentMenuType, icon, text);
            if (action == null) {
                throw new IllegalArgumentException("The action cannot be NULL when there are no sub items.");
            }
            else {
                this.hasSubItems = false;
                this.action = action;
            }
        }

        public Item(
            String fragmentMenuType,
            Iconify.IconValue icon,
            String text,
            List<SubItem> subItems
        ) {
            this.init(fragmentMenuType, icon, text);
            if (subItems == null) {
                throw new IllegalArgumentException("The sub items list cannot be NULL when there is no action.");
            }
            else if (subItems.isEmpty()) {
                throw new IllegalArgumentException("The sub items list cannot be empty when there is no action.");
            }
            else {
                this.hasSubItems = true;
                this.subItems = new ArrayList<>(subItems);
            }
        }

        private void init(
            String fragmentMenuType,
            Iconify.IconValue icon,
            String text
        ) {
            if (fragmentMenuType == null) {
                throw new IllegalArgumentException("The fragment menu type must not be NULL.");
            }
            else if (icon == null) {
                throw new IllegalArgumentException("The icon must not be NULL.");
            }
            else if (text == null) {
                throw new IllegalArgumentException("The text must not be NULL.");
            }
            else {
                this.fragmentMenuType = fragmentMenuType;
                this.icon = icon;
                this.text = text;
            }
        }

        public String getFragmentMenuType() {
            return this.fragmentMenuType;
        }

        public String getText() {
            return this.text;
        }

        public Iconify.IconValue getIcon() {
            return this.icon;
        }

        public boolean hasSubItems() {
            return this.hasSubItems;
        }

        public List<SubItem> getSubItems() {
            if (this.hasSubItems) {
                return Collections.unmodifiableList(this.subItems);
            }
            else {
                throw new IllegalStateException("This item does not have sub items.");
            }
        }

        public IMenuItemAction getAction() {
            if (this.hasSubItems) {
                throw new IllegalStateException("This item does not have an action.");
            }
            else {
                return this.action;
            }
        }
    }

    private static class SubItem {
        private final Object fragmentMenuParameter;
        private final String text;
        private final IMenuItemAction action;

        public SubItem(
            Object fragmentMenuParameter,
            String text,
            IMenuItemAction action
        ) {
            if (text == null) {
                throw new IllegalArgumentException("The text must not be NULL.");
            }
            else if (action == null) {
                throw new IllegalArgumentException("The action must not be NULL.");
            }
            else {
                this.fragmentMenuParameter = fragmentMenuParameter;
                this.text = text;
                this.action = action;
            }
        }

        public Object getFragmentMenuParameter() {
            return this.fragmentMenuParameter;
        }

        public String getText() {
            return this.text;
        }

        public IMenuItemAction getAction() {
            return this.action;
        }
    }

    private static class NavigationDrawerAdapter extends BaseAdapter {
        private static enum ItemType {
            PRIMARY,
            SECONDARY;

            public static ItemType[] values = ItemType.values();
        }

        private static class InternalItem {
            private ItemType itemType;
            private Iconify.IconValue icon;
            private String text;
            private IMenuItemAction action;

            private InternalItem(
                ItemType itemType,
                Iconify.IconValue icon,
                String text,
                IMenuItemAction action
            ) {
                this.itemType = itemType;
                this.icon = icon;
                this.text = text;
                this.action = action;
            }

            public ItemType getItemType() {
                return this.itemType;
            }

            public Iconify.IconValue getIcon() {
                return this.icon;
            }

            public String getText() {
                return this.text;
            }

            public IMenuItemAction getAction() {
                return this.action;
            }
        }

        private static class ViewHolder {
            private TextView textViewForIcon;
            private TextView textViewForText;
        }

        private List<InternalItem> items = new ArrayList<>();
        private Map<String,Map<Object,Integer>> menuTypeAndMenuParameterItemIndexMap = new HashMap<>();
        private LayoutInflater layoutInflater = null;

        public NavigationDrawerAdapter(
            Context context
        ) {
            this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public Integer getIndexByFragmentMenuTypeAndFragmentMenuParameter(
            String fragmentMenuType,
            Object fragmentMenuParameter

        ) {
            Map<Object,Integer> menuParameterItemIndexMap = this.menuTypeAndMenuParameterItemIndexMap.get(fragmentMenuType);
            if (menuParameterItemIndexMap == null) {
                return null;
            }
            else {
                if (menuParameterItemIndexMap.size() == 1) {
                    return menuParameterItemIndexMap.values().iterator().next();
                }
                else {
                    return menuParameterItemIndexMap.get(fragmentMenuParameter);
                }
            }
        }

        public void updateItems(
            List<Item> items
        ) {
            if (items == null) {
                throw new IllegalArgumentException("The items list must not be NULL.");
            }
            else {
                Map<String,Map<Object,Integer>> menuTypeAndMenuParameterItemIndexMapNew = new HashMap<>();
                List<InternalItem> itemsNew = new ArrayList<>();
                for (Item item : items) {
                    if (item == null) {
                        throw new IllegalArgumentException("No item can be NULL.");
                    }
                    else if (menuTypeAndMenuParameterItemIndexMapNew.containsKey(item.getFragmentMenuType())) {
                        throw new IllegalArgumentException("Duplicate item fragment menu type: " + item.getFragmentMenuType());
                    }
                    else {
                        Map<Object,Integer> menuParameterItemIndexMapNew = new HashMap<>();
                        IMenuItemAction menuItemAction;
                        if (!item.hasSubItems()) {
                            menuItemAction = item.getAction();
                            if (menuItemAction == null) {
                                throw new IllegalArgumentException("The action for item fragment menu type '" + item.getFragmentMenuType() + "' must not be NULL.");
                            }
                        }
                        else {
                            menuItemAction = null;
                        }
                        int primaryItemIndex = itemsNew.size();
                        itemsNew.add(
                            new InternalItem(
                                ItemType.PRIMARY,
                                item.getIcon(),
                                item.getText(),
                                menuItemAction
                            )
                        );
                        if (item.hasSubItems()) {
                            for (SubItem subItem : item.getSubItems()) {
                                if (subItem == null) {
                                    throw new IllegalArgumentException("No sub item can be NULL.");
                                }
                                else if (menuParameterItemIndexMapNew.containsKey(subItem.getFragmentMenuParameter())) {
                                    throw new IllegalArgumentException("Duplicate sub item fragment menu parameter for item fragment menu type '" + item.getFragmentMenuType() + "': " + subItem.getFragmentMenuParameter());
                                }
                                else {
                                    menuItemAction = subItem.getAction();
                                    if (menuItemAction == null) {
                                        throw new IllegalArgumentException("The action for sub item fragment menu parameter '" + subItem.getFragmentMenuParameter() + "' for item fragment menu type '" + item.getFragmentMenuType() + "' must not be NULL.");
                                    }
                                    int secondaryItemIndex = itemsNew.size();
                                    itemsNew.add(
                                        new InternalItem(
                                            ItemType.SECONDARY,
                                            null,
                                            subItem.getText(),
                                            menuItemAction
                                        )
                                    );
                                    menuParameterItemIndexMapNew.put(subItem.getFragmentMenuParameter(),secondaryItemIndex);
                                }
                            }
                        }
                        else {
                            menuParameterItemIndexMapNew.put(null,primaryItemIndex);
                        }
                        menuTypeAndMenuParameterItemIndexMapNew.put(item.getFragmentMenuType(),menuParameterItemIndexMapNew);
                    }
                }
                this.items = itemsNew;
                this.menuTypeAndMenuParameterItemIndexMap = menuTypeAndMenuParameterItemIndexMapNew;
                super.notifyDataSetChanged();
            }
        }

        @Override
        public int getCount() {
            return this.items.size();
        }

        @Override
        public InternalItem getItem(
            int position
        ) {
            return this.items.get(position);
        }

        public IMenuItemAction getAction(
            int position
        ) {
            return this.getItem(position).getAction();
        }

        @Override
        public long getItemId(
            int position
        ) {
            return position;
        }

        @Override
        public View getView(
            int position,
            View convertView,
            ViewGroup parent
        ) {
            ViewHolder viewHolder;
            InternalItem item = this.getItem(position);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                switch (item.getItemType()) {
                    case PRIMARY:
                        convertView = this.layoutInflater.inflate(R.layout.view_navigation_drawer_list_item_primary,parent,false);
                        viewHolder.textViewForIcon = (TextView)convertView.findViewById(R.id.lblItemIcon);
                        viewHolder.textViewForText = (TextView)convertView.findViewById(R.id.lblItemText);
                        break;
                    case SECONDARY:
                        convertView = this.layoutInflater.inflate(R.layout.view_navigation_drawer_list_item_secondary,parent,false);
                        viewHolder.textViewForText = (TextView)convertView.findViewById(R.id.lblItemText);
                        break;
                }
                convertView.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder)convertView.getTag();
            }
            if (viewHolder.textViewForIcon != null) {
                Iconify.setIcon(
                    viewHolder.textViewForIcon,
                    item.getIcon()
                );
            }
            viewHolder.textViewForText.setText(item.getText());
            return convertView;
        }

        @Override
        public int getViewTypeCount() {
            return ItemType.values.length;
        }

        @Override
        public int getItemViewType(
            int position
        ) {
            return this.getItem(position).getItemType().ordinal();
        }

        @Override
        public boolean isEnabled(
            int position
        ) {
            return this.getItem(position).getAction() != null;
        }
    }

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayoutViewInParentActivity;
    private View navigationDrawerView;
    private ListView navigationDrawerListView;
    private View fragmentViewInParentActivity;
    private boolean hasUserLearnedNavigationDrawer;
    private NavigationDrawerAdapter listAdapter;
    private boolean userSignedIn = false;

    public NavigationDrawerFragment() {
        super(NavigationDrawerFragment.FRAGMENT_MENU_TYPE);
    }

    private void setSelectedItem(
        int index
    ) {
        LogUtils.d(this, "Set Selected Item To Index " + index);
        if (this.navigationDrawerListView != null) {
            this.navigationDrawerListView.setItemChecked(
                index,
                true
            );
        }
    }

    public void clearSelectedItem() {
        this.setSelectedItem(-1);
    }

    public void updateSelection(Fragment newFragment) {
        LogUtils.d(this,"Updating Selection");
        if (newFragment instanceof IHasFragmentMenuDetails) {
            IHasFragmentMenuDetails fragmentMenuDetails = (IHasFragmentMenuDetails)newFragment;
            Integer index = this.listAdapter.getIndexByFragmentMenuTypeAndFragmentMenuParameter(fragmentMenuDetails.getFragmentMenuType(),fragmentMenuDetails.getFragmentMenuParameter());
            if (index == null) {
                this.clearSelectedItem();
            }
            else {
                this.setSelectedItem(index);
            }
        }
        else {
            this.clearSelectedItem();
        }
    }

    @Override
    public void onCreate(
        Bundle savedInstanceState
    ) {
        super.onCreate(
            savedInstanceState
        );

        LogUtils.d(this,"Creating Fragment");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
            this.getActivity()
        );

        this.hasUserLearnedNavigationDrawer = sharedPreferences.getBoolean(
            NavigationDrawerFragment.PREF_KEY_HAS_USER_LEARNED_NAVIGATION_DRAWER,
            false
        );
    }

    @Override
    public void onActivityCreated(
        Bundle savedInstanceState
    ) {
        super.onActivityCreated(
            savedInstanceState
        );

        LogUtils.d(this,"Activity Created");

        this.setHasOptionsMenu(
            true
        );
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public View onCreateView(
        LayoutInflater layoutInflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        LogUtils.d(this,"Creating View");

        this.navigationDrawerView = layoutInflater.inflate(
            R.layout.fragment_navigation_drawer,
            container,
            false
        );

        TextView lblVersion = (TextView)this.navigationDrawerView.findViewById(R.id.lblVersion);
        lblVersion.setText(this.getString(R.string.navigation_drawer_version, BuildConfig.VERSION_NAME));

        this.navigationDrawerListView = (ListView)this.navigationDrawerView.findViewById(
            R.id.lstNavigation
        );
        this.navigationDrawerListView.setOnItemClickListener((parentView, view, itemIndex, id) -> NavigationDrawerFragment.this.selectItem(itemIndex));

        this.listAdapter = new NavigationDrawerAdapter(this.getActivity());

        navigationDrawerListView.setAdapter(
            this.listAdapter
        );

        return this.navigationDrawerView;
    }

    public void setUp(
        int activityFragmentId,
        DrawerLayout drawerLayoutViewInParentActivity
    ) {
        LogUtils.d(this,"Setting Up");
        this.fragmentViewInParentActivity = this.getActivity().findViewById(
            activityFragmentId
        );
        this.drawerLayoutViewInParentActivity = drawerLayoutViewInParentActivity;

        this.drawerLayoutViewInParentActivity.setDrawerShadow(
            R.drawable.navigation_drawer_shadow,
            GravityCompat.START
        );

        ActionBar actionBar = this.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        this.actionBarDrawerToggle = new ActionBarDrawerToggle(
            this.getActivity(),
            this.drawerLayoutViewInParentActivity,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ) {
            @Override
            public void onDrawerClosed(
                View drawerView
            ) {
                super.onDrawerClosed(
                    drawerView
                );
                if (!NavigationDrawerFragment.this.isAdded()) {
                    return;
                }
                NavigationDrawerFragment.this.getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(
                View drawerView
            ) {
                super.onDrawerOpened(
                    drawerView
                );
                if (!NavigationDrawerFragment.this.isAdded()) {
                    return;
                }

                if (!NavigationDrawerFragment.this.hasUserLearnedNavigationDrawer) {
                    NavigationDrawerFragment.this.hasUserLearnedNavigationDrawer = true;
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                        NavigationDrawerFragment.this.getActivity()
                    );
                    sharedPreferences
                        .edit()
                        .putBoolean(
                            NavigationDrawerFragment.PREF_KEY_HAS_USER_LEARNED_NAVIGATION_DRAWER,
                            true
                        )
                        .apply();
                }

                NavigationDrawerFragment.this.getActivity().invalidateOptionsMenu();
            }
        };

        this.teachUserNavigationDrawerIfRequired();

        this.drawerLayoutViewInParentActivity.post(NavigationDrawerFragment.this.actionBarDrawerToggle::syncState);

        this.drawerLayoutViewInParentActivity.setDrawerListener(
            this.actionBarDrawerToggle
        );
    }

    private void selectItem(
        int position
    ) {
        LogUtils.d(this,"Selecting Item At Position " + position);
        if (this.drawerLayoutViewInParentActivity != null) {
            this.drawerLayoutViewInParentActivity.closeDrawer(
                this.fragmentViewInParentActivity
            );
        }
        this.getCoordinationActivity().displayNewFragment(
            true,
            this.listAdapter.getAction(position).createNewFragment()
        );
    }

    @Override
    public void onConfigurationChanged(
        Configuration newConfiguration
    ) {
        super.onConfigurationChanged(
            newConfiguration
        );
        LogUtils.d(this,"Configuration Changed");
        this.actionBarDrawerToggle.onConfigurationChanged(
            newConfiguration
        );
    }

    private ActionBar getActionBar() {
        return this.getActivity().getActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(
        MenuItem menuItem
    ) {
        LogUtils.d(this,"Options Item Selected");
        if (menuItem != null && menuItem.getItemId() == android.R.id.home && this.actionBarDrawerToggle.isDrawerIndicatorEnabled() && !this.isNavigationDrawerLocked()) {
            if (this.actionBarDrawerToggle.onOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void lockNavigationDrawer() {
        LogUtils.d(this,"Locking Navigation Drawer.");
        this.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        this.drawerLayoutViewInParentActivity.closeDrawers();
    }

    public void unlockNavigationDrawer() {
        LogUtils.d(this,"Unlocking Navigation Drawer.");
        this.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void setDrawerLockMode(
        int lockMode
    ) {
        this.drawerLayoutViewInParentActivity.setDrawerLockMode(Gravity.LEFT, lockMode);
        this.drawerLayoutViewInParentActivity.setDrawerLockMode(Gravity.RIGHT, lockMode);
    }

    public boolean isNavigationDrawerLocked() {
        return this.isNavigationDrawerLocked(Gravity.LEFT) || this.isNavigationDrawerLocked(Gravity.RIGHT);
    }

    private boolean isNavigationDrawerLocked(
        int edgeGravity
    ) {
        int state = this.drawerLayoutViewInParentActivity.getDrawerLockMode(edgeGravity);
        return (state == DrawerLayout.LOCK_MODE_LOCKED_CLOSED || state == DrawerLayout.LOCK_MODE_LOCKED_OPEN);
    }

    public void updateForUser(
        long userId
    ) {
        LogUtils.d(this,"Updating For User With User Id " + userId);
        Context context = this.getContext();
        IContentProviderWrapper contextWrapper = ContentResolverContentProviderWrapper.create(context);
        UserDao userDao = new UserDao(contextWrapper);
        User user = userDao.selectById(userId);

        GenericUtils.applyGravatarToImageView(this.navigationDrawerView.findViewById(R.id.imgGravatar),user.getGravatarId(),GenericUtils.GRAVATAR_RADIUS_SHAPE_CIRCLE);
        ((TextView)this.navigationDrawerView.findViewById(R.id.lblUsername)).setText(user.getUsername());
        ((TextView)this.navigationDrawerView.findViewById(R.id.lblBalance)).setText(this.getString(R.string.navigation_drawer_balance,BigDecimal.valueOf(user.getBalanceInCents()).divide(BigDecimal.valueOf(NumericConstants.CENTS_IN_EURO)).doubleValue()));

        this.listAdapter.updateItems(this.buildMenuItemList(context,contextWrapper,userId));

        this.userSignedIn = true;
        this.teachUserNavigationDrawerIfRequired();
    }

    private void teachUserNavigationDrawerIfRequired() {
        if (!this.hasUserLearnedNavigationDrawer && this.userSignedIn) {
            this.drawerLayoutViewInParentActivity.openDrawer(
                this.fragmentViewInParentActivity
            );
        }
    }

    @SuppressLint("StringFormatMatches")
    private List<Item> buildMenuItemList(
        Context context,
        IContentProviderWrapper contextWrapper,
        long userId
    ) {
        LogUtils.d(this,"Building Menu Item List.");
        List<Item> items = new ArrayList<>();

        items.add(
            new Item(
                DashboardFragment.FRAGMENT_MENU_TYPE,
                Iconify.IconValue.fa_laptop,
                this.getString(R.string.section_dashboard_menu_item),
                () -> DashboardFragment.create(userId)
            )
        );

        AccountDao accountDao = new AccountDao(contextWrapper);
        List<Account> accounts = accountDao.selectAllWithUserId(userId);

        if (!accounts.isEmpty()) {
            List<SubItem> subItems = new ArrayList<>();
            for (Account account : accounts) {
                subItems.add(
                    new SubItem(
                        account.getId(),
                        this.getString(R.string.section_account_details_menu_sub_item,account.getServerName()),
                        () -> AccountDetailsFragment.create(account.getId())
                    )
                );
            }
            items.add(
                new Item(
                    AccountDetailsFragment.FRAGMENT_MENU_TYPE,
                    Iconify.IconValue.fa_signal,
                    this.getString(R.string.section_account_details_menu_item),
                    subItems
                )
            );
        }

        items.add(
            new Item(
                SettingsFragment.FRAGMENT_MENU_TYPE,
                Iconify.IconValue.fa_cog,
                this.getString(R.string.section_settings_menu_item),
                () -> SettingsFragment.create(userId)
            )
        );

        return items;
    }

    @Override
    public Object getFragmentMenuParameter() {
        return null;
    }
}
