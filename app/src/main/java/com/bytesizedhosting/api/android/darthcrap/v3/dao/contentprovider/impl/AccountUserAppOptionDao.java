package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl;

import android.content.ContentValues;
import android.database.Cursor;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountUserAppOptionDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.FieldMapping;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserAppOption;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;

import java.util.Arrays;
import java.util.List;

public class AccountUserAppOptionDao extends AbstractContentProviderDao<AccountUserAppOption> implements IAccountUserAppOptionDao {
    public AccountUserAppOptionDao(
        IContentProviderWrapper contentProviderWrapper
    ) {
        super(
            contentProviderWrapper,
            AccountUserAppOption::new,
            BytesizedContract.AccountUserAppOptionEntitySpec.PATH_ENTITY,
            Arrays.<FieldMapping<AccountUserAppOption>>asList(
                FieldMapping.create(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ID                 , AccountUserAppOption::getId              , AccountUserAppOption::setId              , Cursor::getLong  , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID, AccountUserAppOption::getAccountUserAppId, AccountUserAppOption::setAccountUserAppId, Cursor::getLong  , ContentValues::put)
            ),
            Arrays.<FieldMapping<AccountUserAppOption>>asList(
                FieldMapping.create(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_NAME               , AccountUserAppOption::getName            , AccountUserAppOption::setName            , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_VALUE              , AccountUserAppOption::getValue           , AccountUserAppOption::setValue           , Cursor::getString, ContentValues::put)
            )
        );
    }

    @Override
    public List<AccountUserAppOption> selectAllWithAccountUserAppId(
        long accountUserAppId
    ) {
        return this.selectAllWithParentId(
            BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID,
            accountUserAppId
        );
    }

    @Override
    public List<AccountUserAppOption> selectWithAccountUserAppIdAndCriteria(
        long accountUserAppId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectWithParentIdAndCriteria(
            BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID,
            accountUserAppId,
            whereClause,
            whereParameters,
            sortClause
        );
    }

    @Override
    public AccountUserAppOption selectSingleWithAccountUserAppIdAndCriteria(
        long accountUserAppId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectSingleWithParentIdAndCriteria(
            BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID,
            accountUserAppId,
            whereClause,
            whereParameters,
            sortClause
        );
    }
}
