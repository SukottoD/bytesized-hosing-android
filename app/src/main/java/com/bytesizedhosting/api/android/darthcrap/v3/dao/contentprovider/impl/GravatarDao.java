package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl;

import android.content.ContentValues;
import android.database.Cursor;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IGravatarDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.FieldMapping;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Gravatar;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;

import java.util.Arrays;
import java.util.List;

public class GravatarDao extends AbstractContentProviderDao<Gravatar> implements IGravatarDao {
    public GravatarDao(
        IContentProviderWrapper contentProviderWrapper
    ) {
        super(
            contentProviderWrapper,
            Gravatar::new,
            BytesizedContract.GravatarEntitySpec.PATH_ENTITY,
            Arrays.<FieldMapping<Gravatar>>asList(
                FieldMapping.create(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_ID             , Gravatar::getId            , Gravatar::setId            , Cursor::getLong  , ContentValues::put)
            ),
            Arrays.<FieldMapping<Gravatar>>asList(
                FieldMapping.create(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_GRAVATAR_DIGEST, Gravatar::getGravatarDigest, Gravatar::setGravatarDigest, Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_LAST_FETCHED_AT, Gravatar::getLastFetchedAt , Gravatar::setLastFetchedAt                                        ),
                FieldMapping.create(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_IMAGE_DATA     , Gravatar::getImageData     , Gravatar::setImageData     , Cursor::getBlob  , ContentValues::put)
            )
        );
    }

    @Override
    public List<Gravatar> selectAllWithoutRetrievingImageData() {
        String[] projection = this.getProjection();
        int foundImageDataCount = 0;
        for (int index=0; index<projection.length; index++) {
            if (projection[index].equals(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_IMAGE_DATA)) {
                projection[index] = "NULL AS " + projection[index];
                foundImageDataCount++;
            }
        }
        if (foundImageDataCount == 1) {
            return this.createEntityListFromCursor(
                this.getContentProviderWrapper().query(
                    this.getPathUriForList(),
                    projection,
                    null,
                    null,
                    null
                )
            );
        }
        else {
            throw new IllegalStateException("Unable to calculate correct projection.");
        }
    }

    @Override
    public List<Gravatar> selectAllThatHaveNoImageData() {
        return this.selectWithCriteria(
            BytesizedContract.GravatarEntitySpec.COLUMN_NAME_GRAVATAR_DIGEST + " IS NULL",
            null,
            null
        );
    }

    @Override
    public List<Gravatar> selectAllThatHaveNeverBeenFetched() {
        return this.selectWithCriteria(
            BytesizedContract.GravatarEntitySpec.COLUMN_NAME_LAST_FETCHED_AT + " IS NULL AND " + BytesizedContract.GravatarEntitySpec.COLUMN_NAME_GRAVATAR_DIGEST + " IS NULL",
            null,
            null
        );
    }
}
