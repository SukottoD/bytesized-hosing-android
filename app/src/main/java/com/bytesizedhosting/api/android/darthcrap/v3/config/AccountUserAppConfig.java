package com.bytesizedhosting.api.android.darthcrap.v3.config;

import android.content.Context;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Account;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserApp;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.ui.activities.ICoordinationActivity;
import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class AccountUserAppConfig {
    public static interface IValueRetriever {
        public String getValue(Context context, User user, Account account, AccountUserApp accountUserApp, Map<String,String> accountUserAppOptions);
    }

    private static class StringResourceValueRetriever implements IValueRetriever {
        private int stringId;

        private StringResourceValueRetriever(int stringId) {
            this.stringId = stringId;
        }

        @Override
        public String getValue(Context context, User user, Account account, AccountUserApp accountUserApp, Map<String, String> accountUserAppOptions) {
            return context.getResources().getString(this.stringId);
        }
    }

    private static class StringValueRetriever implements IValueRetriever {
        private String string;

        private StringValueRetriever(String string) {
            this.string = string;
        }

        @Override
        public String getValue(Context context, User user, Account account, AccountUserApp accountUserApp, Map<String, String> accountUserAppOptions) {
            return this.string;
        }
    }

    private static class OptionValueRetriever implements IValueRetriever {
        private String key;

        private OptionValueRetriever(String key) {
            this.key = key.toLowerCase(Locale.ENGLISH);
        }

        @Override
        public String getValue(Context context, User user, Account account, AccountUserApp accountUserApp, Map<String, String> accountUserAppOptions) {
            return accountUserAppOptions.get(this.key);
        }
    }

    private static class UsernameValueRetriever implements IValueRetriever {
        private boolean isLowerCase;

        public UsernameValueRetriever() {
            this(true);
        }

        public UsernameValueRetriever(
            boolean isLowerCase
        ) {
            this.isLowerCase = isLowerCase;
        }
        @Override
        public String getValue(Context context, User user, Account account, AccountUserApp accountUserApp, Map<String, String> accountUserAppOptions) {
            String username = user.getUsername();
            if (this.isLowerCase) {
                username = username.toLowerCase(Locale.ENGLISH);
            }
            return username;
        }
    }

    public static interface IAction {
        public void run(Context context, ICoordinationActivity coordinationActivity, User user, Account account, AccountUserApp accountUserApp, Map<String,String> accountUserAppOptions);
    }

    private static class AccountLinkAction implements IAction {
        // is HTTPS?
        private static class Builder {
            private boolean useHttps = false;
            private boolean prefixUsername = false;
            private String portKey = null;
            private String path = null;

            public Builder withHttps() {
                return this.withHttps(true);
            }

            public Builder withHttps(boolean useHttps) {
                this.useHttps = useHttps;
                return this;
            }

            public Builder withUsernamePrefix() {
                return this.withUsernamePrefix(true);
            }

            public Builder withUsernamePrefix(boolean prefixUsername) {
                this.prefixUsername = prefixUsername;
                return this;
            }

            public Builder withPortKey(String portKey) {
                this.portKey = portKey;
                return this;
            }

            public Builder withPath(String path) {
                this.path = path;
                return this;
            }

            public AccountLinkAction build() {
                return new AccountLinkAction(this.useHttps,this.prefixUsername,this.portKey,this.path);
            }
        }

        private boolean useHttps;
        private boolean prefixUsername;
        private String portKey;
        private String path;

        private AccountLinkAction(boolean useHttps, boolean prefixUsername, String portKey, String path) {
            this.useHttps = useHttps;
            this.prefixUsername = prefixUsername;
            this.portKey = portKey;
            this.path = path;
        }

        public boolean getUseHttps() {
            return useHttps;
        }

        public boolean getPrefixUsername() {
            return prefixUsername;
        }

        public String getPortKey() {
            return portKey;
        }

        public String getPath() {
            return path;
        }

        @Override
        public void run(Context context, ICoordinationActivity coordinationActivity, User user, Account account, AccountUserApp accountUserApp, Map<String, String> accountUserAppOptions) {
            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.append("http");
            if (this.useHttps) {
                urlBuilder.append("s");
            }
            urlBuilder.append("://");
            if (this.prefixUsername) {
                urlBuilder.append(user.getUsername().toLowerCase(Locale.ENGLISH)).append(".");
            }
            urlBuilder.append(account.getServerName()).append(".bytesized-hosting.com");
            if (this.portKey != null) {
                urlBuilder.append(":").append(accountUserAppOptions.get(this.portKey));
            }
            if (this.path != null) {
                urlBuilder.append(this.path);
            }
            String url = urlBuilder.toString();
            GenericUtils.openLink(context,url);
        }
    }

    public static class EntryValue {
        private IValueRetriever valueRetriever;
        private IAction action;
        private Set<String> requiredPropertyKeys;

        private EntryValue(
            IValueRetriever valueRetriever,
            String... requiredPropertyKeys
        ) {
            this(
                valueRetriever,
                null,
                requiredPropertyKeys
            );
        }

        private EntryValue(
            IValueRetriever valueRetriever,
            IAction action,
            String... requiredPropertyKeys
        ) {
            this.valueRetriever = valueRetriever;
            this.action = action;
            this.requiredPropertyKeys = new HashSet<>(Arrays.asList(requiredPropertyKeys));
        }

        private EntryValue(
            String valuePropertyKey
        ) {
            this(
                valuePropertyKey,
                null
            );
        }

        private EntryValue(
            String valuePropertyKey,
            IAction action
        ) {
            this(
                new OptionValueRetriever(valuePropertyKey),
                action,
                valuePropertyKey
            );
        }

        public IValueRetriever getValueRetriever() {
            return this.valueRetriever;
        }

        public IAction getAction() {
            return this.action;
        }

        public Set<String> getRequiredPropertyKeys() {
            return Collections.unmodifiableSet(this.requiredPropertyKeys);
        }
    }

    public static class Entry {
        public static final boolean DEFAULT_REQUIRE_ALL_PROPERTY_KEYS = false;

        private int keyStringId;
        private boolean requireAllPropertyKeys;
        private List<EntryValue> entryValues;

        private Entry(
            int keyStringId,
            EntryValue... entryValues
        ) {
            this(
                keyStringId,
                Entry.DEFAULT_REQUIRE_ALL_PROPERTY_KEYS,
                entryValues
            );
        }

        private Entry(
            int keyStringId,
            boolean requireAllPropertyKeys,
            EntryValue... entryValues
        ) {
            this.keyStringId = keyStringId;
            this.requireAllPropertyKeys = requireAllPropertyKeys;
            this.entryValues = Arrays.asList(entryValues);
        }

        public int getKeyStringId() {
            return this.keyStringId;
        }

        public boolean getRequireAllPropertyKeys() {
            return this.requireAllPropertyKeys;
        }

        public List<EntryValue> getEntryValues() {
            return Collections.unmodifiableList(this.entryValues);
        }

        private static Entry constructSingleValueRawEntry(
            int keyStringId,
            String propertyKey
        ) {
            return new Entry(
                keyStringId,
                new EntryValue(
                    propertyKey
                )
            );
        }

        private static Entry constructWebUiPortEntry(
            String propertyKey
        ) {
            return Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_web_ui_port,
                propertyKey
            );
        }

        private static Entry constructUsernameEntryLower() {
            return Entry.constructUsernameEntry(true);
        }

        private static Entry constructUsernameEntryNormal() {
            return Entry.constructUsernameEntry(false);
        }

        private static Entry constructUsernameEntry(
            boolean isLowerCase
        ) {
            return new Entry(
                R.string.section_account_details_app_panel_entry_key_username,
                new EntryValue(
                    new UsernameValueRetriever(isLowerCase)
                )
            );
        }

        private static Entry constructPasswordEntry(
            String value
        ) {
            return Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_password,
                value
            );
        }

        private static Entry constructWebUiLinkEntry(
            AccountLinkAction.Builder builder
        ) {
            AccountLinkAction accountLinkAction = builder.build();
            EntryValue entryValue;
            IValueRetriever valueRetriever = new StringResourceValueRetriever(R.string.section_account_details_app_panel_entry_value_link_open_web_interface);
            if (accountLinkAction.getPortKey() != null) {
                entryValue = new EntryValue(
                    valueRetriever,
                    accountLinkAction,
                    accountLinkAction.getPortKey()
                );
            }
            else {
                entryValue = new EntryValue(
                    valueRetriever,
                    accountLinkAction
                );
            }
            return new Entry(
                R.string.section_account_details_app_panel_entry_key_link,
                entryValue
            );
        }

        private static Entry constructWebUiLinkMultiEntry(
            AccountLinkAction.Builder baseBuilder,
            String httpPortPropertyKey,
            String httpsPortPropertyKey
        ) {
            return new Entry(
                R.string.section_account_details_app_panel_entry_key_links,
                new EntryValue(
                    new StringResourceValueRetriever(R.string.section_account_details_app_panel_entry_value_link_open_http_web_interface),
                    baseBuilder.withPortKey(httpPortPropertyKey).withHttps(false).build(),
                    httpPortPropertyKey
                ),
                new EntryValue(
                    new StringResourceValueRetriever(R.string.section_account_details_app_panel_entry_value_link_open_https_web_interface),
                    baseBuilder.withPortKey(httpsPortPropertyKey).withHttps(true).build(),
                    httpsPortPropertyKey
                )
            );
        }
    }

    private static final AccountUserAppConfig constructStandard(
        String serverId,
        int titleStringId,
        int iconDrawableId,
        Entry... entries
    ) {
        return new AccountUserAppConfig(
            serverId,
            titleStringId,
            iconDrawableId,
            entries
        );
    }

    private static final AccountUserAppConfig constructWithPortBasedWebUiLink(
        String serverId,
        int titleStringId,
        int iconDrawableId,
        String webUiPropertyKey,
        Entry... entries
    ) {
        return AccountUserAppConfig.constructWithPortBasedWebUiLink(
            serverId,
            titleStringId,
            iconDrawableId,
            webUiPropertyKey,
            new AccountLinkAction.Builder(),
            entries
        );
    }

    private static final AccountUserAppConfig constructWithPortBasedWebUiLink(
        String serverId,
        int titleStringId,
        int iconDrawableId,
        String portPropertyKey,
        AccountLinkAction.Builder builderBase,
        Entry... entries
    ) {
        Entry[] newEntries = new Entry[entries.length+2];
        newEntries[0] = Entry.constructWebUiPortEntry(portPropertyKey);
        for (int index=0; index<entries.length; index++) {
            newEntries[index+1] = entries[index];
        }
        newEntries[entries.length+1] = Entry.constructWebUiLinkEntry(builderBase.withPortKey(portPropertyKey));
        return new AccountUserAppConfig(
            serverId,
            titleStringId,
            iconDrawableId,
            newEntries
        );
    }

    private static final AccountUserAppConfig constructWithPortBasedMultiWebUiLink(
        String serverId,
        int titleStringId,
        int iconDrawableId,
        String httpPropertyKey,
        String httpsPropertyKey,
        Entry... entries
    ) {
        return AccountUserAppConfig.constructWithPortBasedMultiWebUiLink(
            serverId,
            titleStringId,
            iconDrawableId,
            httpPropertyKey,
            httpsPropertyKey,
            new AccountLinkAction.Builder(),
            entries
        );
    }

    private static final AccountUserAppConfig constructWithPortBasedMultiWebUiLink(
        String serverId,
        int titleStringId,
        int iconDrawableId,
        String httpPropertyKey,
        String httpsPropertyKey,
        AccountLinkAction.Builder builderBase,
        Entry... entries
    ) {
        Entry[] newEntries = new Entry[entries.length+3];
        newEntries[0] = Entry.constructSingleValueRawEntry(
            R.string.section_account_details_app_panel_entry_key_http_web_ui_port,
            httpPropertyKey
        );
        newEntries[1] = Entry.constructSingleValueRawEntry(
            R.string.section_account_details_app_panel_entry_key_https_web_ui_port,
            httpsPropertyKey
        );

        for (int index=0; index<entries.length; index++) {
            newEntries[index+2] = entries[index];
        }
        newEntries[entries.length+2] = Entry.constructWebUiLinkMultiEntry(builderBase,httpPropertyKey,httpsPropertyKey);
        return new AccountUserAppConfig(
            serverId,
            titleStringId,
            iconDrawableId,
            newEntries
        );
    }

    private static final AccountUserAppConfig[] accountUserAppConfigArray = {
        AccountUserAppConfig.constructWithPortBasedWebUiLink(
            "53f8fc166675732e77000000",
            R.string.section_account_details_app_panel_app_btsync_title,
            R.drawable.section_account_details_app_icon_btsync,
            "webui_port",
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructWithPortBasedWebUiLink(
            "5457b8f3667573032f000000",
            R.string.section_account_details_app_panel_app_couchpotato_title,
            R.drawable.section_account_details_app_icon_couchpotato,
            "web_port",
            Entry.constructUsernameEntryNormal()
        ),
        AccountUserAppConfig.constructWithPortBasedWebUiLink(
            "53a427dd6675733722000000",
            R.string.section_account_details_app_panel_app_deluge_title,
            R.drawable.section_account_details_app_icon_deluge,
            "web_port",
            Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_daemon_port,
                "daemon_port"
            ),
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructStandard(
            "53f5c5646675732820000000",
            R.string.section_account_details_app_panel_app_flexget_title,
            R.drawable.section_account_details_app_icon_flexget
        ),
        AccountUserAppConfig.constructStandard(
            "54f48a106e322e1aa2000000",
            R.string.section_account_details_app_panel_app_murmur_title,
            R.drawable.section_account_details_app_icon_murmur,
            Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_server_port,
                "port"
            ),
            Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_ice_port,
                "ice_port"
            ),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructStandard(
            "53a427dd6675733722010000",
            R.string.section_account_details_app_panel_app_plex_title,
            R.drawable.section_account_details_app_icon_plex,
            Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_port,
                "port"
            ),
            Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_allowed_ip,
                "ip"
            ),
            Entry.constructWebUiLinkEntry(
                new AccountLinkAction.Builder().withPortKey("port").withPath("/web")
            )
        ),
        AccountUserAppConfig.constructStandard(
            "53b52d7a6675736af1000000",
            R.string.section_account_details_app_panel_app_pydio_title,
            R.drawable.section_account_details_app_icon_pydio,
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password"),
            Entry.constructWebUiLinkEntry(
                new AccountLinkAction.Builder().withUsernamePrefix().withPath("/explorer")
            )
        ),
        AccountUserAppConfig.constructWithPortBasedWebUiLink(
            "53f9c38c6675733031000000",
            R.string.section_account_details_app_panel_app_pyload_title,
            R.drawable.section_account_details_app_icon_pyload,
            "webui_port",
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructStandard(
            "53f506476675732f5daa0200",
            R.string.section_account_details_app_panel_app_rtorrent_title,
            R.drawable.section_account_details_app_icon_rtorrent,
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password"),
            Entry.constructWebUiLinkEntry(
                new AccountLinkAction.Builder().withUsernamePrefix().withPath("/rutorrent")
            )
        ),
        AccountUserAppConfig.constructWithPortBasedMultiWebUiLink(
            "53f225c96675731373000000",
            R.string.section_account_details_app_panel_app_sabnzbd_title,
            R.drawable.section_account_details_app_icon_sabnzbd,
            "port",
            "https_port",
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructWithPortBasedWebUiLink(
            "53ff1242667573038e000000",
            R.string.section_account_details_app_panel_app_sickbeard_title,
            R.drawable.section_account_details_app_icon_sickbeard,
            "web_port",
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructWithPortBasedWebUiLink(
            "54be6b466e322e36b5960000",
            R.string.section_account_details_app_panel_app_sickrage_title,
            R.drawable.section_account_details_app_icon_sickrage,
            "web_port",
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructWithPortBasedMultiWebUiLink(
            "54c79c706e322e70e4a80100",
            R.string.section_account_details_app_panel_app_subsonic_title,
            R.drawable.section_account_details_app_icon_subsonic,
            "web_port",
            "ssl_port",
            new Entry(
                R.string.section_account_details_app_panel_entry_key_username,
                new EntryValue(
                    new StringValueRetriever("admin")
                )
            ),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructWithPortBasedWebUiLink(
            "5507f7386e322e3dff990000",
            R.string.section_account_details_app_panel_app_syncthing_title,
            R.drawable.section_account_details_app_icon_syncthing,
            "web_port",
            Entry.constructUsernameEntryLower(),
            Entry.constructPasswordEntry("password")
        ),
        AccountUserAppConfig.constructStandard(
            "53f32241667573189a000000",
            R.string.section_account_details_app_panel_app_vnc_title,
            R.drawable.section_account_details_app_icon_vnc,
            Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_port,
                "port"
            ),
            Entry.constructSingleValueRawEntry(
                R.string.section_account_details_app_panel_entry_key_offset,
                "offset"
            ),
            Entry.constructPasswordEntry("password")
        )
    };

    private BigInteger serverId;
    private int titleStringId;
    private int iconDrawableId;
    private List<Entry> entries;

    private AccountUserAppConfig(
        String serverId,
        int titleStringId,
        int iconDrawableId,
        Entry... entries
    ) {
        this(AccountUserAppConfig.convertServerId(serverId),titleStringId,iconDrawableId,entries);
    }

    private AccountUserAppConfig(
        BigInteger serverId,
        int titleStringId,
        int iconDrawableId,
        Entry... entries
    ) {
        this.serverId = serverId;
        this.titleStringId = titleStringId;
        this.iconDrawableId = iconDrawableId;
        this.entries = Arrays.asList(entries);
    }

    public BigInteger getServerId() {
        return this.serverId;
    }

    public int getTitleStringId() {
        return this.titleStringId;
    }

    public int getIconDrawableId() {
        return this.iconDrawableId;
    }

    public List<Entry> getEntries() {
        return Collections.unmodifiableList(this.entries);
    }

    public static AccountUserAppConfig getAccountUserAppConfig(String serverId) {
        BigInteger serverIdBigInt = AccountUserAppConfig.convertServerId(serverId);
        for (AccountUserAppConfig accountUserAppConfig : AccountUserAppConfig.accountUserAppConfigArray) {
            if (serverIdBigInt.equals(accountUserAppConfig.getServerId())) {
                return accountUserAppConfig;
            }
        }
        return null;
    }

    public static BigInteger convertServerId(String serverId) {
        return new BigInteger(serverId,16);
    }
}
