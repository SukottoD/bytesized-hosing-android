package com.bytesizedhosting.api.android.darthcrap.v3.util;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class UriUtils {
    private static final Pattern SLASH_PATTERN = Pattern.compile("/");

    private UriUtils() {}

    public static String[] getSegments(
        String path
    ) {
        return UriUtils.SLASH_PATTERN.split(path);
    }

    private static List<Integer> getInputSegmentIndexes(
        String[] pathSegments
    ) {
        List<Integer> inputSegmentIndexes = new ArrayList<>();
        for (int index=0; index<pathSegments.length; index++) {
            String pathSegment = pathSegments[index];
            if (pathSegment.equals("*") || pathSegment.equals("#")) {
                inputSegmentIndexes.add(index);
            }
        }
        return inputSegmentIndexes;
    }

    public static List<Integer> getInputSegmentIndexes(
        String path
    ) {
        return UriUtils.getInputSegmentIndexes(UriUtils.getSegments(path));
    }

    public static String constructPath(
        String path,
        long[] ids
    ) {
        if (ids == null) {
            ids = new long[0];
        }
        String[] segments = UriUtils.getSegments(path);
        List<Integer> inputSegments = UriUtils.getInputSegmentIndexes(segments);
        if (inputSegments.size() != ids.length) {
            throw new IllegalArgumentException("There is a different number of IDs in the path to IDs available.");
        }
        else {
            int inputSegmentIndex = 0;
            StringBuilder pathBuilder = new StringBuilder();
            for (int segmentIndex=0; segmentIndex<segments.length; segmentIndex++) {
                if (segmentIndex > 0) {
                    pathBuilder
                        .append('/');
                }
                if (inputSegmentIndex < inputSegments.size() && inputSegments.get(inputSegmentIndex) == segmentIndex) {
                    pathBuilder
                        .append(ids[inputSegmentIndex]);
                    inputSegmentIndex++;
                }
                else {
                    pathBuilder
                        .append(segments[segmentIndex]);
                }
            }
            return pathBuilder.toString();
        }
    }

    public static Uri constructContentBaseUri(
        String authority
    ) {
        return Uri.parse("content://"+authority);
    }

    public static Uri constructContentUri(
        Uri contentBase,
        String path,
        long... ids
    ) {
        return Uri.withAppendedPath(contentBase, UriUtils.constructPath(path, ids));
    }

    public static Uri constructContentUri(
        String authority,
        String path,
        long... ids
    ) {
        return UriUtils.constructContentUri(UriUtils.constructContentBaseUri(authority),path,ids);
    }

    public static void checkIfPathForEntityIsValid(
        String pathForEntity
    ) {
        String[] pathForEntitySegments = UriUtils.getSegments(pathForEntity);
        if (pathForEntitySegments[pathForEntitySegments.length-1].trim().isEmpty()) {
            throw new IllegalArgumentException("The final path segment must not be empty.");
        }
        else {
            List<Integer> inputSegmentIndexes = UriUtils.getInputSegmentIndexes(pathForEntitySegments);
            if (!inputSegmentIndexes.isEmpty()) {
                throw new IllegalArgumentException("The path must not contain any id segments.");
            }
        }
    }
}
