package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserAppOption;

import java.util.List;

public interface IAccountUserAppOptionDao extends IBasicDao<AccountUserAppOption> {
    List<AccountUserAppOption> selectAllWithAccountUserAppId(
        long accountUserAppId
    );

    List<AccountUserAppOption> selectWithAccountUserAppIdAndCriteria(
        long accountUserAppId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );

    public AccountUserAppOption selectSingleWithAccountUserAppIdAndCriteria(
        long accountUserAppId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );
}
