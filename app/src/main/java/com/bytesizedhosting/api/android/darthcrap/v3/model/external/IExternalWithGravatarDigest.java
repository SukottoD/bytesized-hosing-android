package com.bytesizedhosting.api.android.darthcrap.v3.model.external;

public interface IExternalWithGravatarDigest {
    public String getGravatarDigest();
}
