package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl;

import android.content.ContentValues;
import android.database.Cursor;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IUserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.FieldMapping;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;

import java.util.Arrays;

public class UserDao extends AbstractContentProviderDao<User> implements IUserDao  {
    public UserDao(
        IContentProviderWrapper contentProviderWrapper
    ) {
        super(
            contentProviderWrapper,
            User::new,
            BytesizedContract.UserEntitySpec.PATH_ENTITY,
            Arrays.<FieldMapping<User>>asList(
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_ID                       , User::getId                   , User::setId                   , Cursor::getLong  , ContentValues::put)
            ),
            Arrays.<FieldMapping<User>>asList(
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_API_KEY                  , User::getApiKey               , User::setApiKey               , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_CREATED_AT               , User::getCreatedAt            , User::setCreatedAt                                            ),
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_EMAIL_ADDRESS            , User::getEmailAddress         , User::setEmailAddress         , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_USERNAME                 , User::getUsername             , User::setUsername             , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_COUNTRY                  , User::getCountry              , User::setCountry              , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_BALANCE_IN_CENTS         , User::getBalanceInCents       , User::setBalanceInCents       , Cursor::getInt   , ContentValues::put),
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_GRAVATAR_ID              , User::getGravatarId           , User::setGravatarId           , Cursor::getLong  , ContentValues::put),
                FieldMapping.create(BytesizedContract.UserEntitySpec.COLUMN_NAME_LAST_API_DATA_UPDATE_TIME, User::getLastApiDataUpdateTime, User::setLastApiDataUpdateTime                                       )
            )
        );
    }

    @Override
    public User selectByApiKey(String apiKey) {
        return this.selectSingleWithCriteria(
            BytesizedContract.UserEntitySpec.COLUMN_NAME_API_KEY + " = ?",
            new String[] {apiKey},
            null
        );
    }
}
