package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserApp;

import java.util.List;

public interface IAccountUserAppDao extends IBasicDao<AccountUserApp> {
    List<AccountUserApp> selectAllWithAccountId(
        long accountId
    );

    List<AccountUserApp> selectWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );

    public AccountUserApp selectSingleWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );
}
