package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

public interface IWithId {
    public Long getId();
    public void setId(Long id);
}
