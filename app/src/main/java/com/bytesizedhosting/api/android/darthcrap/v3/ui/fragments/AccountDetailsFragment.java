package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.config.AccountUserAppConfig;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountQuotaHistoryDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountUserAppDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountUserAppOptionDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IUserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountQuotaHistoryDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountUserAppDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountUserAppOptionDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.UserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.ContentResolverContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Account;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountQuotaHistory;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserApp;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserAppOption;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserAppState;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.ui.controls.TitledPanelView;
import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.LogUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.size.SizeString;
import com.bytesizedhosting.api.android.darthcrap.v3.util.size.SizeStringMode;
import com.bytesizedhosting.api.android.darthcrap.v3.util.size.SizeUnitSystem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class AccountDetailsFragment extends AbstractBaseFragment {
    public static final String ARGUMENT_EXTRA_ACCOUNT_ID = "account_id";
    public static final String FRAGMENT_MENU_TYPE = "AccountDetails";

    public static AccountDetailsFragment create(
        long accountId
    ) {
        AccountDetailsFragment fragment = new AccountDetailsFragment();
        Bundle arguments = new Bundle();
        arguments.putLong(AccountDetailsFragment.ARGUMENT_EXTRA_ACCOUNT_ID,accountId);
        fragment.setArguments(arguments);
        return fragment;
    }

    public AccountDetailsFragment() {
        super(
            AccountDetailsFragment.FRAGMENT_MENU_TYPE,
            AccountDetailsFragment.ARGUMENT_EXTRA_ACCOUNT_ID
        );
    }

    @Override
    public View onCreateView(
        LayoutInflater layoutInflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        LogUtils.d(this, "Creating View");
        View accountDetailsView = layoutInflater.inflate(
            R.layout.fragment_account_details,
            container,
            false
        );

        Context context = this.getContext();
        IContentProviderWrapper contentProviderWrapper = ContentResolverContentProviderWrapper.create(this);
        IUserDao userDao = new UserDao(contentProviderWrapper);
        IAccountDao accountDao = new AccountDao(contentProviderWrapper);
        long accountId = this.getAccountId();
        Account account = accountDao.selectById(accountId);
        User user = userDao.selectById(account.getUserId());

        this.displayAccount(context,contentProviderWrapper,accountDetailsView,account);
        this.displayApps(context,contentProviderWrapper,accountDetailsView,user,account);

        return accountDetailsView;
    }

    private void displayAccount(Context context, IContentProviderWrapper contentProviderWrapper, View accountDetailsView, Account account) {
        IAccountQuotaHistoryDao accountQuotaHistoryDao = new AccountQuotaHistoryDao(contentProviderWrapper);
        AccountQuotaHistory accountQuotaHistory = accountQuotaHistoryDao.selectLatestQuotaForAccount(account.getId());

        this.displayAccountUsage(context, accountDetailsView, account, accountQuotaHistory);
        this.displayAccountDetails(context, contentProviderWrapper, accountDetailsView, account, accountQuotaHistory);
    }

    private void displayAccountUsage(Context context, View accountDetailsView, Account account, AccountQuotaHistory accountQuotaHistory) {
        ((TextView) accountDetailsView.findViewById(R.id.lblTitle)).setText(context.getString(R.string.section_account_details_title));
        ((TextView)accountDetailsView.findViewById(R.id.lblSubTitle)).setText(context.getString(R.string.section_account_details_subtitle,account.getServerName(), GenericUtils.getAccountTypeString(context,account.getAccountType())));

        TextView lblDiskUsage = (TextView)accountDetailsView.findViewById(R.id.lblDiskUsage);
        ProgressBar pbDiskUsage = (ProgressBar)accountDetailsView.findViewById(R.id.pbDiskUsage);
        TextView lblDiskUsageBurstWarning = (TextView)accountDetailsView.findViewById(R.id.lblDiskUsageBurstWarning);

        TextView lblBandwidthUsage = (TextView)accountDetailsView.findViewById(R.id.lblBandwidthUsage);
        ProgressBar pbBandwidthUsage = (ProgressBar)accountDetailsView.findViewById(R.id.pbBandwidthUsage);
        TextView lblBandwidthUsageBoostInfo = (TextView)accountDetailsView.findViewById(R.id.lblBandwidthUsageBoostInfo);

        this.updateUsage(context, SizeUnitSystem.SI_1000 , accountQuotaHistory.getStorageQuotaInBytes()  , account.getTotalStorageInBytes()  , 0L                                , lblDiskUsage     , pbDiskUsage     , R.string.section_account_details_disk_usage_without_limit     , R.string.section_account_details_disk_usage_with_limit     );
        this.updateUsage(context, SizeUnitSystem.IEC_1024, accountQuotaHistory.getBandwidthQuotaInBytes(), account.getTotalBandwidthInBytes(), account.getBoostBandwidthInBytes(), lblBandwidthUsage, pbBandwidthUsage, R.string.section_account_details_bandwidth_usage_without_limit, R.string.section_account_details_bandwidth_usage_with_limit);

        lblDiskUsageBurstWarning.setVisibility((accountQuotaHistory.getStorageQuotaInBytes() != null && account.getTotalStorageInBytes() != null && accountQuotaHistory.getStorageQuotaInBytes() > account.getTotalStorageInBytes())?View.VISIBLE:View.GONE);

        if (account.getBoostBandwidthInBytes() == null || account.getBoostBandwidthInBytes() == 0) {
            lblBandwidthUsageBoostInfo.setVisibility(View.GONE);
        }
        else {
            SizeString boostBandwidth = SizeString.createFromBytes(context,account.getBoostBandwidthInBytes(),SizeUnitSystem.IEC_1024,SizeStringMode.ABBREVIATION_FORCE_SINGULAR);
            lblBandwidthUsageBoostInfo.setText(context.getResources().getString(R.string.section_account_details_bandwidth_usage_boost_info,boostBandwidth.getAmount(),boostBandwidth.getSizeUnitString()));
            lblBandwidthUsageBoostInfo.setVisibility(View.VISIBLE);
        }
    }

    private void displayAccountDetails(Context context, IContentProviderWrapper contentProviderWrapper, View accountDetailsView, Account account, AccountQuotaHistory accountQuotaHistory) {
        //TODO
    }

    private void displayApps(Context context, IContentProviderWrapper contentProviderWrapper, View accountDetailsView, User user, Account account) {
        ViewGroup appContainerViewGroup = (ViewGroup)accountDetailsView.findViewById(R.id.llMain);
        IAccountUserAppDao accountUserAppDao = new AccountUserAppDao(contentProviderWrapper);
        IAccountUserAppOptionDao accountUserAppOptionDao = new AccountUserAppOptionDao(contentProviderWrapper);
        List<AccountUserApp> accountUserApps = accountUserAppDao.selectAllWithAccountId(account.getId());
        Collections.sort(accountUserApps,(accountUserApp1,accountUserApp2)->accountUserApp1.getAppName().compareToIgnoreCase(accountUserApp2.getAppName()));
        for (AccountUserApp accountUserApp : accountUserApps) {
            if (AccountUserAppState.INSTALLED.equals(accountUserApp.getAccountUserAppState())) {
                this.displayApp(context, accountUserAppOptionDao, appContainerViewGroup, user, account, accountUserApp);
            }
        }
    }

    private void displayApp(Context context, IAccountUserAppOptionDao accountUserAppOptionDao, ViewGroup appContainerViewGroup, User user, Account account, AccountUserApp accountUserApp) {
        TitledPanelView titledPanelView = TitledPanelView.create(context, appContainerViewGroup, false);
        LinearLayout.LayoutParams layoutParams = GenericUtils.setLinearLayoutParams(titledPanelView);
        layoutParams.setMargins(0,context.getResources().getDimensionPixelSize(R.dimen.global_panel_spacing),0,0);

        AccountUserAppConfig accountUserAppConfig = AccountUserAppConfig.getAccountUserAppConfig(accountUserApp.getAppServerId());
        if (accountUserAppConfig == null) {
            this.displayUnknownApp(context, titledPanelView, accountUserApp);
        }
        else {
            List<AccountUserAppOption> accountUserAppOptionsList = accountUserAppOptionDao.selectAllWithAccountUserAppId(accountUserApp.getId());
            Map<String,String> accountUserAppOptionsMap = new HashMap<>();
            for (AccountUserAppOption accountUserAppOption : accountUserAppOptionsList) {
                accountUserAppOptionsMap.put(accountUserAppOption.getName().toLowerCase(Locale.ENGLISH),accountUserAppOption.getValue());
            }
            this.displayKnownApp(context, titledPanelView, user, account, accountUserApp, accountUserAppConfig, accountUserAppOptionsMap);
        }

        appContainerViewGroup.addView(titledPanelView);
    }

    private void displayUnknownApp(Context context, TitledPanelView titledPanelView, AccountUserApp accountUserApp) {
        titledPanelView.setPanelTitle(context.getResources().getString(R.string.section_account_details_app_panel_title_unknown_app,accountUserApp.getAppName()));
        TextView textView = new TextView(context);
        textView.setText(context.getResources().getString(R.string.section_account_details_app_panel_no_app_config, accountUserApp.getAppName()));
        GenericUtils.setLinearLayoutParams(textView);
        titledPanelView.addView(textView);
    }

    private void displayKnownApp(Context context, TitledPanelView titledPanelView, User user, Account account, AccountUserApp accountUserApp, AccountUserAppConfig accountUserAppConfig, Map<String,String> accountUserAppOptions) {
        String title = context.getResources().getString(accountUserAppConfig.getTitleStringId());
        titledPanelView.setPanelTitle(context.getResources().getString(R.string.section_account_details_app_panel_title_known_app,title));
        titledPanelView.setPanelTitleIcon(accountUserAppConfig.getIconDrawableId());
        List<TableRow> tableRows = new ArrayList<>();
        Set<String> propertiesAvailable = new HashSet<>(accountUserAppOptions.keySet());
        Set<String> propertiesUsed = new HashSet<>();
        Set<String> propertiesMissing = new HashSet<>();

        for (AccountUserAppConfig.Entry entry : accountUserAppConfig.getEntries()) {
            List<AccountUserAppConfig.EntryValue> entryValues = entry.getEntryValues();
            List<View> viewsToDisplay = new ArrayList<>();

            for (AccountUserAppConfig.EntryValue entryValue : entry.getEntryValues()) {
                boolean canDisplay = true;
                for (String requiredKey : entryValue.getRequiredPropertyKeys()) {
                    if (propertiesAvailable.contains(requiredKey)) {
                        propertiesUsed.add(requiredKey);
                    }
                    else {
                        canDisplay = false;
                        propertiesMissing.add(requiredKey);
                    }
                }
                if (canDisplay) {
                    viewsToDisplay.add(this.createAppOptionValueView(context, user, account, accountUserApp, accountUserAppOptions, entryValue));
                }
            }

            if (!viewsToDisplay.isEmpty() && !(entry.getRequireAllPropertyKeys() && viewsToDisplay.size() == entryValues.size())) {
                TableRow tableRow = new TableRow(context);
                tableRow.setGravity(Gravity.CENTER_VERTICAL);
                TextView key = new TextView(new ContextThemeWrapper(context,R.style.Bytesized_Section_AccountDetails_AppPanel_Entry_Text_Key));
                key.setText(context.getResources().getString(entry.getKeyStringId()));
                tableRow.addView(key);
                LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                for (View view : viewsToDisplay) {
                    GenericUtils.setLinearLayoutParams(view);
                    linearLayout.addView(view);
                }
                tableRow.addView(linearLayout);
                tableRows.add(tableRow);
            }
        }

        boolean allKnownPropertiesMatched = propertiesMissing.isEmpty();
        boolean allAvailablePropertiesUsed = (propertiesUsed.size() == propertiesAvailable.size());

        String textViewText = null;
        if (!allAvailablePropertiesUsed || !allKnownPropertiesMatched) {
            textViewText = context.getResources().getString(R.string.section_account_details_app_panel_not_all_settings_could_be_displayed, accountUserApp.getAppName());
        }
        else if (tableRows.isEmpty()) {
            textViewText = context.getResources().getString(R.string.section_account_details_app_panel_no_settings, accountUserApp.getAppName());
        }
        if (textViewText != null) {
            TextView textView = new TextView(context);
            textView.setText(textViewText);
            LinearLayout.LayoutParams layoutParams = GenericUtils.setLinearLayoutParams(textView);
            int bottomMargin;
            if (!tableRows.isEmpty()) {
                bottomMargin = context.getResources().getDimensionPixelSize(R.dimen.control_titled_panel_contents_text_padding);
            }
            else {
                bottomMargin = 0;
            }
            layoutParams.setMargins(0,0,0,bottomMargin);
            titledPanelView.addView(textView);
        }

        if (!tableRows.isEmpty()) {
            TableLayout tableLayout = new TableLayout(context);
            GenericUtils.setLinearLayoutParams(tableLayout);
            for (TableRow tableRow : tableRows) {
                tableLayout.addView(tableRow);
            }
            titledPanelView.addView(tableLayout);
        }
    }

    private View createAppOptionValueView(Context context, User user, Account account, AccountUserApp accountUserApp, Map<String,String> accountUserAppOptions, AccountUserAppConfig.EntryValue entryValue) {
        AccountUserAppConfig.IAction action = entryValue.getAction();
        TextView textView;
        if (action == null) {
            textView = new TextView(new ContextThemeWrapper(context,R.style.Bytesized_Section_AccountDetails_AppPanel_Entry_Text_Value));
        }
        else {
            textView = new Button(new ContextThemeWrapper(context,R.style.Bytesized_Section_AccountDetails_AppPanel_Entry_Button));
            textView.setOnClickListener((button) -> entryValue.getAction().run(context,this.getCoordinationActivity(),user,account,accountUserApp,accountUserAppOptions));
        }
        textView.setText(entryValue.getValueRetriever().getValue(context,user,account,accountUserApp,accountUserAppOptions));
        return textView;
    }

    private void updateUsage(Context context, SizeUnitSystem sizeUnitSystem, Long currentUsage, Long totalAllowance, Long totalAllowanceExtra, TextView usageTextView, ProgressBar usageProgressBar, int withoutLimitStringResourceId, int withLimitStringResourceId) {
        long currentUsageActual;
        Long totalAllowanceActual;
        if (currentUsage == null) {
            currentUsageActual = 0L;
        }
        else {
            currentUsageActual = currentUsage;
        }
        if (totalAllowance != null || totalAllowanceExtra != null) {
            totalAllowanceActual = 0L;
            if (totalAllowance != null) {
                totalAllowanceActual += totalAllowance;
            }
            if (totalAllowanceExtra != null) {
                totalAllowanceActual += totalAllowanceExtra;
            }
        }
        else {
            totalAllowanceActual = null;
        }

        SizeString currentUsageSize = SizeString.createFromBytes(context, currentUsageActual, sizeUnitSystem, SizeStringMode.ABBREVIATION_FORCE_SINGULAR);

        if (totalAllowanceActual == null) {
            usageTextView.setText(context.getResources().getString(withoutLimitStringResourceId, currentUsageSize.getAmount(), currentUsageSize.getSizeUnitString()));
            usageProgressBar.setVisibility(View.GONE);
        }
        else {
            SizeString totalAllowanceSize = SizeString.createFromBytes(context, totalAllowanceActual, sizeUnitSystem, SizeStringMode.ABBREVIATION_FORCE_SINGULAR);
            double usagePercentage = (((double)currentUsageActual)/((double)totalAllowanceActual))*100;
            if (usagePercentage > 100) {
                usagePercentage = 100;
            }
            long divisorForProgress = 1;
            long valueForProgress = (currentUsageActual>totalAllowanceActual)?currentUsageActual:totalAllowanceActual;
            while ((valueForProgress / divisorForProgress) > Integer.MAX_VALUE) {
                divisorForProgress *= sizeUnitSystem.getBase();
            }
            int currentProgress = (int)(currentUsageActual/divisorForProgress);
            int maxProgress = (int)(totalAllowanceActual/divisorForProgress);
            if (currentProgress > maxProgress) {
                currentProgress = maxProgress;
            }
            usageTextView.setText(context.getResources().getString(withLimitStringResourceId,usagePercentage,currentUsageSize.getAmount(),currentUsageSize.getSizeUnitString(),totalAllowanceSize.getAmount(),totalAllowanceSize.getSizeUnitString()));
            usageProgressBar.setMax(maxProgress);
            usageProgressBar.setProgress(currentProgress);
            usageProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private long getAccountId() {
        return this.getArguments().getLong(AccountDetailsFragment.ARGUMENT_EXTRA_ACCOUNT_ID);
    }

    @Override
    public Object getFragmentMenuParameter() {
        return this.getAccountId();
    }
}
