package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.bytesizedhosting.api.android.darthcrap.v3.BytesizedApplication;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;
import com.bytesizedhosting.api.android.darthcrap.v3.util.UriUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContentProviderHelper {
    public static enum ResultType {
        ITEM ("vnd.android.cursor.item"),
        LIST ("vnd.android.cursor.dir");

        private String mimeStart;

        private ResultType(String mimeStart) {
            this.mimeStart = mimeStart;
        }

        public String getMimeStart() {
            return this.mimeStart;
        }
    }

    private static class MimeTypeWrappedQueryHandler {
        private IQueryHandler queryHandler;
        private String mimeType;

        public MimeTypeWrappedQueryHandler(
            ResultType resultType,
            String tableName,
            IQueryHandler queryHandler
        ) {
           this.queryHandler = queryHandler;
           this.mimeType = resultType.getMimeStart()+"/"+ BytesizedApplication.class.getPackage().getName()+"."+tableName;
        }

        public IQueryHandler getQueryHandler() {
            return this.queryHandler;
        }

        public String getMimeType() {
            return this.mimeType;
        }
    }
    
    private static interface IIDRetriever {
        public long getID(
            List<String> pathSegments
        );
    }

    private static interface ISelectionGenerator {
        String generateSelection(
            List<String> pathSegments,
            String providedSelection
        );
    }

    private String authority;
    private int nextUriMatcherToken = 1;
    private UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private Map<Integer,MimeTypeWrappedQueryHandler> tokenToHandlerMap = new HashMap<>();

    public ContentProviderHelper(
        String authority
    ) {
        if (authority == null) {
            throw new IllegalArgumentException("The authority must not be NULL.");
        }
        else {
            this.authority = authority;
        }
    }

    private MimeTypeWrappedQueryHandler lookupMimeTypeWrappedQueryHandler(
        Uri uri
    ) {
        if (uri == null) {
            throw new IllegalArgumentException("The Uri must not be NULL.");
        }
        else {
            int token = this.uriMatcher.match(uri);
            MimeTypeWrappedQueryHandler handler = this.tokenToHandlerMap.get(token);
            if (handler == null) {
                throw new UnsupportedOperationException("Unrecognized URI - The Uri in question was not recognized as being handled by this provider: " + uri);
            }
            else {
                return handler;
            }
        }
    }

    private IQueryHandler lookupQueryHandler(
        Uri uri
    ) {
        return this.lookupMimeTypeWrappedQueryHandler(uri).getQueryHandler();
    }

    public String getType(
        Uri uri
    ) {
        return this.lookupMimeTypeWrappedQueryHandler(uri).getMimeType();
    }

    public Cursor select(
        Context context,
        SQLiteOpenHelper dbHelper,
        Uri uri,
        String[] projection,
        String selection,
        String[] selectionArgs,
        String sortOrder
    ) {
        return this.lookupQueryHandler(uri).select(context,dbHelper.getReadableDatabase(),uri,uri.getPathSegments(),projection,selection,selectionArgs,sortOrder);
    }

    public Uri insert(
        Context context,
        SQLiteOpenHelper dbHelper,
        Uri uri,
        ContentValues values
    ) {
        return this.lookupQueryHandler(uri).insert(context,dbHelper.getWritableDatabase(),uri,uri.getPathSegments(),values);
    }

    public int delete(
        Context context,
        SQLiteOpenHelper dbHelper,
        Uri uri,
        String selection,
        String[] selectionArgs
    ) {
        return this.lookupQueryHandler(uri).delete(context,dbHelper.getWritableDatabase(),uri,uri.getPathSegments(),selection,selectionArgs);
    }

    public int update(
        Context context,
        SQLiteOpenHelper dbHelper,
        Uri uri,
        ContentValues values,
        String selection,
        String[] selectionArgs
    ) {
        return this.lookupQueryHandler(uri).update(context,dbHelper.getWritableDatabase(),uri,uri.getPathSegments(),values,selection,selectionArgs);
    }

    public ContentProviderResult[] applyBatch(
        ContentProvider contentProvider,
        SQLiteOpenHelper dbHelper,
        ArrayList<ContentProviderOperation> operations
    ) throws OperationApplicationException {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            int operationCount = operations.size();
            ContentProviderResult[] results = new ContentProviderResult[operationCount];
            for (int operationIndex = 0; operationIndex < operationCount; operationIndex++) {
                results[operationIndex] = operations.get(operationIndex).apply(contentProvider, results, operationIndex);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    private void addCore(
        String path,
        MimeTypeWrappedQueryHandler mimeTypeWrappedQueryHandler
    ) {
        int token = this.nextUriMatcherToken;
        this.nextUriMatcherToken++;
        this.uriMatcher.addURI(this.authority,path,token);
        this.tokenToHandlerMap.put(token,mimeTypeWrappedQueryHandler);
    }

    public void addQuery(
        String pathForQuery,
        ResultType resultType,
        String tableName,
        IQueryHandler queryHandler
    ) {
        this.addCore(
            pathForQuery,
            new MimeTypeWrappedQueryHandler(
                resultType,
                tableName,
                queryHandler
            )
        );
    }

    public void addStandardEntity(
        String pathForEntityList,
        String tableName,
        String columnNameId
    ) {
        if (pathForEntityList == null) {
            throw new IllegalArgumentException("The entity path must not be NULL.");
        }
        else if (tableName == null) {
            throw new IllegalArgumentException("The table name must not be NULL.");
        }
        else if (columnNameId == null) {
            throw new IllegalArgumentException("The column name for the ID field must not be NULL.");
        }
        else {
            UriUtils.checkIfPathForEntityIsValid(pathForEntityList);
            String pathForEntityById = pathForEntityList + "/#";
            IIDRetriever idRetriever = (
                List<String> pathSegments
            ) -> Long.parseLong(pathSegments.get(pathSegments.size() - 1));
            ISelectionGenerator selectionGenerator = (
                List<String> pathSegments,
                String providedSelection
            ) -> {
                if (providedSelection == null || providedSelection.trim().isEmpty()) {
                    return columnNameId + " = " + idRetriever.getID(pathSegments);
                }
                else {
                    throw new IllegalArgumentException("A selection cannot be provided for a by-id URI.");
                }
            };
            this.addQuery(
                pathForEntityList,
                ResultType.LIST,
                tableName,
                new QueryHandlerAdapter(pathForEntityList) {
                    @Override
                    public Cursor select(
                        Context context,
                        SQLiteDatabase readableDatabase,
                        Uri uri,
                        List<String> pathSegments,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder
                    ) {
                        return readableDatabase.query(
                            tableName,
                            projection,
                            selection,
                            selectionArgs,
                            null,
                            null,
                            sortOrder
                        );
                    }

                    @Override
                    public Uri insert(
                        Context context,
                        SQLiteDatabase writableDatabase,
                        Uri uri,
                        List<String> pathSegments,
                        ContentValues values
                    ) {
                        long newId = writableDatabase.insertOrThrow(
                            tableName,
                            null,
                            values
                        );
                        Uri newUri = UriUtils.constructContentUri(
                            UriUtils.constructContentBaseUri(ContentProviderHelper.this.authority),
                            pathForEntityById,
                            new long[] {newId}
                        );
                        ContentProviderHelper.this.notifyChange(context, uri);
                        return newUri;
                    }

                    @Override
                    public int update(
                        Context context,
                        SQLiteDatabase writableDatabase,
                        Uri uri,
                        List<String> pathSegments,
                        ContentValues values,
                        String selection,
                        String[] selectionArgs
                    ) {
                        int rows = writableDatabase.update(
                            tableName,
                            values,
                            selection,
                            selectionArgs
                        );
                        ContentProviderHelper.this.notifyChange(context,uri);
                        return rows;
                    }

                    @Override
                    public int delete(
                        Context context,
                        SQLiteDatabase writableDatabase,
                        Uri uri,
                        List<String> pathSegments,
                        String selection,
                        String[] selectionArgs
                    ) {
                        if (selection == null) {
                            selection = "1";
                        }
                        int rows = writableDatabase.delete(
                            tableName,
                            selection,
                            selectionArgs
                        );
                        ContentProviderHelper.this.notifyChange(context,uri);
                        return rows;
                    }
                }
            );
            this.addQuery(
                pathForEntityById,
                ResultType.ITEM,
                tableName,
                new QueryHandlerAdapter(pathForEntityById) {
                    @Override
                    public Cursor select(
                        Context context,
                        SQLiteDatabase readableDatabase,
                        Uri uri,
                        List<String> pathSegments,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder
                    ) {
                        return readableDatabase.query(
                            tableName,
                            projection,
                            selectionGenerator.generateSelection(pathSegments,selection),
                            selectionArgs,
                            null,
                            null,
                            sortOrder
                        );
                    }

                    @Override
                    public int delete(
                        Context context,
                        SQLiteDatabase writableDatabase,
                        Uri uri,
                        List<String> pathSegments,
                        String selection,
                        String[] selectionArgs
                    ) {
                        int rows = writableDatabase.delete(
                            tableName,
                            selectionGenerator.generateSelection(pathSegments,selection),
                            selectionArgs
                        );
                        ContentProviderHelper.this.notifyChange(context,uri);
                        return rows;
                    }

                    @Override
                    public int update(
                        Context context,
                        SQLiteDatabase writableDatabase,
                        Uri uri,
                        List<String> pathSegments,
                        ContentValues values,
                        String selection,
                        String[] selectionArgs
                    ) {
                        Long valuesId = values.getAsLong(columnNameId);
                        long pathId = idRetriever.getID(pathSegments);
                        if (valuesId == null) {
                            throw new IllegalArgumentException("No ID was provided in the content values.");
                        }
                        else if (valuesId != pathId) {
                            throw new IllegalArgumentException("The ID provided in the URI (" + pathId + ") does not match the ID provided in the content values (" + valuesId + ").");
                        }
                        else {
                            int rows = writableDatabase.update(
                                tableName,
                                values,
                                selectionGenerator.generateSelection(pathSegments, selection),
                                selectionArgs
                            );
                            ContentProviderHelper.this.notifyChange(context,uri);
                            return rows;
                        }
                    }
                }
            );
        }
    }

    private void notifyChange(
        Context context,
        Uri uri
    ) {
        if (!BytesizedContract.hasSyncAdapterQueryStringParameter(uri)) {
            context.getContentResolver().notifyChange(uri, null);
        }
    }
}
