package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl;

import android.content.ContentValues;
import android.database.Cursor;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountQuotaHistoryDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.FieldMapping;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountQuotaHistory;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;
import com.bytesizedhosting.api.android.darthcrap.v3.util.UriUtils;

import java.util.Arrays;
import java.util.List;

public class AccountQuotaHistoryDao extends AbstractContentProviderDao<AccountQuotaHistory> implements IAccountQuotaHistoryDao {
    public AccountQuotaHistoryDao(
        IContentProviderWrapper contentProviderWrapper
    ) {
        super(
            contentProviderWrapper,
            AccountQuotaHistory::new,
            BytesizedContract.AccountQuotaHistoryEntitySpec.PATH_ENTITY,
            Arrays.<FieldMapping<AccountQuotaHistory>>asList(
                FieldMapping.create(BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ID                      , AccountQuotaHistory::getId                   , AccountQuotaHistory::setId                   , Cursor::getLong, ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ACCOUNT_ID              , AccountQuotaHistory::getAccountId            , AccountQuotaHistory::setAccountId            , Cursor::getLong, ContentValues::put)
            ),
            Arrays.<FieldMapping<AccountQuotaHistory>>asList(
                FieldMapping.create(BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_TAKEN_AT                , AccountQuotaHistory::getTakenAt              , AccountQuotaHistory::setTakenAt                                                   ),
                FieldMapping.create(BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_STORAGE_QUOTA_IN_BYTES, AccountQuotaHistory::getStorageQuotaInBytes, AccountQuotaHistory::setStorageQuotaInBytes, Cursor::getLong, ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_BANDWIDTH_QUOTA_IN_BYTES, AccountQuotaHistory::getBandwidthQuotaInBytes, AccountQuotaHistory::setBandwidthQuotaInBytes, Cursor::getLong, ContentValues::put)
            )
        );
    }

    @Override
    public AccountQuotaHistory selectLatestQuotaForAccount(
        long accountId
    ) {
        return this.createEntityFromCursorAndClose(
            this.getContentProviderWrapper().query(
                UriUtils.constructContentUri(
                    BytesizedContract.AUTHORITY,
                    BytesizedContract.AccountQuotaHistoryEntitySpec.PATH_QUERY_SELECT_CURRENT_QUOTA_FOR_ACCOUNT,
                    accountId
                ),
                this.getProjection(),
                null,
                null,
                null
            )
        );
    }

    @Override
    public List<AccountQuotaHistory> selectAllWithAccountId(
        long accountId
    ) {
        return this.selectAllWithParentId(
            BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ACCOUNT_ID,
            accountId
        );
    }

    @Override
    public List<AccountQuotaHistory> selectWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectWithParentIdAndCriteria(
            BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ACCOUNT_ID,
            accountId,
            whereClause,
            whereParameters,
            sortClause
        );
    }

    @Override
    public AccountQuotaHistory selectSingleWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectSingleWithParentIdAndCriteria(
            BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ACCOUNT_ID,
            accountId,
            whereClause,
            whereParameters,
            sortClause
        );
    }
}
