package com.bytesizedhosting.api.android.darthcrap.v3.util.size;

import android.content.Context;

public class SizeString {
    private double amount;
    private String sizeUnitString;

    public SizeString(double amount, String sizeUnitString) {
        this.amount = amount;
        this.sizeUnitString = sizeUnitString;
    }

    public double getAmount() {
        return this.amount;
    }

    public String getSizeUnitString() {
        return this.sizeUnitString;
    }

    public static SizeString createFromSize(Context context, Size size, SizeStringMode sizeStringMode) {
        double amount = size.getAmount();
        SizeUnit sizeUnit = size.getSizeUnit();
        int sizeUnitStringResourceId;
        if (sizeStringMode == SizeStringMode.STANDARD) {
            if (amount == 1) {
                sizeUnitStringResourceId = sizeUnit.getStandardSingularStringResourceId();
            }
            else {
                sizeUnitStringResourceId = sizeUnit.getStandardPluralStringResourceId();
            }
        }
        else {
            if (amount == 1 || sizeStringMode == SizeStringMode.ABBREVIATION_FORCE_SINGULAR) {
                sizeUnitStringResourceId = sizeUnit.getAbbreviationSingularStringResourceId();
            }
            else {
                sizeUnitStringResourceId = sizeUnit.getAbbreviationPluralStringResourceId();
            }
        }
        String sizeUnitString = context.getResources().getString(sizeUnitStringResourceId);
        return new SizeString(amount,sizeUnitString);
    }

    public static SizeString createFromBytes(Context context, long amountOfBytes, SizeUnitSystem sizeUnitSystem, SizeStringMode sizeStringMode) {
        return SizeString.createFromSize(context, Size.createFromBytes(amountOfBytes, sizeUnitSystem), sizeStringMode);
    }

    public static SizeString createFromBytes(Context context, long amountOfBytes, SizeUnitSystem sizeUnitSystem, double roundUpModifier, SizeStringMode sizeStringMode) {
        return SizeString.createFromSize(context, Size.createFromBytes(amountOfBytes, sizeUnitSystem, roundUpModifier), sizeStringMode);
    }
}