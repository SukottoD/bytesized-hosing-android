package com.bytesizedhosting.api.android.darthcrap.v3.model.external;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExternalError {
    @JsonProperty("error")
    private String errorMessage;

    @JsonProperty("error_code")
    private int errorCode;

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
