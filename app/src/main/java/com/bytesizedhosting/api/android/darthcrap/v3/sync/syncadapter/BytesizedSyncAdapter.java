package com.bytesizedhosting.api.android.darthcrap.v3.sync.syncadapter;

import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.SQLException;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.bytesizedhosting.api.android.darthcrap.v3.BytesizedApplication;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IGravatarDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.IContentProviderDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountQuotaHistoryDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountUserAppDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountUserAppOptionDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.GravatarDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.UserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.ContentProviderClientContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.exceptions.ApiErrorException;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Account;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountQuotaHistory;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountState;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountType;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserApp;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserAppOption;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserAppState;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Gravatar;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.IWithId;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.model.external.ExternalAccount;
import com.bytesizedhosting.api.android.darthcrap.v3.model.external.ExternalAccountUserApp;
import com.bytesizedhosting.api.android.darthcrap.v3.model.external.ExternalUser;
import com.bytesizedhosting.api.android.darthcrap.v3.model.external.IExternalWithGravatarDigest;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.authenticator.BytesizedAuthenticator;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.IBytesizedAPIService;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.IGravatarImageDataService;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.impl.BytesizedAPIService;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.impl.GravatarImageDataService;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.syncadapter.util.PossibleBackReferenceIndexBasedId;
import com.bytesizedhosting.api.android.darthcrap.v3.util.IntentUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.NumericConstants;
import com.bytesizedhosting.api.android.darthcrap.v3.util.UriUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.size.SizeUnit;

import org.joda.time.DateTime;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class BytesizedSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final int WANTED_GRAVATAR_SIZE = 144;
    private static final long DEFAULT_PERIODIC_SYNC_FREQUENCY_IN_HOURS = 6;
    private static final long DEFAULT_PERIODIC_SYNC_FREQUENCY_IN_SECONDS = BytesizedSyncAdapter.DEFAULT_PERIODIC_SYNC_FREQUENCY_IN_HOURS * 60 * 60;
    private static final String EXTERNAL_ACCOUNT_TYPE_STREAMBOX = "Streambox";
    private static final String EXTERNAL_ACCOUNT_TYPE_SHARED_ACCOUNT = "SharedAccount";
    private static final String EXTERNAL_ACCOUNT_TYPE_VPS_ACCOUNT = "VpsAccount";

    private Context context;
    private IBytesizedAPIService bytesizedAPIService;
    private IGravatarImageDataService gravatarImageDataService;

    public BytesizedSyncAdapter(
        Context context,
        boolean autoInitialize
    ) {
        super(context, autoInitialize);
        this.context = context;
        this.bytesizedAPIService = new BytesizedAPIService();
        this.gravatarImageDataService = new GravatarImageDataService();
    }

    public static void initializeSyncForAccount(android.accounts.Account account) {
        Bundle periodicSyncExtras = new Bundle();
        Bundle nowSyncExtras = new Bundle();

        ContentResolver.setIsSyncable(account, BytesizedContract.AUTHORITY, 1);
        ContentResolver.addPeriodicSync(account, BytesizedContract.AUTHORITY, periodicSyncExtras, BytesizedSyncAdapter.DEFAULT_PERIODIC_SYNC_FREQUENCY_IN_SECONDS);
        ContentResolver.requestSync(account, BytesizedContract.AUTHORITY, nowSyncExtras);
    }

    private static class ApiKeyWrappedExternalUser {
        private ExternalUser externalUser;
        private String apiKey;

        private ApiKeyWrappedExternalUser(
            ExternalUser externalUser,
            String apiKey
        ) {
            this.externalUser = externalUser;
            this.apiKey = apiKey;
        }

        public ExternalUser getExternalUser() {
            return this.externalUser;
        }

        public String getApiKey() {
            return this.apiKey;
        }
    }

    @Override
    public void onPerformSync(
        android.accounts.Account account,
        Bundle extras,
        String authority,
        ContentProviderClient provider,
        SyncResult syncResult
    ) {
        try {
            this.broadcastStartedIntent();

            AccountManager accountManager = AccountManager.get(this.context);
            String apiKey = accountManager.blockingGetAuthToken(account, BytesizedAuthenticator.getApiKeyAuthTokenType(this.context), true);
            ExternalUser externalUser = this.bytesizedAPIService.fetch(apiKey);

            IContentProviderWrapper providerWrapper = new ContentProviderClientContentProviderWrapper(provider);

            ArrayList<ContentProviderOperation> operations = new ArrayList<>();

            Map<String,PossibleBackReferenceIndexBasedId> gravatarIds = this.syncGravatarEntities(operations, providerWrapper, externalUser);
            this.syncUsers(operations, providerWrapper, gravatarIds, Arrays.asList(new ApiKeyWrappedExternalUser(externalUser,apiKey)));

            provider.applyBatch(operations);

            // We don't care about rollback with these.
            this.syncGravatarImageData(providerWrapper);

            ContentResolver contentResolver = this.context.getContentResolver();
            this.notifyChange(contentResolver,BytesizedContract.UserEntitySpec                .PATH_ENTITY);
            this.notifyChange(contentResolver,BytesizedContract.AccountEntitySpec             .PATH_ENTITY);
            this.notifyChange(contentResolver,BytesizedContract.AccountQuotaHistoryEntitySpec .PATH_ENTITY);
            this.notifyChange(contentResolver,BytesizedContract.AccountUserAppEntitySpec      .PATH_ENTITY);
            this.notifyChange(contentResolver,BytesizedContract.AccountUserAppOptionEntitySpec.PATH_ENTITY);
            this.notifyChange(contentResolver,BytesizedContract.GravatarEntitySpec            .PATH_ENTITY);
        }
        catch (AuthenticatorException exA) {
            syncResult.stats.numAuthExceptions++;
        }
        catch (ApiErrorException exAE) {
            syncResult.stats.numParseExceptions++;
        }
        catch (OperationCanceledException exOC) {
            syncResult.stats.numAuthExceptions++;
        }
        catch (IOException exIO) {
            syncResult.stats.numIoExceptions++;
        }
        catch (RemoteException exR) {
            syncResult.stats.numParseExceptions++;
        }
        catch (OperationApplicationException exOA) {
            syncResult.stats.numParseExceptions++;
        }
        catch (SQLException exSQL) {
            syncResult.stats.numIoExceptions++;
        }
        finally {
            this.broadcastCompletedIntent(!syncResult.hasError());
        }
    }

    private void notifyChange(
        ContentResolver contentResolver,
        String pathForEntity
    ) {
        contentResolver.notifyChange(UriUtils.constructContentUri(BytesizedContract.AUTHORITY,pathForEntity),null);
    }

    private void broadcastStartedIntent() {
        Log.d(BytesizedApplication.TAG, "Sync Starting - Broadcasting Intent.");
        this.context.sendBroadcast(IntentUtils.createSyncStartedIntent());
    }

    private void broadcastCompletedIntent(boolean wasSuccessful) {
        Log.d(BytesizedApplication.TAG, "Sync Completed - Broadcasting Intent (Successful = " + wasSuccessful + ").");
        this.context.sendBroadcast(IntentUtils.createSyncCompletedIntent(wasSuccessful));
    }

    private Map<String,PossibleBackReferenceIndexBasedId> syncGravatarEntities(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        ExternalUser externalUser
    ) {
        GravatarDao gravatarDao = new GravatarDao(provider);
        BytesizedContract.applySyncAdapterQueryStringParameter(gravatarDao);
        List<Gravatar> knownGravatars = gravatarDao.selectAllWithoutRetrievingImageData();
        Map<String,PossibleBackReferenceIndexBasedId> knownGravatarIds = new HashMap<>(knownGravatars.size());
        for (Gravatar knownGravatar : knownGravatars) {
            knownGravatarIds.put(this.normalizeGravatarDigest(knownGravatar.getGravatarDigest()),PossibleBackReferenceIndexBasedId.forId(knownGravatar.getId()));
        }

        this.syncGravatarEntities(operations, knownGravatarIds, gravatarDao, externalUser);

        return knownGravatarIds;
    }

    private void syncGravatarEntities(
        List<ContentProviderOperation> operations,
        Map<String,PossibleBackReferenceIndexBasedId> knownGravatarIds,
        IGravatarDao gravatarDao,
        ExternalUser externalUser
    ) {
        this.syncGravatarEntity(operations, knownGravatarIds, gravatarDao, externalUser);
    }

    private void syncGravatarEntity(
        List<ContentProviderOperation> operations,
        Map<String,PossibleBackReferenceIndexBasedId> knownGravatarIds,
        IGravatarDao gravatarDao,
        IExternalWithGravatarDigest gravatarContainingExternal
    ) {
        String gravatarDigest = gravatarContainingExternal.getGravatarDigest();
        if (gravatarDigest != null) {
            gravatarDigest = this.normalizeGravatarDigest(gravatarDigest);
            if (!knownGravatarIds.containsKey(gravatarDigest)) {
                Gravatar newGravatar = new Gravatar();
                newGravatar.setGravatarDigest(gravatarDigest);
                newGravatar.setImageData(null);
                newGravatar.setLastFetchedAt(null);
                knownGravatarIds.put(gravatarDigest,PossibleBackReferenceIndexBasedId.forBackReference(operations,gravatarDao.insertOperation(newGravatar).build()));
            }
        }
    }

    private String normalizeGravatarDigest(
        String gravatarDigest
    ) {
        return gravatarDigest.trim().toLowerCase(Locale.ENGLISH);
    }

    private void syncUsers(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
        List<ApiKeyWrappedExternalUser> apiKeyWrappedExternalUsers
    ) {
        this.<User,ApiKeyWrappedExternalUser,String,UserDao>syncEntities(
            operations,
            provider,
            gravatarIds,
            apiKeyWrappedExternalUsers,
            User::getApiKey,
            UserDao::new,
            UserDao::selectAll,
            false,
            this::transformUser,
            this::processSyncedUser,
            (operationBuilder, apiKeyWrappedExternalUser) -> this.applyGravatarToOperationBuilder(operationBuilder, gravatarIds, BytesizedContract.UserEntitySpec.COLUMN_NAME_GRAVATAR_ID, apiKeyWrappedExternalUser.getExternalUser().getGravatarDigest())
        );
    }

    private void applyGravatarToOperationBuilder(
        ContentProviderOperation.Builder operationBuilder,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds,
        String gravatarIdColumnName,
        String gravatarDigest
    ) {
        PossibleBackReferenceIndexBasedId gravatarId = this.getGravatarId(gravatarIds, gravatarDigest);
        if (gravatarId != null) {
            gravatarId.applyTo(gravatarIdColumnName, operationBuilder);
        }
    }

    private static interface IGravatarIdSetter {
        public void setGravatarId(Long gravatarId);
    }

    private <EntityType> void applyGravatarToEntity(
        IGravatarIdSetter gravatarIdSetter,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds,
        String gravatarDigest
    ) {
        PossibleBackReferenceIndexBasedId gravatarId = gravatarIds.get(this.normalizeGravatarDigest(gravatarDigest));
        if (gravatarId != null && gravatarId.isId()) {
            gravatarIdSetter.setGravatarId(gravatarId.getId());
        }
    }

    private PossibleBackReferenceIndexBasedId getGravatarId(
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds,
        String gravatarDigest
    ) {
        return gravatarIds.get(this.normalizeGravatarDigest(gravatarDigest));
    }

    private User transformUser(
        ApiKeyWrappedExternalUser apiKeyWrappedExternalUser,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds
    ) {
        String apiKey = apiKeyWrappedExternalUser.getApiKey();
        ExternalUser externalUser = apiKeyWrappedExternalUser.getExternalUser();

        User user = new User();
        user.setApiKey(apiKey);
        user.setBalanceInCents(new BigDecimal(externalUser.getCurrentBalanceAsString()).multiply(BigDecimal.valueOf(NumericConstants.CENTS_IN_EURO)).intValueExact());
        user.setCountry(externalUser.getCountryAsString());
        user.setCreatedAt(externalUser.getCreatedAt());
        user.setEmailAddress(externalUser.getEmail());
        user.setUsername(externalUser.getLogin());
        user.setLastApiDataUpdateTime(DateTime.now());
        this.applyGravatarToEntity(user::setGravatarId,gravatarIds,externalUser.getGravatarDigest());

        return user;
    }

    private void processSyncedUser(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId userId,
        ApiKeyWrappedExternalUser apiKeyWrappedExternalUser
    ) {
        this.syncAccounts(
            operations,
            provider,
            gravatarIds,
            userId,
            apiKeyWrappedExternalUser.getExternalUser().getActiveAccounts()
        );
    }

    private void syncAccounts(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId userId,
        List<ExternalAccount> externalAccounts
    ) {
        this.syncEntities(
            operations,
            provider,
            gravatarIds,
            userId,
            BytesizedContract.AccountEntitySpec.COLUMN_NAME_USER_ID,
            externalAccounts,
            Account::getServerId,
            AccountDao::new,
            AccountDao::selectAllWithUserId,
            true,
            this::transformAccount,
            this::processSyncedAccount,
            null
        );
    }

    private Account transformAccount(
        ExternalAccount externalAccount,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds
    ) {
        AccountState accountState = AccountState.UNKNOWN;
        if (externalAccount.isDeployed()) {
            if (externalAccount.isDisabled()) {
                accountState = AccountState.DISABLED;
            }
            else {
                accountState = AccountState.DEPLOYED;
            }
        }
        else {
            if (!externalAccount.isDisabled()) {
                accountState = AccountState.INACTIVE;
            }
        }

        AccountType accountType;
        String externalAccountType = externalAccount.getAccountType();
        if (BytesizedSyncAdapter.EXTERNAL_ACCOUNT_TYPE_STREAMBOX.equals(externalAccountType)) {
            accountType = AccountType.STREAMBOX;
        }
        else if (BytesizedSyncAdapter.EXTERNAL_ACCOUNT_TYPE_SHARED_ACCOUNT.equals(externalAccountType)) {
            accountType = AccountType.SHARED_ACCOUNT;
        }
        else if (BytesizedSyncAdapter.EXTERNAL_ACCOUNT_TYPE_VPS_ACCOUNT.equals(externalAccountType)) {
            accountType = AccountType.VPS_ACCOUNT;
        }
        else {
            accountType = AccountType.UNKNOWN;
        }

        Account account = new Account();
        account.setAccountIp(externalAccount.getAccountIp());
        account.setAccountState(accountState);
        account.setBurstStorageInBytes(this.convertToBytes(externalAccount.getBurstAmountInGigabytes(), SizeUnit.GIGABYTE_1000));
        account.setDeployedAt(externalAccount.getDeployedOn());
        account.setLoginPassword(externalAccount.getLoginPassword());
        account.setPaidUntil(externalAccount.getPaidUntil());
        account.setPlanName(externalAccount.getPlanName());
        account.setRenewalPeriodInMonths(externalAccount.getRenewalPeriodInMonths());
        account.setServerName(externalAccount.getServerName());
        account.setServerId(externalAccount.getId().getId());
        account.setTotalBandwidthInBytes(this.convertToBytes(externalAccount.getTotalBandwidthInTebibytes(), SizeUnit.TEBIBYTE_1024));
        account.setTotalStorageInBytes(this.convertToBytes(externalAccount.getTotalStorageInGigabytes(), SizeUnit.GIGABYTE_1000));
        account.setBoostBandwidthInBytes(this.convertToBytes(externalAccount.getActiveBandwidthBoostsInTebibytes(), SizeUnit.TEBIBYTE_1024));
        account.setAccountType(accountType);
        account.setWebUIURL(externalAccount.getWebUIURL());

        return account;
    }

    private Long convertToBytes(Double inputInUnits, SizeUnit sizeUnit) {
        if (inputInUnits == null) {
            return null;
        }
        else {
            return (long)(inputInUnits*sizeUnit.getAmountOfBytes());
        }
    }

    private void processSyncedAccount(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId accountId,
        ExternalAccount externalAccount
    ) {
        this.addQuotaHistoryEntry(
            operations,
            provider,
            accountId,
            externalAccount
        );
        this.syncAccountUserApps(
            operations,
            provider,
            gravatarIds,
            accountId,
            externalAccount.getUserApps()
        );
    }

    private void syncAccountUserApps(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId accountId,
        List<ExternalAccountUserApp> externalAccountUserApps
    ) {
        this.syncEntities(
            operations,
            provider,
            gravatarIds,
            accountId,
            BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ACCOUNT_ID,
            externalAccountUserApps,
            AccountUserApp::getServerId,
            AccountUserAppDao::new,
            AccountUserAppDao::selectAllWithAccountId,
            true,
            this::transformAccountUserApp,
            this::processSyncedAccountUserApp,
            null
        );
    }

    private AccountUserApp transformAccountUserApp(
        ExternalAccountUserApp externalAccountUserApp,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds
    ) {
        AccountUserAppState accountUserAppState;
        if (externalAccountUserApp.isInstalling() && !externalAccountUserApp.isInstalled() && !externalAccountUserApp.isDeleted()) {
            accountUserAppState = AccountUserAppState.INSTALLING;
        }
        else if (!externalAccountUserApp.isInstalling() && externalAccountUserApp.isInstalled() && !externalAccountUserApp.isDeleted()) {
            accountUserAppState = AccountUserAppState.INSTALLED;
        }
        else if (!(externalAccountUserApp.isInstalling() && externalAccountUserApp.isInstalled()) && externalAccountUserApp.isDeleted()) {
            accountUserAppState = AccountUserAppState.DELETED;
        }
        else {
            accountUserAppState = AccountUserAppState.UNKNOWN;
        }

        AccountUserApp accountUserApp = new AccountUserApp();
        accountUserApp.setAccountUserAppState(accountUserAppState);
        accountUserApp.setAppName(externalAccountUserApp.getAppName());
        accountUserApp.setServerId(externalAccountUserApp.getId().getId());
        accountUserApp.setAppServerId(externalAccountUserApp.getAppId().getId());

        return accountUserApp;
    }

    private void processSyncedAccountUserApp(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId accountUserAppId,
        ExternalAccountUserApp externalAccountUserApp
    ) {
        this.syncAccountUserAppOptions(
            operations,
            provider,
            gravatarIds,
            accountUserAppId,
            externalAccountUserApp.getOptions()
        );
    }

    private void syncAccountUserAppOptions(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId accountUserAppId,
        Map<String, Object> accountUserAppOptions
    ) {
        this.syncEntities(
            operations,
            provider,
            gravatarIds,
            accountUserAppId,
            BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID,
            accountUserAppOptions.entrySet(),
            AccountUserAppOption::getName,
            AccountUserAppOptionDao::new,
            AccountUserAppOptionDao::selectAllWithAccountUserAppId,
            true,
            this::transformAccountUserAppOption,
            null,
            null
        );
    }

    private AccountUserAppOption transformAccountUserAppOption(
        Map.Entry<String, Object> accountUserAppOptionMapEntry,
        Map<String, PossibleBackReferenceIndexBasedId> gravatarIds
    ) {
        AccountUserAppOption accountUserAppOption = new AccountUserAppOption();
        accountUserAppOption.setName(accountUserAppOptionMapEntry.getKey());
        Object value = accountUserAppOptionMapEntry.getValue();
        String valueString;
        if (value == null) {
            valueString = null;
        }
        else {
            valueString = value.toString();
        }
        accountUserAppOption.setValue(valueString);
        return accountUserAppOption;
    }

    private void addQuotaHistoryEntry(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        PossibleBackReferenceIndexBasedId accountId,
        ExternalAccount externalAccount
    ) {
        AccountQuotaHistoryDao dao = new AccountQuotaHistoryDao(provider);
        BytesizedContract.applySyncAdapterQueryStringParameter(dao);
        AccountQuotaHistory history = new AccountQuotaHistory();
        history.setBandwidthQuotaInBytes(externalAccount.getBandwidthQuotaInBytes());
        history.setStorageQuotaInBytes(this.convertToBytes(externalAccount.getDiskQuotaInKiloBytes(), SizeUnit.KILOBYTE_1000));
        history.setTakenAt(DateTime.now());
        ContentProviderOperation.Builder operationBuilder = dao.insertOperation(history);
        accountId.applyTo(BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ACCOUNT_ID,operationBuilder);
        operations.add(operationBuilder.build());
    }

    private static interface IDaoEntityListFetcher<DaoType extends IContentProviderDao<EntityType>, EntityType> {
        public List<EntityType> fetch(
            DaoType dao
        );
    }

    private static interface IDaoEntityListFetcherWithParentId<DaoType extends IContentProviderDao<EntityType>, EntityType> {
        public List<EntityType> fetch(
            DaoType dao,
            long parentId
        );
    }

    private static interface IKeyGetter<EntityType,KeyType> {
        KeyType getKey(
            EntityType entity
        );
    }

    private static interface IExternalEntityTransformer<ExternalEntityType,EntityType> {
        EntityType transform(
            ExternalEntityType externalEntity,
            Map<String, PossibleBackReferenceIndexBasedId> gravatarIds
        );
    }

    private static interface ISyncedEntityProcessor<ExternalEntityType> {
        void process(
            List<ContentProviderOperation> operations,
            IContentProviderWrapper provider,
            Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
            PossibleBackReferenceIndexBasedId parentId,
            ExternalEntityType externalEntity
        );
    }

    private static interface IDaoCreator<DaoType extends IContentProviderDao<EntityType>, EntityType> {
        DaoType createDao(
            IContentProviderWrapper provider
        );
    }

    private static interface IOperationModifier<ExternalEntityType> {
        void modifyOperation(
            ContentProviderOperation.Builder operationBuilder,
            ExternalEntityType externalEntity
        );
    }

    private <EntityType extends IWithId,ExternalEntityType,KeyType,DaoType extends IContentProviderDao<EntityType>> void syncEntities(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
        Collection<ExternalEntityType> externalEntities,
        IKeyGetter<EntityType,KeyType> keyGetter,
        IDaoCreator<DaoType,EntityType> daoCreator,
        IDaoEntityListFetcher<DaoType,EntityType> daoEntityListFetcher,
        boolean deleteEntitiesNotProvided,
        IExternalEntityTransformer<ExternalEntityType,EntityType> externalEntityTransformer,
        ISyncedEntityProcessor<ExternalEntityType> syncedEntityProcessor,
        IOperationModifier<ExternalEntityType> operationModifier
    ) {
        this.syncEntitiesCore(
            operations,
            provider,
            gravatarIds,
            null,
            null,
            externalEntities,
            keyGetter,
            daoCreator,
            daoEntityListFetcher,
            deleteEntitiesNotProvided,
            externalEntityTransformer,
            syncedEntityProcessor,
            operationModifier
        );
    }

    private <EntityType extends IWithId,ExternalEntityType,KeyType,DaoType extends IContentProviderDao<EntityType>> void syncEntities(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId parentId,
        String parentIdFieldName,
        Collection<ExternalEntityType> externalEntities,
        IKeyGetter<EntityType,KeyType> keyGetter,
        IDaoCreator<DaoType,EntityType> daoCreator,
        IDaoEntityListFetcherWithParentId<DaoType,EntityType> daoEntityListFetcher,
        boolean deleteEntitiesNotProvided,
        IExternalEntityTransformer<ExternalEntityType,EntityType> externalEntityTransformer,
        ISyncedEntityProcessor<ExternalEntityType> syncedEntityProcessor,
        IOperationModifier<ExternalEntityType> operationModifier
    ) {
        this.syncEntitiesCore(
            operations,
            provider,
            gravatarIds,
            parentId,
            parentIdFieldName,
            externalEntities,
            keyGetter,
            daoCreator,
            (dao) -> daoEntityListFetcher.fetch(dao, parentId.getId()),
            deleteEntitiesNotProvided,
            externalEntityTransformer,
            syncedEntityProcessor,
            operationModifier
        );
    }

    private <EntityType extends IWithId,ExternalEntityType,KeyType,DaoType extends IContentProviderDao<EntityType>> void syncEntitiesCore(
        List<ContentProviderOperation> operations,
        IContentProviderWrapper provider,
        Map<String,PossibleBackReferenceIndexBasedId> gravatarIds,
        PossibleBackReferenceIndexBasedId parentId,
        String parentIdFieldName,
        Collection<ExternalEntityType> externalEntities,
        IKeyGetter<EntityType,KeyType> keyGetter,
        IDaoCreator<DaoType,EntityType> daoCreator,
        IDaoEntityListFetcher<DaoType,EntityType> daoEntityListFetcher,
        boolean deleteEntitiesNotProvided,
        IExternalEntityTransformer<ExternalEntityType,EntityType> externalEntityTransformer,
        ISyncedEntityProcessor<ExternalEntityType> syncedEntityProcessor,
        IOperationModifier<ExternalEntityType> operationModifier
    ) {
        if (externalEntities == null) {
            externalEntities = Collections.emptyList();
        }
        Map<EntityType,ExternalEntityType> transformedExternalEntities = new HashMap<>();
        Map<EntityType,PossibleBackReferenceIndexBasedId> transformedEntityIds = new HashMap<>();
        for (ExternalEntityType externalEntity : externalEntities) {
            transformedExternalEntities.put(externalEntityTransformer.transform(externalEntity,gravatarIds),externalEntity);
        }
        List<EntityType> databaseEntities;
        DaoType dao = daoCreator.createDao(provider);
        BytesizedContract.applySyncAdapterQueryStringParameter(dao);
        if (parentId == null || !parentId.isBackReferenceIndex()) {
            databaseEntities = daoEntityListFetcher.fetch(dao);
        }
        else {
            databaseEntities = Collections.emptyList();
        }
        List<EntityType> databaseEntitiesToRemove = new ArrayList<>(databaseEntities);
        Map<KeyType,EntityType> databaseEntitiesByKey = new HashMap<>(databaseEntities.size());
        for (EntityType databaseEntity : databaseEntities) {
            databaseEntitiesByKey.put(keyGetter.getKey(databaseEntity),databaseEntity);
        }
        for (EntityType transformedExternalEntity : transformedExternalEntities.keySet()) {
            KeyType transformedExternalEntityKey = keyGetter.getKey(transformedExternalEntity);
            EntityType databaseEntity = databaseEntitiesByKey.get(transformedExternalEntityKey);
            ContentProviderOperation.Builder operationBuilder = null;
            if (databaseEntity != null) {
                transformedExternalEntity.setId(databaseEntity.getId());
                databaseEntitiesToRemove.remove(databaseEntity);
                if (!dao.isPartiallyEqual(transformedExternalEntity,databaseEntity)) {
                    operationBuilder = dao.updateOperation(transformedExternalEntity);
                }
            }
            else {
                operationBuilder = dao.insertOperation(transformedExternalEntity);
            }
            PossibleBackReferenceIndexBasedId id;
            if (operationBuilder != null) {
                if (parentId != null && parentIdFieldName != null) {
                    parentId.applyTo(parentIdFieldName, operationBuilder);
                }
                if (operationModifier != null) {
                    operationModifier.modifyOperation(operationBuilder, transformedExternalEntities.get(transformedExternalEntity));
                }
                if (databaseEntity != null) {
                    id = PossibleBackReferenceIndexBasedId.forId(databaseEntity.getId());
                    operations.add(operationBuilder.build());
                }
                else {
                    id = PossibleBackReferenceIndexBasedId.forBackReference(operations, operationBuilder.build());
                }
            }
            else if (databaseEntity != null) {
                id = PossibleBackReferenceIndexBasedId.forId(databaseEntity.getId());
            }
            else {
                throw new IllegalStateException("The operation builder is NULL AND the database entity is NULL. This should never happen.");
            }
            transformedEntityIds.put(transformedExternalEntity,id);
        }
        if (deleteEntitiesNotProvided) {
            for (EntityType databaseEntity : databaseEntitiesToRemove) {
                operations.add(dao.deleteOperation(databaseEntity).build());
            }
        }
        if (syncedEntityProcessor != null) {
            for (Map.Entry<EntityType, ExternalEntityType> transformedExternalEntity : transformedExternalEntities.entrySet()) {
                PossibleBackReferenceIndexBasedId id = transformedEntityIds.get(transformedExternalEntity.getKey());
                syncedEntityProcessor.process(
                    operations,
                    provider,
                    gravatarIds,
                    id,
                    transformedExternalEntity.getValue()
                );
            }
        }
    }

    private void syncGravatarImageData(
        IContentProviderWrapper provider
    ) throws IOException {
        IGravatarDao gravatarDao = new GravatarDao(provider);
        List<Gravatar> neverFetchedGravatars = gravatarDao.selectAllThatHaveNeverBeenFetched();
        if (!neverFetchedGravatars.isEmpty()) {
            Map<String,Gravatar> gravatarDigests = new HashMap<>(neverFetchedGravatars.size());
            for (Gravatar gravatar : neverFetchedGravatars) {
                gravatarDigests.put(gravatar.getGravatarDigest(),gravatar);
            }
            Map<String,byte[]> imageData = this.gravatarImageDataService.getBulkImageData(gravatarDigests.keySet(),BytesizedSyncAdapter.WANTED_GRAVATAR_SIZE);
            for (Map.Entry<String,byte[]> imageDataEntry : imageData.entrySet()) {
                Gravatar gravatar = gravatarDigests.get(imageDataEntry.getKey());
                byte[] imageDataForEntry = imageDataEntry.getValue();
                if (imageDataForEntry == null) {
                    gravatar.setImageData(null);
                }
                else {
                    gravatar.setImageData(imageDataForEntry);
                }
                gravatarDao.update(gravatar);
            }
        }
    }
}
