package com.bytesizedhosting.api.android.darthcrap.v3.ui.controls;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;

public class TitledPanelView extends AbstractViewWrapperLinearLayout {
    public TitledPanelView(Context context) {
        super(LinearLayout.VERTICAL, context);
    }

    public TitledPanelView(Context context, AttributeSet attrs) {
        super(LinearLayout.VERTICAL, context, attrs, R.styleable.TitledPanelView);
    }

    public TitledPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(LinearLayout.VERTICAL, context, attrs, defStyleAttr, R.styleable.TitledPanelView);
    }

    public static TitledPanelView create(Context context, ViewGroup parent, boolean attachToParent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        return (TitledPanelView)layoutInflater.inflate(R.layout.control_instance_titled_panel, parent, attachToParent);
    }

    protected void extractAttributes(Context context, TypedArray ta) {
        this.defaultTitleBackgroundColour = context.getResources().getColor(R.color.control_titled_panel_title_background_default);
        this.setPanelTitleInner(ta.getString(R.styleable.TitledPanelView_panelTitle));
        this.setPanelTitleBackgroundColourInner(ta.getColor(R.styleable.TitledPanelView_panelTitleBackgroundColour, this.defaultTitleBackgroundColour));
        this.setPanelTitleIconInner(ta.getResourceId(R.styleable.TitledPanelView_panelTitleIcon,0));
    }

    private String title;
    private int titleBackgroundColour;
    private int defaultTitleBackgroundColour;
    private Integer panelTitleIconResourceId;

    private ImageView imageView;
    private TableLayout tableLayout;
    private TextView textView;
    private LinearLayout linearLayout;

    @Override
    protected void wrapChildren(View[] children) {
        Context context = this.getContext();
        Resources resources = context.getResources();

        int titlePadding = resources.getDimensionPixelSize(R.dimen.control_titled_panel_title_padding);
        int contentsPadding = resources.getDimensionPixelSize(R.dimen.control_titled_panel_contents_padding);

        this.setBackground(resources.getDrawable(R.drawable.control_titled_panel_background));

        this.tableLayout = new TableLayout(context);
        GenericUtils.setLinearLayoutParams(this.tableLayout);
        this.tableLayout.setColumnStretchable(0,false);
        this.tableLayout.setColumnStretchable(1,true);
        this.tableLayout.setPadding(titlePadding,titlePadding,titlePadding,titlePadding);

        TableRow tableRow = new TableRow(context);
        tableRow.setGravity(Gravity.CENTER_VERTICAL);

        this.imageView = new ImageView(context);
        this.imageView.setPadding(0,0,titlePadding*2,0);

        this.textView = new TextView(context);
        this.textView.setTextAppearance(context, android.R.style.TextAppearance_Medium);
        this.textView.setTextColor(resources.getColor(R.color.control_titled_panel_title_text));
        this.textView.setPadding(0,0,0,0);

        tableRow.addView(this.imageView);
        tableRow.addView(this.textView);

        this.tableLayout.addView(tableRow);

        this.linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(contentsPadding, contentsPadding, contentsPadding, contentsPadding);
        GenericUtils.setLinearLayoutParams(linearLayout);

        this.updateUI();

        GenericUtils.addAllViews(children,linearLayout);

        this.addView(this.tableLayout);
        this.addView(linearLayout);
    }

    protected final void updateUI() {
        if (this.textView != null) {
            this.applyPanelTitle(this.textView);
        }
        if (this.tableLayout != null) {
            this.applyPanelTitleBackgroundColour(this.tableLayout);
        }
        if (this.imageView != null) {
            this.applyPanelTitleIcon(this.imageView);
        }
    }

    private void applyPanelTitle(TextView textView) {
        textView.setText(this.title);
    }

    private void applyPanelTitleBackgroundColour(TableLayout tableLayout) { tableLayout.setBackgroundColor(this.titleBackgroundColour); }

    private void applyPanelTitleIcon(ImageView imageView) {
        if (this.panelTitleIconResourceId == null) {
            imageView.setVisibility(View.GONE);
        }
        else {
            imageView.setImageResource(this.panelTitleIconResourceId);
            imageView.setVisibility(View.VISIBLE);
        }
    }

    private void setPanelTitleInner(String panelTitle) {
        if (panelTitle == null) {
            this.title = "";
        }
        else {
            this.title = panelTitle;
        }
        this.updateUI();
    }

    public void setPanelTitle(String panelTitle) {
        this.setPanelTitleInner(panelTitle);
    }

    public String getPanelTitle() {
        return this.title;
    }

    private void setPanelTitleBackgroundColourInner(Integer panelTitleBackgroundColour) {
        if (panelTitleBackgroundColour == null) {
            this.titleBackgroundColour = this.defaultTitleBackgroundColour;
        }
        else {
            this.titleBackgroundColour = panelTitleBackgroundColour;
        }
        this.updateUI();
    }

    public void setPanelTitleBackgroundColour(Integer panelTitleBackgroundColour) {
        this.setPanelTitleBackgroundColourInner(panelTitleBackgroundColour);
    }

    public Integer getPanelTitleBackgroundColour() {
        return this.titleBackgroundColour;
    }

    private void setPanelTitleIconInner(Integer panelTitleIconResourceId) {
        if (panelTitleIconResourceId != null && panelTitleIconResourceId == 0) {
            this.panelTitleIconResourceId = null;
        }
        else {
            this.panelTitleIconResourceId = panelTitleIconResourceId;
        }
        this.updateUI();
    }

    public void setPanelTitleIcon(Integer panelTitleIconResourceId) {
        this.setPanelTitleIconInner(panelTitleIconResourceId);
    }

    public Integer getPanelTitleIcon() {
        return this.panelTitleIconResourceId;
    }

    @Override
    public LinearLayout getContainer() {
        return this.linearLayout;
    }
}
