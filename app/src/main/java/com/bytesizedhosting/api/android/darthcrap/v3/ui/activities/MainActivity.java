package com.bytesizedhosting.api.android.darthcrap.v3.ui.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments.ApiDataUpdateFragment;
import com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments.DashboardFragment;
import com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments.NavigationDrawerFragment;
import com.bytesizedhosting.api.android.darthcrap.v3.util.LogUtils;

public class MainActivity extends Activity implements ICoordinationActivity, ApiDataUpdateFragment.FragmentInteractionListener {
    private NavigationDrawerFragment navigationDrawerFragment = null;

    @Override
    protected void onCreate(
        Bundle savedInstanceState
    ) {
        super.onCreate(savedInstanceState);
        LogUtils.d(this,"Creating Activity");
        this.setContentView(R.layout.activity_main);

        this.getFragmentManager().addOnBackStackChangedListener(
            MainActivity.this::handleBackStateChanged
        );

        this.navigationDrawerFragment = (NavigationDrawerFragment)this.getFragmentManager().findFragmentById(R.id.drawer_layout_drawer);

        this.navigationDrawerFragment.setUp(
            R.id.drawer_layout_drawer,
            (DrawerLayout) this.findViewById(R.id.drawer_layout)
        );

        this.requestApiDataUpdate(false);
    }

    @Override
    public void requestApiDataUpdate(
        boolean forceNewSync
    ) {
        LogUtils.d(this,"Requesting An Api Data Update With Force Mode Set To " + forceNewSync);
        this.navigationDrawerFragment.lockNavigationDrawer();
        this.displayNewFragment(
            true,
            ApiDataUpdateFragment.create(forceNewSync)
        );
    }

    @Override
    public void onApiDataUpdated(
        long userId
    ) {
        LogUtils.d(this,"API Data Updated");
        this.navigationDrawerFragment.updateForUser(userId);
        this.navigationDrawerFragment.unlockNavigationDrawer();
        this.displayNewFragment(
            true,
            DashboardFragment.create(userId)
        );
    }

    @Override
    public void displayNewFragment(
        boolean replaceBackStack,
        Fragment newFragment
    ) {
        LogUtils.d(this,"Displaying New Fragment With Replacement Mode Set To " + replaceBackStack);
        if (newFragment != null) {
            FragmentManager fragmentManager = this.getFragmentManager();
            if (replaceBackStack) {
                fragmentManager.popBackStackImmediate(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            Fragment currentFragment = fragmentManager.findFragmentById(R.id.drawer_layout_content);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (!replaceBackStack) {
                fragmentTransaction.setCustomAnimations(R.anim.global_fade_in, R.anim.global_fade_out,R.anim.global_fade_in,R.anim.global_fade_out);
            }
            fragmentTransaction.replace(
                    R.id.drawer_layout_content,
                    newFragment
                );
            if (currentFragment != null && !replaceBackStack) {
                fragmentTransaction
                    .addToBackStack(null);
            }
            fragmentTransaction.commit();
            this.updateNavigationDrawer(
                newFragment
            );
        }
    }

    private void updateNavigationDrawer() {
        LogUtils.d(this,"Updating Navigation Drawer");
        FragmentManager fragmentManager = this.getFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.drawer_layout_content);
        this.updateNavigationDrawer(
            currentFragment
        );
    }

    private void updateNavigationDrawer(
        Fragment newFragment
    ) {
        LogUtils.d(this,"Updating Navigation Drawer With Fragment");
        this.navigationDrawerFragment.updateSelection(newFragment);
    }

    private void handleBackStateChanged() {
        LogUtils.d(this,"Handling Back Stack Change");
        this.updateNavigationDrawer();
    }
}
