package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

public class AccountUserAppOption implements IWithId {
    private Long id;
    private Long accountUserAppId;
    private String name;
    private String value;

    public AccountUserAppOption() {}

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(
        Long id
    ) {
        this.id = id;
    }

    public Long getAccountUserAppId() {
        return this.accountUserAppId;
    }

    public void setAccountUserAppId(
        Long accountUserAppId
    ) {
        this.accountUserAppId = accountUserAppId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(
        String name
    ) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(
        String value
    ) {
        this.value = value;
    }
}
