package com.bytesizedhosting.api.android.darthcrap.v3.util;

import android.util.Log;

import com.bytesizedhosting.api.android.darthcrap.v3.BytesizedApplication;

public class LogUtils {
    private LogUtils() {}

    public static void d(String message) {
        Log.d(BytesizedApplication.TAG, message);
    }

    public static void e(String message) {
        Log.e(BytesizedApplication.TAG, message);
    }

    public static void i(String message) {
        Log.i(BytesizedApplication.TAG, message);
    }

    public static void v(String message) {
        Log.v(BytesizedApplication.TAG, message);
    }

    public static void w(String message) {
        Log.w(BytesizedApplication.TAG, message);
    }

    public static void wtf(String message) {
        Log.wtf(BytesizedApplication.TAG, message);
    }

    public static void d(Class<?> source, String message) {
        LogUtils.d(source.getSimpleName() + ": " + message);
    }

    public static void e(Class<?> source, String message) {
        LogUtils.e(source.getSimpleName() + ": " + message);
    }

    public static void i(Class<?> source, String message) {
        LogUtils.i(source.getSimpleName() + ": " + message);
    }

    public static void v(Class<?> source, String message) {
        LogUtils.v(source.getSimpleName() + ": " + message);
    }

    public static void w(Class<?> source, String message) {
        LogUtils.w(source.getSimpleName() + ": " + message);
    }

    public static void wtf(Class<?> source, String message) {
        LogUtils.wtf(source.getSimpleName() + ": " + message);
    }

    public static void d(Object source, String message) {
        LogUtils.d(source.getClass(),message);
    }

    public static void e(Object source, String message) {
        LogUtils.e(source.getClass(),message);
    }

    public static void i(Object source, String message) {
        LogUtils.i(source.getClass(),message);
    }

    public static void v(Object source, String message) {
        LogUtils.v(source.getClass(),message);
    }

    public static void w(Object source, String message) {
        LogUtils.w(source.getClass(),message);
    }

    public static void wtf(Object source, String message) {
        LogUtils.wtf(source.getClass(),message);
    }
}
