package com.bytesizedhosting.api.android.darthcrap.v3;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

public class BytesizedApplication extends Application {
    public static final String TAG = "BYSH-V3";

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }
}
