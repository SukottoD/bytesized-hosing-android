package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountQuotaHistory;

import java.util.List;

public interface IAccountQuotaHistoryDao extends IBasicDao<AccountQuotaHistory> {
    public AccountQuotaHistory selectLatestQuotaForAccount(
        long accountId
    );

    List<AccountQuotaHistory> selectAllWithAccountId(
        long accountId
    );

    List<AccountQuotaHistory> selectWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );

    public AccountQuotaHistory selectSingleWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );
}
