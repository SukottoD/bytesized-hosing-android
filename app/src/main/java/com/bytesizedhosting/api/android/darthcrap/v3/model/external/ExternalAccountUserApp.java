package com.bytesizedhosting.api.android.darthcrap.v3.model.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class ExternalAccountUserApp implements IExternalWithServerId {
    @JsonProperty("_id")
    private ExternalOID id;

    @JsonProperty("app_id")
    private ExternalOID appId;

    @JsonProperty("installed")
    private boolean isInstalled;

    @JsonProperty("installing")
    private boolean isInstalling;

    @JsonProperty("is_deleted")
    private boolean isDeleted;

    @JsonProperty("options")
    private Map<String,Object> options;

    @JsonProperty("app_name")
    private String appName;

    @Override
    public ExternalOID getId() {
        return this.id;
    }

    public void setId(ExternalOID id) {
        this.id = id;
    }

    public ExternalOID getAppId() {
        return this.appId;
    }

    public void setAppId(ExternalOID appId) {
        this.appId = appId;
    }

    public boolean isInstalled() {
        return this.isInstalled;
    }

    public void setInstalled(boolean isInstalled) {
        this.isInstalled = isInstalled;
    }

    public boolean isInstalling() {
        return this.isInstalling;
    }

    public void setInstalling(boolean isInstalling) {
        this.isInstalling = isInstalling;
    }

    public boolean isDeleted() {
        return this.isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Map<String, Object> getOptions() {
        if (this.options == null) {
            return new HashMap<>();
        }
        else {
            return this.options;
        }
    }

    public void setOptions(Map<String, Object> options) {
        if (options == null) {
            this.options = new HashMap<>();
        }
        else {
            this.options = new HashMap<>(options);
        }

    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
