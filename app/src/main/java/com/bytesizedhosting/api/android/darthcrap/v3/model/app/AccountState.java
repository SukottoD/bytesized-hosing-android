package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

public enum AccountState {
    INACTIVE,
    DEPLOYED,
    DISABLED,
    UNKNOWN
}
