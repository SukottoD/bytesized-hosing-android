package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;

public interface IUserDao extends IBasicDao<User> {
    public User selectByApiKey(
        String apiKey
    );
}
