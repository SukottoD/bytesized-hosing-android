package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.List;

public interface IQueryHandler {
    public Cursor select(
        Context context,
        SQLiteDatabase readableDatabase,
        Uri uri,
        List<String> pathSegments,
        String[] projection,
        String selection,
        String[] selectionArgs,
        String sortOrder
    );

    public Uri insert(
        Context context,
        SQLiteDatabase writableDatabase,
        Uri uri,
        List<String> pathSegments,
        ContentValues values
    );

    public int delete(
        Context context,
        SQLiteDatabase writableDatabase,
        Uri uri,
        List<String> pathSegments,
        String selection,
        String[] selectionArgs
    );

    public int update(
        Context context,
        SQLiteDatabase writableDatabase,
        Uri uri,
        List<String> pathSegments,
        ContentValues values,
        String selection,
        String[] selectionArgs
    );
}
