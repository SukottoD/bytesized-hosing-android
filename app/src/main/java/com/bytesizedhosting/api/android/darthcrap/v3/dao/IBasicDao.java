package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import android.content.ContentProviderOperation;

import java.util.List;

public interface IBasicDao<EntityType> {
    List<EntityType> selectAll();

    List<EntityType> selectWithCriteria(
        String whereClause,
        String[] whereParameters,
        String sortClause
    );

    public EntityType selectSingleWithCriteria(
        String whereClause,
        String[] whereParameters,
        String sortClause
    );

    EntityType selectById(
        long id
    );

    long insert(
        EntityType entity
    );

    ContentProviderOperation.Builder insertOperation(
        EntityType entity
    );

    boolean update(
        EntityType entity
    );

    ContentProviderOperation.Builder updateOperation(
        EntityType entity
    );

    boolean update(
        long id,
        EntityType entity
    );

    ContentProviderOperation.Builder updateOperation(
        long id,
        EntityType entity
    );

    boolean delete(
        long id
    );

    ContentProviderOperation.Builder deleteOperation(
        long id
    );

    boolean delete(
        EntityType entity
    );

    ContentProviderOperation.Builder deleteOperation(
        EntityType entity
    );

    int deleteAll();

    boolean checkIfExists(
        long id
    );
}
