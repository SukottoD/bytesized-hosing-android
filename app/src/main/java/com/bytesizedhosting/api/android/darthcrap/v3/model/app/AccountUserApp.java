package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

public class AccountUserApp implements IWithId, IWithServerId {
    private Long id;
    private Long accountId;
    private String serverId;
    private String appServerId;
    private String appName;
    private AccountUserAppState accountUserAppState;

    public AccountUserApp() {}

    public AccountUserAppState getAccountUserAppState() {
        return this.accountUserAppState;
    }

    public void setAccountUserAppState(
        AccountUserAppState accountUserAppState
    ) {
        this.accountUserAppState = accountUserAppState;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(
        Long id
    ) {
        this.id = id;
    }

    public Long getAccountId() {
        return this.accountId;
    }

    public void setAccountId(
        Long accountId
    ) {
        this.accountId = accountId;
    }

    @Override
    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(
        String serverId
    ) {
        this.serverId = serverId;
    }

    public String getAppServerId() {
        return this.appServerId;
    }

    public void setAppServerId(
        String appServerId
    ) {
        this.appServerId = appServerId;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
