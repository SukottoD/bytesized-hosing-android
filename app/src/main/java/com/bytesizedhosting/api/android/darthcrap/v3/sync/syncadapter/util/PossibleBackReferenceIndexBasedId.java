package com.bytesizedhosting.api.android.darthcrap.v3.sync.syncadapter.util;

import android.content.ContentProviderOperation;

import java.util.List;

public class PossibleBackReferenceIndexBasedId {
    private Long id;
    private Integer backReferenceIndex;
    private boolean isBackReferenceIndex;

    private PossibleBackReferenceIndexBasedId(
        Long id,
        Integer backReferenceIndex,
        boolean isBackReferenceIndex
    ) {
        this.id = id;
        this.backReferenceIndex = backReferenceIndex;
        this.isBackReferenceIndex = isBackReferenceIndex;
    }

    public boolean isBackReferenceIndex() {
        return this.isBackReferenceIndex;
    }

    public boolean isId() {
        return !this.isBackReferenceIndex;
    }

    public int getBackReferenceIndex() {
        if (this.isBackReferenceIndex) {
            return this.backReferenceIndex;
        }
        else {
            throw new IllegalStateException("This does not represent a back reference.");
        }
    }

    public long getId() {
        if (this.isBackReferenceIndex) {
            throw new IllegalStateException("This represents a back reference.");
        }
        else {
            return this.id;
        }
    }

    public void applyTo(
        String fieldName,
        ContentProviderOperation.Builder operationBuilder
    ) {
        if (this.isBackReferenceIndex) {
            operationBuilder.withValueBackReference(fieldName,this.backReferenceIndex);
        }
        else {
            operationBuilder.withValue(fieldName,this.id);
        }
    }

    public static PossibleBackReferenceIndexBasedId forBackReference(
        int backReferenceIndex
    ) {
        return new PossibleBackReferenceIndexBasedId(null,backReferenceIndex,true);
    }

    public static PossibleBackReferenceIndexBasedId forBackReference(
        List<ContentProviderOperation> operations,
        ContentProviderOperation newOperation
    ) {
        int index = operations.size();
        operations.add(newOperation);
        return PossibleBackReferenceIndexBasedId.forBackReference(index);
    }

    public static PossibleBackReferenceIndexBasedId forId(
        long id
    ) {
        return new PossibleBackReferenceIndexBasedId(id,null,false);
    }
}
