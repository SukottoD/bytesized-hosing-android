package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util.ContentProviderHelper;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util.NoClauseSelectOnlyQueryHandler;

import java.util.ArrayList;
import java.util.List;

public class BytesizedContentProvider extends ContentProvider {
    private static final ContentProviderHelper CONTENT_PROVIDER_HELPER = BytesizedContentProvider.buildContentProviderHelper();

    private BytesizedSQLiteOpenHelper dbHelper;

    private static ContentProviderHelper buildContentProviderHelper() {
        ContentProviderHelper helper = new ContentProviderHelper(BytesizedContract.AUTHORITY);
        helper.addStandardEntity(
            BytesizedContract.UserEntitySpec                .PATH_ENTITY,
            BytesizedContract.UserEntitySpec                .TABLE_NAME,
            BytesizedContract.UserEntitySpec                .COLUMN_NAME_ID
        );
        helper.addStandardEntity(
            BytesizedContract.AccountEntitySpec             .PATH_ENTITY,
            BytesizedContract.AccountEntitySpec             .TABLE_NAME,
            BytesizedContract.AccountEntitySpec             .COLUMN_NAME_ID
        );
        helper.addStandardEntity(
            BytesizedContract.AccountUserAppEntitySpec      .PATH_ENTITY,
            BytesizedContract.AccountUserAppEntitySpec      .TABLE_NAME,
            BytesizedContract.AccountUserAppEntitySpec      .COLUMN_NAME_ID
        );
        helper.addStandardEntity(
            BytesizedContract.AccountUserAppOptionEntitySpec.PATH_ENTITY,
            BytesizedContract.AccountUserAppOptionEntitySpec.TABLE_NAME,
            BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ID
        );
        helper.addStandardEntity(
            BytesizedContract.AccountQuotaHistoryEntitySpec .PATH_ENTITY,
            BytesizedContract.AccountQuotaHistoryEntitySpec .TABLE_NAME,
            BytesizedContract.AccountQuotaHistoryEntitySpec .COLUMN_NAME_ID
        );
        helper.addStandardEntity(
            BytesizedContract.GravatarEntitySpec            .PATH_ENTITY,
            BytesizedContract.GravatarEntitySpec            .TABLE_NAME,
            BytesizedContract.GravatarEntitySpec            .COLUMN_NAME_ID
        );
        helper.addQuery(
            BytesizedContract.AccountQuotaHistoryEntitySpec.PATH_QUERY_SELECT_CURRENT_QUOTA_FOR_ACCOUNT,
            ContentProviderHelper.ResultType.ITEM,
            BytesizedContract.AccountQuotaHistoryEntitySpec.TABLE_NAME,
            new NoClauseSelectOnlyQueryHandler(
                BytesizedContract.AccountQuotaHistoryEntitySpec.PATH_QUERY_SELECT_CURRENT_QUOTA_FOR_ACCOUNT
            ) {
                public Cursor select(
                    Context context,
                    SQLiteDatabase readableDatabase,
                    Uri uri,
                    List<String> pathParts,
                    String[] projection
                ) {
                    return readableDatabase.query(
                        BytesizedContract.AccountQuotaHistoryEntitySpec.TABLE_NAME,
                        projection,
                        BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_ACCOUNT_ID + " = ?",
                        new String[] {
                            pathParts.get(3),
                        },
                        null,
                        null,
                        BytesizedContract.AccountQuotaHistoryEntitySpec.COLUMN_NAME_TAKEN_AT + " DESC",
                        "0,1"
                    );
                }
            }
        );
        return helper;
    }

    @Override
    public boolean onCreate() {
        this.dbHelper = new BytesizedSQLiteOpenHelper(this.getContext());
        return true;
    }

    @Override
    public ContentProviderResult[] applyBatch(
        ArrayList<ContentProviderOperation> operations
    ) throws OperationApplicationException {
        return BytesizedContentProvider.CONTENT_PROVIDER_HELPER.applyBatch(this, this.dbHelper, operations);
    }

    @Override
    public String getType(Uri uri) {
        return BytesizedContentProvider.CONTENT_PROVIDER_HELPER.getType(uri);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return BytesizedContentProvider.CONTENT_PROVIDER_HELPER.select(this.getContext(), this.dbHelper, uri, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return BytesizedContentProvider.CONTENT_PROVIDER_HELPER.insert(this.getContext(), this.dbHelper, uri, values);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return BytesizedContentProvider.CONTENT_PROVIDER_HELPER.delete(this.getContext(), this.dbHelper, uri, selection, selectionArgs);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return BytesizedContentProvider.CONTENT_PROVIDER_HELPER.update(this.getContext(), this.dbHelper, uri, values, selection, selectionArgs);
    }
}
