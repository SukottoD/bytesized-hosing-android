package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;

public interface IHasFragmentMenuDetails {
    public String getFragmentMenuType();
    public Object getFragmentMenuParameter();
}
