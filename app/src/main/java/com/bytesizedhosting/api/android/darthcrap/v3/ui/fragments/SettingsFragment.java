package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.UserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.ContentResolverContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.authenticator.BytesizedAuthenticator;
import com.bytesizedhosting.api.android.darthcrap.v3.util.LogUtils;

import java.io.IOException;

public class SettingsFragment extends AbstractBaseFragment {
    public static final String ARGUMENT_EXTRA_USER_ID = "user_id";
    public static final String FRAGMENT_MENU_TYPE = "SETTINGS";

    public static SettingsFragment create(long userId) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle arguments = new Bundle();
        arguments.putLong(SettingsFragment.ARGUMENT_EXTRA_USER_ID,userId);
        fragment.setArguments(arguments);
        return fragment;
    }

    public SettingsFragment() {
        super(
            SettingsFragment.FRAGMENT_MENU_TYPE,
            SettingsFragment.ARGUMENT_EXTRA_USER_ID
        );
    }

    @Override
    public View onCreateView(
        LayoutInflater layoutInflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        LogUtils.d(this, "Creating View");
        View settingsView = layoutInflater.inflate(
            R.layout.fragment_settings,
            container,
            false
        );

        settingsView.findViewById(R.id.btnUninstall).setOnClickListener(
            clickedView -> {
                Intent iIntent = new Intent(Intent.ACTION_DELETE);
                iIntent.setData(
                    Uri.parse("package:" + SettingsFragment.this.getActivity().getApplicationContext().getPackageName())
                );
                SettingsFragment.this.startActivity(iIntent);
            }
        );

        UserDao userDao = new UserDao(ContentResolverContentProviderWrapper.create(this));
        User user = userDao.selectById(this.getUserId());

        String currentApiKey = user.getApiKey();
        ((TextView) settingsView.findViewById(R.id.lblAccountApiKeyBody)).setText(currentApiKey);

        settingsView.findViewById(R.id.btnAssociateDifferentAccount).setOnClickListener(
            clickedView -> {
                AccountManager accountManager = AccountManager.get(this.getContext());
                accountManager.updateCredentials(
                    accountManager.getAccountsByType(BytesizedAuthenticator.getAccountType(this.getActivity()))[0],
                    BytesizedAuthenticator.getApiKeyAuthTokenType(this.getContext()),
                    null,
                    this.getActivity(),
                    new AccountManagerCallback<Bundle>() {
                        @Override
                        public void run(AccountManagerFuture<Bundle> future) {
                            try {
                                LogUtils.d(this,"Handling API Key AccountManagerFuture.");
                                Bundle result = future.getResult();
                                String newApiKey = result.getString(AccountManager.KEY_AUTHTOKEN);
                                if (!currentApiKey.equals(newApiKey)) {
                                    SettingsFragment.this.getCoordinationActivity().requestApiDataUpdate(false);
                                }
                            }
                            catch (AuthenticatorException | IOException | OperationCanceledException e) {
                                // Do nothing, we don't care.
                            }
                        }
                    },
                    null
                );
            }
        );

        return settingsView;
    }

    private long getUserId() {
        return this.getArguments().getLong(DashboardFragment.ARGUMENT_EXTRA_USER_ID);
    }

    @Override
    public Object getFragmentMenuParameter() {
        return this.getUserId();
    }
}
