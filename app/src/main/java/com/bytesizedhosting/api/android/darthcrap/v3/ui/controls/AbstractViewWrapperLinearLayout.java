package com.bytesizedhosting.api.android.darthcrap.v3.ui.controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;

public abstract class AbstractViewWrapperLinearLayout extends LinearLayout {
    private boolean inflateFinished = false;
    private int orientation;

    public AbstractViewWrapperLinearLayout(int orientation, Context context) {
        super(context);
        this.orientation = orientation;
    }

    public AbstractViewWrapperLinearLayout(int orientation, Context context, AttributeSet attrs) {
        super(context, attrs);
        this.orientation = orientation;
    }

    public AbstractViewWrapperLinearLayout(int orientation, Context context, AttributeSet attrs, int[] styleableParent) {
        super(context, attrs);
        this.extractAttributes(context, attrs, styleableParent);
        this.orientation = orientation;
    }

    public AbstractViewWrapperLinearLayout(int orientation, Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.orientation = orientation;
    }

    public AbstractViewWrapperLinearLayout(int orientation, Context context, AttributeSet attrs, int defStyleAttr, int[] styleableParent) {
        super(context, attrs, defStyleAttr);
        this.extractAttributes(context, attrs, styleableParent);
        this.orientation = orientation;
    }

    private void extractAttributes(Context context, AttributeSet attrs, int[] styleableParent) {
        TypedArray ta = context.obtainStyledAttributes(attrs, styleableParent);
        this.extractAttributes(context,ta);
        ta.recycle();
    }

    protected void extractAttributes(Context context, TypedArray ta) {}

    protected static interface IVoidContainerOperation {
        public void run(ViewGroup container);
    }

    /*

        this.checkInflateNotFinished();
        super.addView(child,index,params);
     */

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (!this.runContainerOperation((container)->container.addView(child,index,params))) {
            super.addView(child,index,params);
        }
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        if (!this.runContainerOperation((container)->container.addView(child,params))) {
            super.addView(child,params);
        }
    }

    @Override
    public void addView(View child, int index) {
        if (!this.runContainerOperation((container)->container.addView(child,index))) {
            super.addView(child,index);
        }
    }

    @Override
    public void addView(View child) {
        if (!this.runContainerOperation((container)->container.addView(child))) {
            super.addView(child);
        }
    }

    @Override
    public void addView(View child, int width, int height) {
        if (!this.runContainerOperation((container)->container.addView(child,width,height))) {
            super.addView(child,width,height);
        }
    }

    @Override
    public void removeAllViews() {
        if (!this.runContainerOperation((container)->container.removeAllViews())) {
            super.removeAllViews();
        }
    }

    @Override
    public void removeAllViewsInLayout() {
        if (!this.runContainerOperation((container)->container.removeAllViewsInLayout())) {
            super.removeAllViewsInLayout();
        }
    }

    @Override
    public void removeView(View view) {
        if (!this.runContainerOperation((container)->container.removeView(view))) {
            super.removeView(view);
        }
    }

    @Override
    public void removeViewAt(int index) {
        if (!this.runContainerOperation((container)->container.removeViewAt(index))) {
            super.removeViewAt(index);
        }
    }

    @Override
    public void removeViewInLayout(View view) {
        if (!this.runContainerOperation((container)->container.removeViewInLayout(view))) {
            super.removeViewInLayout(view);
        }
    }

    @Override
    public void removeViews(int start, int count) {
        if (!this.runContainerOperation((container)->container.removeViews(start,count))) {
            super.removeViews(start,count);
        }
    }

    @Override
    public void removeViewsInLayout(int start, int count) {
        if (!this.runContainerOperation((container)->container.removeViewsInLayout(start,count))) {
            super.removeViewsInLayout(start,count);
        }
    }

    protected boolean runContainerOperation(
        IVoidContainerOperation voidContainerOperation
    ) {
        if (this.inflateFinished) {
            ViewGroup container = this.getContainer();
            if (container == null) {
                throw new IllegalStateException("This cannot be called after inflate has finished, as there is no designated container.");
            }
            else {
                voidContainerOperation.run(container);
            }
            return true;
        }
        else {
            return false;
        }
    }

    protected void checkChildrenAreValid(View[] children) {}

    protected void wrapChildren(View[] children) {}

    @Override
    protected void onFinishInflate() {
        // Based on 'http://stackoverflow.com/questions/4604066/custom-android-control-with-children'.
        if (this.inflateFinished) {
            throw new IllegalStateException("onFinishInflate has already been run.");
        }

        int childCount = this.getChildCount();
        View[] children = new View[childCount];
        int index = childCount;
        while(--index >= 0) {
            children[index] = getChildAt(index);
        }

        this.checkChildrenAreValid(children);

        this.detachAllViewsFromParent();
        this.setOrientation(this.orientation);

        this.wrapChildren(children);

        this.inflateFinished = true;
        super.onFinishInflate();
    }

    protected void addChild(View[] children, int childIndex, ViewGroup parentViewGroup) {
        if (children.length > childIndex) {
            View childView = children[childIndex];
            GenericUtils.setLinearLayoutParams(childView);
            parentViewGroup.addView(childView);
        }
    }

    protected ViewGroup getContainer() {
        return null;
    }
}
