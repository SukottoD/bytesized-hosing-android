package com.bytesizedhosting.api.android.darthcrap.v3.util.size;

public enum SizeUnitSystem {
    SI_1000 (1000),
    IEC_1024 (1024);

    private int base;

    private SizeUnitSystem(int base) {
        this.base = base;
    }

    public int getBase() {
        return this.base;
    }
}