package com.bytesizedhosting.api.android.darthcrap.v3.ui.controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;

public class BarredPanelView extends AbstractViewWrapperLinearLayout {
    public static final int DEFAULT_BACKGROUND_COLOUR = 0xFFB6C2C9;

    private static final float BAR_COLOUR_MULTIPLIER = 1.0f-0.4f;

    public BarredPanelView(Context context) {
        super(LinearLayout.VERTICAL, context);
    }

    public BarredPanelView(Context context, AttributeSet attrs) {
        super(LinearLayout.VERTICAL, context, attrs, R.styleable.BarredPanelView);
    }

    public BarredPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(LinearLayout.VERTICAL, context, attrs, defStyleAttr, R.styleable.BarredPanelView);
    }

    @Override
    protected void extractAttributes(Context context, TypedArray ta) {
        this.setBackgroundColourInner(ta.getColor(R.styleable.BarredPanelView_backgroundColour,BarredPanelView.DEFAULT_BACKGROUND_COLOUR));
    }

    private int backgroundColour = BarredPanelView.DEFAULT_BACKGROUND_COLOUR;
    private LinearLayout mainView = null;
    private LinearLayout barView = null;

    @Override
    protected void checkChildrenAreValid(View[] children) {
        if (children.length > 2) {
            throw new IllegalStateException(BarredPanelView.class.getSimpleName() + " can only contain 0-2 children.");
        }
    }

    @Override
    protected void wrapChildren(View[] children) {
        this.mainView = this.buildLinearLayout(R.dimen.control_barred_panel_main_padding);
        this.barView  = this.buildLinearLayout(R.dimen.control_barred_panel_bar_padding);

        this.applyBackgroundColor();

        this.addChild(children,0,this.mainView);
        this.addChild(children,1,this.barView);

        this.addView(this.mainView);
        this.addView(this.barView);
    }

    private LinearLayout buildLinearLayout(int paddingDimenResourceId) {
        LinearLayout linearLayout = new LinearLayout(this.getContext());
        GenericUtils.setLinearLayoutParams(linearLayout);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        int padding = this.getResources().getDimensionPixelSize(paddingDimenResourceId);
        linearLayout.setPadding(padding, padding, padding, padding);
        return linearLayout;
    }

    protected final void applyBackgroundColor() {
        if (this.mainView != null && this.barView != null) {
            this.applyBackgroundColor(this.mainView, this.barView);
        }
    }

    private void applyBackgroundColor(LinearLayout mainView, LinearLayout barView) {
        int intMainBackgroundColour = this.backgroundColour;
        int intBarAlpha = (intMainBackgroundColour & 0xFF000000) >> 24;
        int intBarRed   = (intMainBackgroundColour & 0x00FF0000) >> 16;
        int intBarGreen = (intMainBackgroundColour & 0x0000FF00) >>  8;
        int intBarBlue  = (intMainBackgroundColour & 0x000000FF);
        intBarRed *= BarredPanelView.BAR_COLOUR_MULTIPLIER;
        intBarGreen *= BarredPanelView.BAR_COLOUR_MULTIPLIER;
        intBarBlue *= BarredPanelView.BAR_COLOUR_MULTIPLIER;
        int intBarBackgroundColour =
            ((intBarAlpha & 0xFF) << 24)
            |
            ((intBarRed   & 0xFF) << 16)
            |
            ((intBarGreen & 0xFF) <<  8)
            |
            ((intBarBlue  & 0xFF));
        float radius = this.getResources().getDimension(R.dimen.control_barred_panel_radius);
        this.applyBackgroundColor(mainView,radius,     0,intMainBackgroundColour   );
        this.applyBackgroundColor(barView ,     0,radius,intBarBackgroundColour    );
    }

    private void applyBackgroundColor(LinearLayout linearLayout, float topRadiusDip, float bottomRadiusDip, int color) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(new float[]{topRadiusDip,topRadiusDip,topRadiusDip,topRadiusDip,bottomRadiusDip,bottomRadiusDip,bottomRadiusDip,bottomRadiusDip},null,null));
        shapeDrawable.getPaint().setColor(color);
        linearLayout.setBackground(shapeDrawable);
    }

    private void setBackgroundColourInner(int backgroundColour) {
        this.backgroundColour = backgroundColour;
        this.applyBackgroundColor();
    }

    public void setBackgroundColour(int backgroundColour) {
        this.setBackgroundColourInner(backgroundColour);
    }

    public int getBackgroundColour() {
        return this.backgroundColour;
    }
}
