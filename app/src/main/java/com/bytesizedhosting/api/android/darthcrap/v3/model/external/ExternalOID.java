package com.bytesizedhosting.api.android.darthcrap.v3.model.external;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExternalOID {
    @JsonProperty("$oid")
    private String id;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        else if (other == null || this.getClass() != other.getClass()) {
            return false;
        }
        else {
            ExternalOID otherOID = (ExternalOID) other;

            //noinspection RedundantIfStatement
            if (this.id != null ? !this.id.equals(otherOID.id) : otherOID.id != null) {
                return false;
            }

            return true;
        }
    }

    @Override
    public int hashCode() {
        return (this.id != null ? this.id.hashCode() : 0);
    }
}
