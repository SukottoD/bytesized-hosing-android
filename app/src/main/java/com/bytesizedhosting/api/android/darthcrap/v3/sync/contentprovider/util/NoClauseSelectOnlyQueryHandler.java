package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.List;

public abstract class NoClauseSelectOnlyQueryHandler extends QueryHandlerAdapter {
    public NoClauseSelectOnlyQueryHandler(
        String queryName
    ) {
        super(queryName);
    }

    protected abstract Cursor select(
        Context context,
        SQLiteDatabase readableDatabase,
        Uri uri,
        List<String> pathSegments,
        String[] projection
    );

    @Override
    public Cursor select(
        Context context,
        SQLiteDatabase readableDatabase,
        Uri uri,
        List<String> pathSegments,
        String[] projection,
        String selection,
        String[] selectionArgs,
        String sortOrder
    ) {
        if (selection != null) {
            throw new IllegalArgumentException("Select operations on this query do not support selection clauses: " + this.getQueryName());
        }
        else if (selectionArgs != null) {
            throw new IllegalArgumentException("Select operations on this query do not support selection arguments: " + this.getQueryName());
        }
        else if (sortOrder != null) {
            throw new IllegalArgumentException("Select operations on this query do not support sort orders: " + this.getQueryName());
        }
        else {
            return this.select(context,readableDatabase,uri,pathSegments,projection);
        }
    }
}
