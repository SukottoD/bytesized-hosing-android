package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.bytesizedhosting.api.android.darthcrap.v3.BuildConfig;
import com.bytesizedhosting.api.android.darthcrap.v3.BytesizedApplication;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util.TableCreator;
import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.File;
import java.io.IOException;

public class BytesizedSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_FILE_NAME = "bytesized.db";

    private static final int DATABASE_VERSION_RELEASE_3_0_0 = 0;
    private static final int DATABASE_VERSION_RELEASE_3_1_0 = 1;

    private static final int DATABASE_VERSION_CURRENT = BytesizedSQLiteOpenHelper.DATABASE_VERSION_RELEASE_3_1_0;

    private static final String   GRAVATAR_SEED_ASSETS_PATH = "gravatars/seed";
    private static final DateTime GRAVATAR_SEED_DATE_TIME   = new DateTime(2015, 2, 1, 0, 0, 0, DateTimeZone.UTC);

    private static final boolean USE_DEBUG_MODE = false;

    private Context context;

    public BytesizedSQLiteOpenHelper(
        Context context
    ) {
        super(context, BytesizedSQLiteOpenHelper.getDatabasePath(), null, BytesizedSQLiteOpenHelper.DATABASE_VERSION_CURRENT);
        this.context = context;
    }

    private static String getDatabasePath() {
        //noinspection PointlessBooleanExpression
        if (BuildConfig.DEBUG && BytesizedSQLiteOpenHelper.USE_DEBUG_MODE) {
            Log.d(BytesizedApplication.TAG,"Using DEBUG database path");
            File dataDir = new File(Environment.getExternalStorageDirectory(),"data");
            if (!dataDir.exists()) {
                dataDir.mkdir();
            }
            File packageDir = new File(dataDir,BytesizedApplication.class.getPackage().getName());
            if (!packageDir.exists()) {
                packageDir.mkdir();
            }
            File dbFile = new File(packageDir,BytesizedSQLiteOpenHelper.DATABASE_FILE_NAME);
            String path = dbFile.getPath();
            Log.d(BytesizedApplication.TAG,"DEBUG database path is " + path);
            return path;
        }
        else {
            Log.d(BytesizedApplication.TAG,"Using RELEASE database path");
            return BytesizedSQLiteOpenHelper.DATABASE_FILE_NAME;
        }
    }

    @Override
    public void onConfigure (
        SQLiteDatabase db
    ) {
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(
        SQLiteDatabase db
    ) {
       this.onUpgrade(db, BytesizedSQLiteOpenHelper.DATABASE_VERSION_RELEASE_3_0_0, BytesizedSQLiteOpenHelper.DATABASE_VERSION_CURRENT);
    }

    @Override
    public void onUpgrade(
        SQLiteDatabase db,
        int oldVersion,
        int newVersion
    ) {
        if (oldVersion < BytesizedSQLiteOpenHelper.DATABASE_VERSION_RELEASE_3_1_0 && newVersion >= BytesizedSQLiteOpenHelper.DATABASE_VERSION_RELEASE_3_1_0) {
            this.updateToRelease310(db);
        }
    }

    private void updateToRelease310(
        SQLiteDatabase db
    ) {
        this.updateToRelease310CreateTables(db);
        this.updateToRelease310SeedGravatars(db);
    }

    private void updateToRelease310CreateTables(
        SQLiteDatabase db
    ) {
        new TableCreator(BytesizedContract.GravatarEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.GravatarEntitySpec            .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.GravatarEntitySpec            .COLUMN_NAME_GRAVATAR_DIGEST          , "CHARACTER(32) NOT NULL")
            .addColumn(BytesizedContract.GravatarEntitySpec            .COLUMN_NAME_LAST_FETCHED_AT          , "DATETIME"              )
            .addColumn(BytesizedContract.GravatarEntitySpec            .COLUMN_NAME_IMAGE_DATA               , "BLOB"                  )
            .addUniqueConstraint(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_GRAVATAR_DIGEST)
            .execute(db);

        new TableCreator(BytesizedContract.UserEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_API_KEY                  , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_CREATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_EMAIL_ADDRESS            , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_USERNAME                 , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_COUNTRY                  , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_BALANCE_IN_CENTS         , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_GRAVATAR_ID              , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.UserEntitySpec                .COLUMN_NAME_LAST_API_DATA_UPDATE_TIME, "DATETIME NOT NULL"     )
            .addUniqueConstraint(BytesizedContract.UserEntitySpec.COLUMN_NAME_API_KEY)
            .addForeignKey(BytesizedContract.UserEntitySpec.COLUMN_NAME_GRAVATAR_ID, BytesizedContract.GravatarEntitySpec.TABLE_NAME, BytesizedContract.GravatarEntitySpec.COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.AccountEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_USER_ID                  , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_ACCOUNT_STATE            , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_DEPLOYED_AT              , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_PAID_UNTIL               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_RENEWAL_PERIOD_IN_MONTHS , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_BURST_STORAGE_IN_BYTES   , "INTEGER"               )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_PLAN_NAME                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_ACCOUNT_IP               , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_WEB_UI_URL               , "TEXT"                  )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_LOGIN_PASSWORD           , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_SERVER_NAME              , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_TOTAL_BANDWIDTH_IN_BYTES , "INTEGER"               )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_TOTAL_STORAGE_IN_BYTES   , "INTEGER"               )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_ACCOUNT_TYPE             , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountEntitySpec             .COLUMN_NAME_BOOST_BANDWIDTH_IN_BYTES , "INTEGER"               )
            .addUniqueConstraint(BytesizedContract.AccountEntitySpec.COLUMN_NAME_SERVER_ID)
            .addForeignKey(BytesizedContract.AccountEntitySpec.COLUMN_NAME_USER_ID, BytesizedContract.UserEntitySpec.TABLE_NAME, BytesizedContract.UserEntitySpec.COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.AccountUserAppEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.AccountUserAppEntitySpec      .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.AccountUserAppEntitySpec      .COLUMN_NAME_ACCOUNT_ID               , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.AccountUserAppEntitySpec      .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountUserAppEntitySpec      .COLUMN_NAME_ACCOUNT_USER_APP_STATE   , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountUserAppEntitySpec      .COLUMN_NAME_APP_NAME                 , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.AccountUserAppEntitySpec      .COLUMN_NAME_APP_SERVER_ID            , "TEXT NOT NULL"         )
            .addUniqueConstraint(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_SERVER_ID)
            .addForeignKey(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ACCOUNT_ID, BytesizedContract.AccountEntitySpec.TABLE_NAME, BytesizedContract.AccountEntitySpec.COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.AccountUserAppOptionEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID      , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_NAME, "TEXT NOT NULL")
            .addColumn(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_VALUE                    , "TEXT"                  )
            .addUniqueConstraint(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID, BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_NAME)
            .addForeignKey(BytesizedContract.AccountUserAppOptionEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_ID, BytesizedContract.AccountUserAppEntitySpec.TABLE_NAME, BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.AccountQuotaHistoryEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.AccountQuotaHistoryEntitySpec .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.AccountQuotaHistoryEntitySpec .COLUMN_NAME_ACCOUNT_ID               , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.AccountQuotaHistoryEntitySpec .COLUMN_NAME_TAKEN_AT                 , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.AccountQuotaHistoryEntitySpec .COLUMN_NAME_STORAGE_QUOTA_IN_BYTES   , "INTEGER"               )
            .addColumn(BytesizedContract.AccountQuotaHistoryEntitySpec .COLUMN_NAME_BANDWIDTH_QUOTA_IN_BYTES , "INTEGER"               )
            .addForeignKey(BytesizedContract.AccountQuotaHistoryEntitySpec .COLUMN_NAME_ACCOUNT_ID         , BytesizedContract.AccountEntitySpec       .TABLE_NAME, BytesizedContract.AccountEntitySpec       .COLUMN_NAME_ID)
            .execute(db);

        /*
        new TableCreator(BytesizedContract.TicketEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.TicketEntitySpec              .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.TicketEntitySpec              .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.TicketEntitySpec              .COLUMN_NAME_IS_CLOSED                , "BOOLEAN NOT NULL"      )
            .addColumn(BytesizedContract.TicketEntitySpec              .COLUMN_NAME_IS_PRIVATE               , "BOOLEAN NOT NULL"      )
            .addColumn(BytesizedContract.TicketEntitySpec              .COLUMN_NAME_TITLE                    , "TEXT NOT NULL"         )
            .addUniqueConstraint(BytesizedContract.TicketEntitySpec.COLUMN_NAME_SERVER_ID)
            .execute(db);

        new TableCreator(BytesizedContract.TicketPostEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_TICKET_ID                , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_ORDER_NUMBER             , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_BODY                     , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_CREATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_UPDATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_USERNAME                 , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_GRAVATAR_ID              , "INTEGER NOT NULL"      )
            .addUniqueConstraint(BytesizedContract.TicketPostEntitySpec.COLUMN_NAME_SERVER_ID)
            .addForeignKey(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_TICKET_ID          , BytesizedContract.TicketEntitySpec        .TABLE_NAME, BytesizedContract.TicketEntitySpec        .COLUMN_NAME_ID)
            .addForeignKey(BytesizedContract.TicketPostEntitySpec          .COLUMN_NAME_GRAVATAR_ID        , BytesizedContract.GravatarEntitySpec      .TABLE_NAME, BytesizedContract.GravatarEntitySpec      .COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.TicketVisibilityEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.TicketVisibilityEntitySpec    .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.TicketVisibilityEntitySpec    .COLUMN_NAME_USER_ID                  , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.TicketVisibilityEntitySpec    .COLUMN_NAME_TICKET_ID                , "INTEGER NOT NULL"      )
            .addUniqueConstraint(BytesizedContract.TicketVisibilityEntitySpec.COLUMN_NAME_USER_ID, BytesizedContract.TicketVisibilityEntitySpec.COLUMN_NAME_TICKET_ID)
            .addForeignKey(BytesizedContract.TicketVisibilityEntitySpec    .COLUMN_NAME_USER_ID            , BytesizedContract.UserEntitySpec          .TABLE_NAME, BytesizedContract.UserEntitySpec          .COLUMN_NAME_ID)
            .addForeignKey(BytesizedContract.TicketVisibilityEntitySpec    .COLUMN_NAME_TICKET_ID          , BytesizedContract.TicketEntitySpec        .TABLE_NAME, BytesizedContract.TicketEntitySpec        .COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.NewsEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.NewsEntitySpec                .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.NewsEntitySpec                .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.NewsEntitySpec                .COLUMN_NAME_BODY                     , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.NewsEntitySpec                .COLUMN_NAME_CREATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.NewsEntitySpec                .COLUMN_NAME_IS_IMPORTANT             , "BOOLEAN NOT NULL"      )
            .addColumn(BytesizedContract.NewsEntitySpec                .COLUMN_NAME_TITLE                    , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.NewsEntitySpec                .COLUMN_NAME_UPDATED_AT               , "DATETIME NOT NULL"     )
            .addUniqueConstraint(BytesizedContract.NewsEntitySpec.COLUMN_NAME_SERVER_ID)
            .execute(db);

        new TableCreator(BytesizedContract.NewsCommentEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_NEWS_ID                  , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_BODY                     , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_CREATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_UPDATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_USERNAME                 , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_GRAVATAR_ID              , "INTEGER NOT NULL"      )
            .addUniqueConstraint(BytesizedContract.NewsCommentEntitySpec.COLUMN_NAME_SERVER_ID)
            .addForeignKey(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_NEWS_ID            , BytesizedContract.NewsEntitySpec          .TABLE_NAME, BytesizedContract.NewsEntitySpec          .COLUMN_NAME_ID)
            .addForeignKey(BytesizedContract.NewsCommentEntitySpec         .COLUMN_NAME_GRAVATAR_ID        , BytesizedContract.GravatarEntitySpec      .TABLE_NAME, BytesizedContract.GravatarEntitySpec      .COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.InvoiceEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_USER_ID                  , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_CREATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_DUE_AT                   , "DATETIME"              )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_IS_PAID                  , "BOOLEAN NOT NULL"      )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_PAID_AT                  , "DATETIME"              )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_IS_PRO_FORMA             , "BOOLEAN NOT NULL"      )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_TAX_AMOUNT_IN_CENTS      , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_AMOUNT_IN_CENTS          , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_NUMBER                   , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_DESCRIPTION              , "TEXT NOT NULL"         )
            .addUniqueConstraint(BytesizedContract.InvoiceEntitySpec.COLUMN_NAME_SERVER_ID)
            .addForeignKey(BytesizedContract.InvoiceEntitySpec             .COLUMN_NAME_USER_ID            , BytesizedContract.UserEntitySpec          .TABLE_NAME, BytesizedContract.UserEntitySpec          .COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.InvoiceLineEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.InvoiceLineEntitySpec         .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.InvoiceLineEntitySpec         .COLUMN_NAME_INVOICE_ID               , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.InvoiceLineEntitySpec         .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.InvoiceLineEntitySpec         .COLUMN_NAME_BODY                     , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.InvoiceLineEntitySpec         .COLUMN_NAME_INCREASE_IN_CENTS      , "NOT NULL"               )
            .addUniqueConstraint(BytesizedContract.InvoiceLineEntitySpec.COLUMN_NAME_SERVER_ID)
            .addForeignKey(BytesizedContract.InvoiceLineEntitySpec         .COLUMN_NAME_INVOICE_ID         , BytesizedContract.InvoiceEntitySpec       .TABLE_NAME, BytesizedContract.InvoiceEntitySpec       .COLUMN_NAME_ID)
            .execute(db);

        new TableCreator(BytesizedContract.TransactionEntitySpec.TABLE_NAME)
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_ID                       , "INTEGER PRIMARY KEY"   )
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_USER_ID                  , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_SERVER_ID                , "TEXT NOT NULL"         )
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_CREATED_AT               , "DATETIME NOT NULL"     )
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_EXTERNAL_ID              , "TEXT"                  )
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_FRIENDLY_TYPE            , "TEXT"                  )
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_CREDIT_IN_CENTS          , "INTEGER NOT NULL"      )
            .addColumn(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_DEBIT_IN_CENTS           , "INTEGER NOT NULL"      )
            .addUniqueConstraint(BytesizedContract.TransactionEntitySpec.COLUMN_NAME_SERVER_ID)
            .addForeignKey(BytesizedContract.TransactionEntitySpec         .COLUMN_NAME_USER_ID            , BytesizedContract.UserEntitySpec          .TABLE_NAME, BytesizedContract.UserEntitySpec          .COLUMN_NAME_ID)
            .execute(db);
        */
    }

    private void updateToRelease310SeedGravatars(
        SQLiteDatabase db
    ) {
        try {
            AssetManager assetManager = this.context.getAssets();
            String[] seedGravatarNames = assetManager.list(BytesizedSQLiteOpenHelper.GRAVATAR_SEED_ASSETS_PATH);
            for (String seedGravatarName : seedGravatarNames) {
                byte[] imageData = GenericUtils.readAsset(assetManager,BytesizedSQLiteOpenHelper.GRAVATAR_SEED_ASSETS_PATH + "/" + seedGravatarName);
                ContentValues values = new ContentValues();
                values.put(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_GRAVATAR_DIGEST, seedGravatarName.substring(0,seedGravatarName.indexOf("."))    );
                values.put(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_LAST_FETCHED_AT, BytesizedSQLiteOpenHelper.GRAVATAR_SEED_DATE_TIME.getMillis());
                if (imageData.length == 0) {
                    values.putNull(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_IMAGE_DATA);
                }
                else {
                    values.put(BytesizedContract.GravatarEntitySpec.COLUMN_NAME_IMAGE_DATA, imageData);
                }
                db.insertOrThrow(BytesizedContract.GravatarEntitySpec.TABLE_NAME, null, values);
            }
        }
        catch (IOException exIO) {
            throw new IllegalStateException("Unable to seed the gravatars table.",exIO);
        }
    }
}
