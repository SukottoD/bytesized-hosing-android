package com.bytesizedhosting.api.android.darthcrap.v3.sync.syncadapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class BytesizedSyncAdapterService extends Service {
    private static final Object syncAdapterLock = new Object();
    private static BytesizedSyncAdapter syncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (BytesizedSyncAdapterService.syncAdapterLock) {
            if (BytesizedSyncAdapterService.syncAdapter == null) {
                BytesizedSyncAdapterService.syncAdapter = new BytesizedSyncAdapter(this.getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return BytesizedSyncAdapterService.syncAdapter.getSyncAdapterBinder();
    }
}
