package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

import org.joda.time.DateTime;

public class Account implements IWithId, IWithServerId {
    private Long id;
    private Long userId;
    private String serverId;
    private AccountState accountState;
    private DateTime deployedAt;
    private DateTime paidUntil;
    private Integer renewalPeriodInMonths;
    private Long burstStorageInBytes;
    private String planName;
    private String accountIp;
    private String webUIURL;
    private String loginPassword;
    private String serverName;
    private Long totalBandwidthInBytes;
    private Long totalStorageInBytes;
    private AccountType accountType;
    private Long boostBandwidthInBytes;

    public AccountType getAccountType() {
        return this.accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Long getBoostBandwidthInBytes() {
        return this.boostBandwidthInBytes;
    }

    public void setBoostBandwidthInBytes(Long boostBandwidthInBytes) {
        this.boostBandwidthInBytes = boostBandwidthInBytes;
    }

    public Account() {}

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(
        Long id
    ) {
        this.id = id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(
        Long userId
    ) {
        this.userId = userId;
    }

    @Override
    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(
        String serverId
    ) {
        this.serverId = serverId;
    }

    public AccountState getAccountState() {
        return this.accountState;
    }

    public void setAccountState(
        AccountState accountState
    ) {
        this.accountState = accountState;
    }

    public DateTime getDeployedAt() {
        return this.deployedAt;
    }

    public void setDeployedAt(
        DateTime deployedAt
    ) {
        this.deployedAt = deployedAt;
    }

    public DateTime getPaidUntil() {
        return this.paidUntil;
    }

    public void setPaidUntil(
        DateTime paidUntil
    ) {
        this.paidUntil = paidUntil;
    }

    public Integer getRenewalPeriodInMonths() {
        return this.renewalPeriodInMonths;
    }

    public void setRenewalPeriodInMonths(
        Integer renewalPeriodInMonths
    ) {
        this.renewalPeriodInMonths = renewalPeriodInMonths;
    }

    public Long getBurstStorageInBytes() {
        return this.burstStorageInBytes;
    }

    public void setBurstStorageInBytes(
        Long burstStorageInBytes
    ) {
        this.burstStorageInBytes = burstStorageInBytes;
    }

    public String getPlanName() {
        return this.planName;
    }

    public void setPlanName(
        String planName
    ) {
        this.planName = planName;
    }

    public String getAccountIp() {
        return this.accountIp;
    }

    public void setAccountIp(
        String accountIp
    ) {
        this.accountIp = accountIp;
    }

    public String getWebUIURL() {
        return this.webUIURL;
    }

    public void setWebUIURL(
        String webUIURL
    ) {
        this.webUIURL = webUIURL;
    }

    public String getLoginPassword() {
        return this.loginPassword;
    }

    public void setLoginPassword(
        String loginPassword
    ) {
        this.loginPassword = loginPassword;
    }

    public String getServerName() {
        return this.serverName;
    }

    public void setServerName(
        String serverName
    ) {
        this.serverName = serverName;
    }

    public Long getTotalBandwidthInBytes() {
        return this.totalBandwidthInBytes;
    }

    public void setTotalBandwidthInBytes(
        Long totalBandwidthInBytes
    ) {
        this.totalBandwidthInBytes = totalBandwidthInBytes;
    }

    public Long getTotalStorageInBytes() {
        return this.totalStorageInBytes;
    }

    public void setTotalStorageInBytes(
        Long totalStorageInBytes
    ) {
        this.totalStorageInBytes = totalStorageInBytes;
    }
}
