package com.bytesizedhosting.api.android.darthcrap.v3.util.size;

public class Size {
    public static final double NO_ROUND_UP_MODIFIER = 1;

    private double amount;
    private SizeUnit sizeUnit;

    public Size(double amount, SizeUnit sizeUnit) {
        this.amount = amount;
        this.sizeUnit = sizeUnit;
    }

    public double getAmount() {
        return this.amount;
    }

    public SizeUnit getSizeUnit() {
        return this.sizeUnit;
    }

    public static Size createFromBytes(long amountOfBytes, SizeUnitSystem sizeUnitSystem) {
        return Size.createFromBytes(amountOfBytes, sizeUnitSystem, Size.NO_ROUND_UP_MODIFIER);
    }

    public static Size createFromBytes(long amountOfBytes, SizeUnitSystem sizeUnitSystem, double roundUpModifier) {
        if (amountOfBytes < 0) {
            throw new IllegalArgumentException("The amount of bytes must be at least 0.");
        }
        else if (roundUpModifier <= 0) {
            throw new IllegalArgumentException("The round up modifier must be larger than 0.");
        }
        else {
            SizeUnit bestSizeUnit = SizeUnit.BYTE;
            for (SizeUnit sizeUnit : SizeUnit.values()) {
                if (sizeUnit != SizeUnit.BYTE && sizeUnit.getSizeUnitSystem() == sizeUnitSystem && sizeUnit.getAmountOfBytes() > bestSizeUnit.getAmountOfBytes()) {
                    double modifiedSizeUnitAmountOfBytes = sizeUnit.getAmountOfBytes()*roundUpModifier;
                    if (amountOfBytes >= modifiedSizeUnitAmountOfBytes) {
                        bestSizeUnit = sizeUnit;
                    }
                }
            }
            return new Size((double)amountOfBytes/(double)bestSizeUnit.getAmountOfBytes(),bestSizeUnit);
        }
    }
}