package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;

public class ContentProviderClientContentProviderWrapper implements IContentProviderWrapper {
    private ContentProviderClient contentProviderClient;

    public ContentProviderClientContentProviderWrapper(
        ContentProviderClient contentProviderClient
    ) {
        if (contentProviderClient == null) {
            throw new IllegalArgumentException("The content provider client must not be NULL.");
        }
        else {
            this.contentProviderClient = contentProviderClient;
        }
    }

    @Override
    public String getType(Uri uri) {
        try {
            return this.contentProviderClient.getType(uri);
        }
        catch (RemoteException exRemote) {
            throw new IllegalStateException(exRemote);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        try {
            return this.contentProviderClient.query(uri, projection, selection, selectionArgs, sortOrder);
        }
        catch (RemoteException exRemote) {
            throw new IllegalStateException(exRemote);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        try {
            return this.contentProviderClient.insert(uri, values);
        }
        catch (RemoteException exRemote) {
            throw new IllegalStateException(exRemote);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        try {
            return this.contentProviderClient.delete(uri, selection, selectionArgs);
        }
        catch (RemoteException exRemote) {
            throw new IllegalStateException(exRemote);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        try {
            return this.contentProviderClient.update(uri, values, selection, selectionArgs);
        }
        catch (RemoteException exRemote) {
            throw new IllegalStateException(exRemote);
        }
    }
}
