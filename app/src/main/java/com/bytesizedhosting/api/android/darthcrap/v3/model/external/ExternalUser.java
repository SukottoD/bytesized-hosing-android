package com.bytesizedhosting.api.android.darthcrap.v3.model.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExternalUser implements IExternalWithGravatarDigest {
    @JsonProperty("created_at")
    private DateTime createdAt;

    @JsonProperty("email")
    private String email;

    @JsonProperty("login")
    private String login;

    @JsonProperty("country_as_string")
    private String countryAsString;

    @JsonProperty("current_balance")
    private String currentBalanceAsString;

    @JsonProperty("number_of_active_accounts")
    private String numberOfActiveAccounts;

    @JsonProperty("gravatar_digest")
    private String gravatarDigest;

    @JsonProperty("active_accounts")
    private List<ExternalAccount> activeAccounts;

    public ExternalUser() {}

    public DateTime getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCountryAsString() {
        return this.countryAsString;
    }

    public void setCountryAsString(String countryAsString) {
        this.countryAsString = countryAsString;
    }

    public String getCurrentBalanceAsString() {
        return this.currentBalanceAsString;
    }

    public void setCurrentBalanceAsString(String currentBalanceAsString) {
        this.currentBalanceAsString = currentBalanceAsString;
    }

    @Override
    public String getGravatarDigest() {
        return this.gravatarDigest;
    }

    public void setGravatarDigest(String gravatarDigest) {
        this.gravatarDigest = gravatarDigest;
    }

    public String getNumberOfActiveAccounts() {
        return this.numberOfActiveAccounts;
    }

    public void setNumberOfActiveAccounts(String numberOfActiveAccounts) {
        this.numberOfActiveAccounts = numberOfActiveAccounts;
    }

    public List<ExternalAccount> getActiveAccounts() {
        if (this.activeAccounts == null) {
            return new ArrayList<>();
        }
        else {
            return Collections.unmodifiableList(this.activeAccounts);
        }
    }

    public void setActiveAccounts(List<ExternalAccount> activeAccounts) {
        if (activeAccounts == null) {
            this.activeAccounts = new ArrayList<>();
        }
        else {
            this.activeAccounts = new ArrayList<>(activeAccounts);
        }
    }
}
