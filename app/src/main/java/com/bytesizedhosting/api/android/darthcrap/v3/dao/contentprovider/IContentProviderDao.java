package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IBasicDao;

import java.util.Map;

public interface IContentProviderDao<EntityType> extends IBasicDao<EntityType> {
    public String getQueryStringParameter(
        String key
    );

    public void setQueryStringParameter(
        String key,
        String value
    );

    public void removeQueryStringParameter(
        String key
    );

    public Map<String,String> getQueryStringParameter();

    public boolean isPartiallyEqual(
        EntityType first,
        EntityType second
    );
}
