package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IUserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.UserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.ContentResolverContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.authenticator.BytesizedAuthenticator;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;
import com.bytesizedhosting.api.android.darthcrap.v3.util.IntentUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.LogUtils;

import org.joda.time.DateTime;

import java.io.IOException;

public class ApiDataUpdateFragment extends AbstractFragmentInteractionBaseFragment<ApiDataUpdateFragment.FragmentInteractionListener> {
    public static final String FRAGMENT_MENU_TYPE = "api_data_update";
    public static final String ARGUMENT_EXTRA_FORCE_SYNC = "force_sync";
    private boolean startedUpdate = false;

    public ApiDataUpdateFragment() {
        super(
            ApiDataUpdateFragment.FRAGMENT_MENU_TYPE,
            ApiDataUpdateFragment.FragmentInteractionListener.class,
            ApiDataUpdateFragment.ARGUMENT_EXTRA_FORCE_SYNC
        );
    }

    private static class ButtonSpec {
        private int textStringResourceId;
        private Runnable clickRunnable;

        private ButtonSpec(int textStringResourceId, Runnable clickRunnable) {
            this.textStringResourceId = textStringResourceId;
            this.clickRunnable = clickRunnable;
        }

        public int getTextStringResourceId() {
            return this.textStringResourceId;
        }

        public Runnable getClickRunnable() {
            return this.clickRunnable;
        }
    }

    private static interface IntentRunnable {
        public void run(Intent intent);
    }

    private TextView lblMessage;
    private LinearLayout llButtons;
    private ViewGroup vgLoading;
    private ViewGroup vgError;
    private AccountManager accountManager;
    private boolean forceNewSync;
    private IntentRunnable intentRunnable = null;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(
            Context context,
            android.content.Intent intent
        ) {
            LogUtils.d(this,"Handling sync completed intent.");
            ApiDataUpdateFragment.this.handleSyncCompletedIntent(intent);
        }
    };

    @Override
    public void onPause() {
        LogUtils.d(this,"Pausing.");
        this.getActivity().unregisterReceiver(this.broadcastReceiver);
        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtils.d(this,"Resuming.");
        super.onResume();
        this.getActivity().registerReceiver(this.broadcastReceiver, new IntentFilter(IntentUtils.INTENT_SYNC_COMPLETED));
    }

    public static ApiDataUpdateFragment create(
        boolean forceNewSync
    ) {
        ApiDataUpdateFragment fragment = new ApiDataUpdateFragment();
        Bundle arguments = new Bundle();
        arguments.putBoolean(ApiDataUpdateFragment.ARGUMENT_EXTRA_FORCE_SYNC,forceNewSync);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public View onCreateView(
        LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        LogUtils.d(this,"Creating View.");
        View vRoot = inflater.inflate(
            R.layout.fragment_api_data_update,
            container,
            false
        );

        this.lblMessage = (TextView)vRoot.findViewById(R.id.lblMessage);
        this.llButtons = (LinearLayout)vRoot.findViewById(R.id.llButtons);
        this.vgError = (ViewGroup)vRoot.findViewById(R.id.rlError);
        this.vgLoading = (ViewGroup)vRoot.findViewById(R.id.llLoading);

        this.accountManager = AccountManager.get(this.getActivity());
        this.forceNewSync = this.getArguments().getBoolean(ApiDataUpdateFragment.ARGUMENT_EXTRA_FORCE_SYNC);
        this.startUpdate();

        return vRoot;
    }

    private void startUpdate() {
        if (this.startedUpdate) {
            LogUtils.d(this, "Could not start update as one has already started, or has completed.");
        }
        else {
            LogUtils.d(this, "Starting Update.");
            this.startedUpdate = true;
            this.fetchAccount();
        }
    }

    private void fetchAccount() {
        LogUtils.d(this, "Fetching Account.");
        this.displayLoadingIndicator();
        Account[] accounts = this.accountManager.getAccountsByType(BytesizedAuthenticator.getAccountType(this.getActivity()));
        if (accounts == null || accounts.length < 1) {
            this.displayError(
                R.string.section_api_data_update_error_no_account,
                new ButtonSpec(
                    R.string.section_api_data_update_button_create_account,
                    this::createAccount
                )
            );
        }
        else if (accounts.length > 1) {
            this.displayError(
                R.string.section_api_data_update_error_too_many_accounts
            );
        }
        else {
            this.fetchApiKey(accounts[0]);
        }
    }

    private void fetchApiKey(
        Account account
    ) {
        LogUtils.d(this, "Fetching API Key.");
        this.displayLoadingIndicator();
        this.accountManager.getAuthToken(
            account,
            BytesizedAuthenticator.getApiKeyAuthTokenType(this.getContext()),
            null,
            this.getActivity(),
            future -> {
                try {
                    LogUtils.d(this,"Handling API Key AccountManagerFuture.");
                    Bundle result = future.getResult();
                    this.useApiKey(account,result.getString(AccountManager.KEY_AUTHTOKEN));
                }
                catch (AuthenticatorException | IOException e) {
                    ApiDataUpdateFragment.this.displayRetryError(
                        R.string.section_api_data_update_error_could_not_retrieve_api_key
                    );
                }
                catch (OperationCanceledException e) {
                    ApiDataUpdateFragment.this.displayRetryError(
                        R.string.section_api_data_update_error_api_key_retrieval_cancelled
                    );
                }
            },
            null
        );
    }

    private void createAccount() {
        LogUtils.d(this,"Creating Account.");
        this.displayLoadingIndicator();
        this.accountManager.addAccount(
            BytesizedAuthenticator.getAccountType(this.getContext()),
            BytesizedAuthenticator.getApiKeyAuthTokenType(this.getContext()),
            null,
            null,
            this.getActivity(),
            future -> {
                try {
                    LogUtils.d(this,"Handling Create Account AccountManagerFuture.");
                    future.getResult();
                    this.fetchAccount();
                }
                catch (AuthenticatorException | IOException e) {
                    ApiDataUpdateFragment.this.displayRetryError(
                        R.string.section_api_data_update_error_could_not_create_account
                    );
                }
                catch (OperationCanceledException e) {
                    ApiDataUpdateFragment.this.displayRetryError(
                        R.string.section_api_data_update_error_account_creation_cancelled
                    );
                }
            },
            null
        );
    }

    private void useApiKey(
        Account account,
        String apiKey
    ) {
        LogUtils.d(this,"Using API Key (No Information Yet).");
        this.displayLoadingIndicator();
        User currentUser = this.getCurrentUser(apiKey);
        LogUtils.d(this,"Using API Key, ApiKey=" + apiKey + ", ForceNewSync=" + this.forceNewSync + ", CurrentUser=" + this.isNullForLog(currentUser) + ".");
        if (!this.forceNewSync && currentUser != null) {
            this.finishUpdate(currentUser);
        }
        else {
            this.startSync(account,currentUser,apiKey);
        }
    }

    private void startSync(
        Account account,
        User currentUser,
        String apiKey
    ) {
        LogUtils.d(this,"Starting Sync.");
        this.displayLoadingIndicator();
        this.intentRunnable = (intent) -> this.finishSync(currentUser, apiKey, intent);
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_IGNORE_SETTINGS, true);
        ContentResolver.requestSync(account, BytesizedContract.AUTHORITY, bundle);
    }

    private void handleSyncCompletedIntent(
        Intent intent
    ) {
        LogUtils.d(this,"Handling Sync Completed Intent, IntentRunnable=" + this.isNullForLog(this.intentRunnable) + ".");
        if (this.intentRunnable != null) {
            this.intentRunnable.run(intent);
        }
    }

    private void finishSync(
        User oldUser,
        String apiKey,
        Intent intent
    ) {
        LogUtils.d(this,"Finishing Sync, ApiKey=" + apiKey + ", OldUser=" + this.isNullForLog(oldUser) + ".");
        this.intentRunnable = null;
        if (intent.getBooleanExtra(IntentUtils.INTENT_SYNC_COMPLETE_EXTRA_WAS_SUCCESSFUL, false)) {
            User newUser = this.getCurrentUser(apiKey);
            DateTime oldFetchTime = (oldUser == null)? null : oldUser.getLastApiDataUpdateTime();
            DateTime newFetchTime = (newUser == null)? null : newUser.getLastApiDataUpdateTime();
            if (newFetchTime != null && !newFetchTime.equals(oldFetchTime)) {
                this.syncWorked(newUser);
            }
            else {
                this.syncFailed(oldUser);
            }
        }
        else {
            this.syncFailed(oldUser);
        }
    }

    private void syncWorked(
        User newUser
    ) {
        LogUtils.d(this,"Sync Worked.");
        this.finishUpdate(newUser);
    }

    private void syncFailed(
        User oldUser
    ) {
        LogUtils.d(this,"Sync Failed.");
        if (oldUser != null) {
            if (this.forceNewSync) {
                this.displayError(
                    R.string.section_api_data_update_error_could_not_sync,
                    this.createRetryButton(),
                    new ButtonSpec(
                        R.string.section_api_data_update_button_ignore_sync_failure,
                        () -> this.finishUpdate(oldUser)
                    )
                );
            }
            else {
                this.finishUpdate(oldUser);
            }
        }
        else {
            this.displayRetryError(
                R.string.section_api_data_update_error_could_not_sync
            );
        }
    }

    private void finishUpdate(
        User currentUser
    ) {
        LogUtils.d(this,"Finishing Update.");
        this.getFragmentInteractionListener().onApiDataUpdated(currentUser.getId());
    }

    private User getCurrentUser(
        String apiKey
    ) {
        IUserDao userDao = new UserDao(ContentResolverContentProviderWrapper.create(this));
        return userDao.selectByApiKey(apiKey);
    }

    private void displayLoadingIndicator() {
        this.displayViewGroups(true);
    }

    private void displayViewGroups(
        boolean displayLoading
    ) {
        this.vgLoading.setVisibility(displayLoading?View.VISIBLE:View.GONE   );
        this.vgError  .setVisibility(displayLoading?View.GONE   :View.VISIBLE);
    }

    private void displayRetryError(
        int messageStringResourceId
    ) {
        this.displayError(
            messageStringResourceId,
            this.createRetryButton()
        );
    }

    private ButtonSpec createRetryButton() {
        return new ButtonSpec(
            R.string.section_api_data_update_button_retry,
            this::fetchAccount
        );
    }

    private void displayError(
        int messageStringResourceId,
        ButtonSpec... buttons
    ) {
        this.lblMessage.setText(messageStringResourceId);
        this.llButtons.removeAllViews();
        this.addSpace(llButtons);
        for (ButtonSpec buttonSpec : buttons) {
            this.addButton(this.llButtons,buttonSpec);
            this.addSpace(this.llButtons);
        }
        this.displayViewGroups(false);
    }

    private void addButton(
        LinearLayout linearLayout,
        ButtonSpec buttonSpec
    ) {
        Button button = new Button(linearLayout.getContext());
        button.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        button.setText(buttonSpec.getTextStringResourceId());
        button.setOnClickListener(innerButton -> buttonSpec.getClickRunnable().run());
        linearLayout.addView(button);
    }

    private void addSpace(
        LinearLayout linearLayout
    ) {
        Space space = new Space(linearLayout.getContext());
        space.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
        linearLayout.addView(space);
    }

    @Override
    public Object getFragmentMenuParameter() {
        return this.forceNewSync;
    }

    public static interface FragmentInteractionListener {
        public void onApiDataUpdated(
            long userId
        );
    }

    private String isNullForLog(
        Object object
    ) {
        return (object==null?"Null":"Non-Null");
    }
}
