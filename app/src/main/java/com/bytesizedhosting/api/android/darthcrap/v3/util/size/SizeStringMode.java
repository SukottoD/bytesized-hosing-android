package com.bytesizedhosting.api.android.darthcrap.v3.util.size;

public enum SizeStringMode {
    STANDARD,
    ABBREVIATION,
    ABBREVIATION_FORCE_SINGULAR,
}