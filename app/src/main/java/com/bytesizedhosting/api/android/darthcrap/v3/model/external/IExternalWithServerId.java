package com.bytesizedhosting.api.android.darthcrap.v3.model.external;

public interface IExternalWithServerId {
    public ExternalOID getId();
}
