package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util;

import android.content.ContentValues;
import android.database.Cursor;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class FieldMapping<EntityType> {
    public static interface IEntityGetter<EntityType,ModelPropertyType> {
        ModelPropertyType getFromEntity(EntityType entity);
    }

    public static interface IEntitySetter<EntityType,ModelPropertyType> {
        void setToEntity(EntityType entity, ModelPropertyType value);
    }

    public static interface ICursorGetter<DatabasePropertyType> {
        DatabasePropertyType getFromCursor(Cursor cursor, int index);
    }

    public static interface IContentValuesSetter<DatabasePropertyType> {
        void setToContentValues(ContentValues contentValues, String name, DatabasePropertyType value);
    }

    public static interface IConverter<InputType,OutputType> {
        OutputType convert(InputType input);
    }

    private static interface IFieldFromCursorReader<EntityType> {
        public void readFieldFromCursor(EntityType entity, Cursor cursor);
    }

    private static interface IFieldToContentValuesWriter<EntityType> {
        public void writeEntityFieldToContentValues(EntityType entity, ContentValues contentValues);
    }

    private String fieldName;
    private IFieldFromCursorReader<EntityType> fieldFromCursorReader;
    private IFieldToContentValuesWriter<EntityType> fieldToContentValuesWriter;
    private IEntityGetter<EntityType,?> entityGetter;

    public <ModelPropertyType,DatabasePropertyType> FieldMapping(
        String fieldName,
        IEntityGetter<EntityType,ModelPropertyType> entityGetter,
        IEntitySetter<EntityType,ModelPropertyType> entitySetter,
        ICursorGetter<DatabasePropertyType> cursorGetter,
        IContentValuesSetter<DatabasePropertyType> contentValuesSetter,
        IConverter<ModelPropertyType,DatabasePropertyType> modelToDatabaseConverter,
        IConverter<DatabasePropertyType,ModelPropertyType> databaseToModelConverter
    ) {
        this.fieldName = fieldName;
        this.fieldFromCursorReader = (EntityType entity, Cursor cursor) -> {
            int columnIndex =
                cursor.getColumnIndexOrThrow(
                    fieldName
                );
            DatabasePropertyType databaseProperty;
            if (cursor.isNull(columnIndex)) {
                databaseProperty = null;
            }
            else {
                databaseProperty =
                    cursorGetter.getFromCursor(
                        cursor,
                        columnIndex
                    );
            }
            entitySetter.setToEntity(
                entity,
                databaseToModelConverter.convert(
                    databaseProperty
                )
            );
        };
        this.fieldToContentValuesWriter = (EntityType entity, ContentValues contentValues) -> {
            DatabasePropertyType databaseProperty =
                modelToDatabaseConverter.convert(
                    entityGetter.getFromEntity(
                        entity
                    )
                );
            if (databaseProperty == null) {
                contentValues.putNull(
                    fieldName
                );
            }
            else {
                contentValuesSetter.setToContentValues(
                    contentValues,
                    fieldName,
                    databaseProperty
                );
            }
        };
        this.entityGetter = entityGetter;
    }

    public void readFieldFromCursor(EntityType entity, Cursor cursor) {
        this.fieldFromCursorReader.readFieldFromCursor(entity,cursor);
    }

    public void writeEntityFieldToContentValues(EntityType entity, ContentValues contentValues) {
        this.fieldToContentValuesWriter.writeEntityFieldToContentValues(entity,contentValues);
    }

    public Object getValueAsObject(EntityType entity) {
        return this.entityGetter.getFromEntity(entity);
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public static <EntityType,PropertyType> FieldMapping<EntityType> create(
        String fieldName,
        IEntityGetter<EntityType,PropertyType> entityGetter,
        IEntitySetter<EntityType,PropertyType> entitySetter,
        ICursorGetter<PropertyType> cursorGetter,
        IContentValuesSetter<PropertyType> contentValuesSetter
    ) {
       return FieldMapping.create(
            fieldName,
            entityGetter,
            entitySetter,
            cursorGetter,
            contentValuesSetter,
            property -> property, // Function.identity() does not come with retrolambda.
            property -> property
        );
    }

    public static <EntityType> FieldMapping<EntityType> create(
        String fieldName,
        IEntityGetter<EntityType,DateTime> entityGetter,
        IEntitySetter<EntityType,DateTime> entitySetter
    ) {
        return FieldMapping.create(
            fieldName,
            entityGetter,
            entitySetter,
            Cursor::getLong,
            ContentValues::put,
            DateTime::getMillis,
            millis -> new DateTime(millis, DateTimeZone.UTC)
        );
    }

    public static <EntityType,EnumType extends Enum<EnumType>> FieldMapping<EntityType> create(
        String fieldName,
        IEntityGetter<EntityType,EnumType> entityGetter,
        IEntitySetter<EntityType,EnumType> entitySetter,
        Class<EnumType> enumClass
    ) {
        return FieldMapping.create(
            fieldName,
            entityGetter,
            entitySetter,
            Cursor::getString,
            ContentValues::put,
            EnumType::toString,
            (stringValue) -> Enum.valueOf(enumClass, stringValue)
        );
    }

    public static <EntityType,ModelPropertyType,DatabasePropertyType> FieldMapping<EntityType> create(
        String fieldName,
        IEntityGetter<EntityType,ModelPropertyType> entityGetter,
        IEntitySetter<EntityType,ModelPropertyType> entitySetter,
        ICursorGetter<DatabasePropertyType> cursorGetter,
        IContentValuesSetter<DatabasePropertyType> contentValuesSetter,
        IConverter<ModelPropertyType,DatabasePropertyType> modelToDatabaseConverter,
        IConverter<DatabasePropertyType,ModelPropertyType> databaseToModelConverter
    ) {
        return new FieldMapping<>(
            fieldName,
            entityGetter,
            entitySetter,
            cursorGetter,
            contentValuesSetter,
            modelToDatabaseConverter,
            databaseToModelConverter
        );
    }
}
