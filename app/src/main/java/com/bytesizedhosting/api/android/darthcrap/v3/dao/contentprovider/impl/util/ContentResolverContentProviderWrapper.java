package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;

public class ContentResolverContentProviderWrapper implements IContentProviderWrapper {
    private ContentResolver contentResolver;

    public ContentResolverContentProviderWrapper(
        ContentResolver contentResolver
    ) {
        if (contentResolver == null) {
            throw new IllegalArgumentException("The content provider client must not be NULL.");
        }
        else {
            this.contentResolver = contentResolver;
        }
    }

    @Override
    public String getType(Uri uri) {
        return this.contentResolver.getType(uri);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return this.contentResolver.query(uri, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return this.contentResolver.insert(uri, values);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return this.contentResolver.delete(uri, selection, selectionArgs);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return this.contentResolver.update(uri, values, selection, selectionArgs);
    }

    public static ContentResolverContentProviderWrapper create(
        ContentResolver contentResolver
    ) {
        return new ContentResolverContentProviderWrapper(contentResolver);
    }

    public static ContentResolverContentProviderWrapper create(
        Context context
    ) {
        return ContentResolverContentProviderWrapper.create(context.getContentResolver());
    }

    public static ContentResolverContentProviderWrapper create(
        Activity activity
    ) {
        return ContentResolverContentProviderWrapper.create(activity.getContentResolver());
    }

    public static ContentResolverContentProviderWrapper create(
        Fragment fragment
    ) {
        return ContentResolverContentProviderWrapper.create(fragment.getActivity());
    }

    public static ContentResolverContentProviderWrapper create(
        View view
    ) {
        return ContentResolverContentProviderWrapper.create(view.getContext());
    }
}
