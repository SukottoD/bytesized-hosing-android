package com.bytesizedhosting.api.android.darthcrap.v3.sync.authenticator;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class BytesizedAuthenticatorService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return new BytesizedAuthenticator(this).getIBinder();
    }
}
