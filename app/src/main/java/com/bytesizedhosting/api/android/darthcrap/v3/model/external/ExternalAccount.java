package com.bytesizedhosting.api.android.darthcrap.v3.model.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExternalAccount implements IExternalWithServerId {
    @JsonProperty("_id")
    private ExternalOID id;

    @JsonProperty("bandwidth_quota")
    private long bandwidthQuotaInBytes;

    @JsonProperty("deployed")
    private boolean isDeployed;

    @JsonProperty("deployed_on")
    private DateTime deployedOn;

    @JsonProperty("disabled")
    private boolean isDisabled;

    @JsonProperty("disk_quota")
    private double diskQuotaInKiloBytes;

    @JsonProperty("paid_till")
    private DateTime paidUntil;

    @JsonProperty("renew_period")
    private int renewalPeriodInMonths;

    @JsonProperty("burst_amount")
    private Double burstAmountInGigabytes;

    @JsonProperty("plan_name")
    private String planName;

    @JsonProperty("account_ip")
    private String accountIp;

    @JsonProperty("web_ui")
    private String webUIURL;

    @JsonProperty("disk_quota_percentage")
    private String diskQuotaPercentage;

    @JsonProperty("total_bandwidth")
    private Double totalBandwidthInTebibytes;

    @JsonProperty("total_storage")
    private Double totalStorageInGigabytes;

    @JsonProperty("server_name")
    private String serverName;

    @JsonProperty("login_password")
    private String loginPassword;

    @JsonProperty("pretty_bw_quota")
    private String prettyBandwidthQuota;

    @JsonProperty("pretty_disk_quota")
    private String prettyDiskQuota;

    @JsonProperty("type")
    private String accountType;

    @JsonProperty("active_boosts")
    private Double activeBandwidthBoostsInTebibytes;

    @JsonProperty("user_apps")
    private List<ExternalAccountUserApp> userApps;

    @Override
    public ExternalOID getId() {
        return this.id;
    }

    public void setId(ExternalOID id) {
        this.id = id;
    }

    public long getBandwidthQuotaInBytes() {
        return this.bandwidthQuotaInBytes;
    }

    public void setBandwidthQuotaInBytes(long bandwidthQuotaInBytes) {
        this.bandwidthQuotaInBytes = bandwidthQuotaInBytes;
    }

    public boolean isDeployed() {
        return this.isDeployed;
    }

    public void setDeployed(boolean isDeployed) {
        this.isDeployed = isDeployed;
    }

    public DateTime getDeployedOn() {
        return this.deployedOn;
    }

    public void setDeployedOn(DateTime deployedOn) {
        this.deployedOn = deployedOn;
    }

    public boolean isDisabled() {
        return this.isDisabled;
    }

    public void setDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public double getDiskQuotaInKiloBytes() {
        return this.diskQuotaInKiloBytes;
    }

    public void setDiskQuotaInKiloBytes(double diskQuotaInKiloBytes) {
        this.diskQuotaInKiloBytes = diskQuotaInKiloBytes;
    }

    public DateTime getPaidUntil() {
        return this.paidUntil;
    }

    public void setPaidUntil(DateTime paidUntil) {
        this.paidUntil = paidUntil;
    }

    public int getRenewalPeriodInMonths() {
        return this.renewalPeriodInMonths;
    }

    public void setRenewalPeriodInMonths(int renewalPeriodInMonths) {
        this.renewalPeriodInMonths = renewalPeriodInMonths;
    }

    public Double getBurstAmountInGigabytes() {
        return this.burstAmountInGigabytes;
    }

    public void setBurstAmountInGigabytes(Double burstAmountInGigabytes) {
        this.burstAmountInGigabytes = burstAmountInGigabytes;
    }

    public String getPlanName() {
        return this.planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getAccountIp() {
        return this.accountIp;
    }

    public void setAccountIp(String accountIp) {
        this.accountIp = accountIp;
    }

    public String getWebUIURL() {
        return this.webUIURL;
    }

    public void setWebUIURL(String webUIURL) {
        this.webUIURL = webUIURL;
    }

    public String getDiskQuotaPercentage() {
        return this.diskQuotaPercentage;
    }

    public void setDiskQuotaPercentage(String diskQuotaPercentage) {
        this.diskQuotaPercentage = diskQuotaPercentage;
    }

    public Double getTotalBandwidthInTebibytes() {
        return this.totalBandwidthInTebibytes;
    }

    public void setTotalBandwidthInTebibytes(Double totalBandwidthInTebibytes) {
        this.totalBandwidthInTebibytes = totalBandwidthInTebibytes;
    }

    public Double getTotalStorageInGigabytes() {
        return this.totalStorageInGigabytes;
    }

    public void setTotalStorageInGigabytes(Double totalStorageInGigabytes) {
        this.totalStorageInGigabytes = totalStorageInGigabytes;
    }

    public String getServerName() {
        return this.serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getLoginPassword() {
        return this.loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getPrettyBandwidthQuota() {
        return this.prettyBandwidthQuota;
    }

    public void setPrettyBandwidthQuota(String prettyBandwidthQuota) {
        this.prettyBandwidthQuota = prettyBandwidthQuota;
    }

    public String getPrettyDiskQuota() {
        return this.prettyDiskQuota;
    }

    public void setPrettyDiskQuota(String prettyDiskQuota) {
        this.prettyDiskQuota = prettyDiskQuota;
    }

    public List<ExternalAccountUserApp> getUserApps() {
        if (this.userApps == null) {
            return new ArrayList<>();
        }
        else {
            return Collections.unmodifiableList(this.userApps);
        }
    }

    public void setUserApps(List<ExternalAccountUserApp> userApps) {
        if (userApps == null) {
            this.userApps = new ArrayList<>();
        }
        else {
            this.userApps = new ArrayList<>(userApps);
        }
    }

    public String getAccountType() {
        return this.accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Double getActiveBandwidthBoostsInTebibytes() {
        return this.activeBandwidthBoostsInTebibytes;
    }

    public void setActiveBandwidthBoostsInTebibytes(Double activeBandwidthBoostsInTebibytes) {
        this.activeBandwidthBoostsInTebibytes = activeBandwidthBoostsInTebibytes;
    }
}
