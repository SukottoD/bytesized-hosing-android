package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl;

import android.content.ContentValues;
import android.database.Cursor;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.FieldMapping;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Account;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountState;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountType;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;

import java.util.Arrays;
import java.util.List;

public class AccountDao extends AbstractContentProviderDao<Account> implements IAccountDao {
    public AccountDao(
        IContentProviderWrapper contentProviderWrapper
    ) {
        super(
            contentProviderWrapper,
            Account::new,
            BytesizedContract.AccountEntitySpec.PATH_ENTITY,
            Arrays.<FieldMapping<Account>>asList(
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_ID                      , Account::getId                   , Account::setId                   , Cursor::getLong   , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_USER_ID                 , Account::getUserId               , Account::setUserId               , Cursor::getLong   , ContentValues::put)
            ),
            Arrays.<FieldMapping<Account>>asList(
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_SERVER_ID               , Account::getServerId             , Account::setServerId             , Cursor::getString , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_ACCOUNT_STATE           , Account::getAccountState         , Account::setAccountState         , AccountState.class                    ),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_DEPLOYED_AT             , Account::getDeployedAt           , Account::setDeployedAt                                                   ),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_PAID_UNTIL              , Account::getPaidUntil            , Account::setPaidUntil                                                    ),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_RENEWAL_PERIOD_IN_MONTHS, Account::getRenewalPeriodInMonths, Account::setRenewalPeriodInMonths, Cursor::getInt    , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_BURST_STORAGE_IN_BYTES  , Account::getBurstStorageInBytes  , Account::setBurstStorageInBytes  , Cursor::getLong   , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_PLAN_NAME               , Account::getPlanName             , Account::setPlanName             , Cursor::getString , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_ACCOUNT_IP              , Account::getAccountIp            , Account::setAccountIp            , Cursor::getString , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_WEB_UI_URL              , Account::getWebUIURL             , Account::setWebUIURL             , Cursor::getString , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_LOGIN_PASSWORD          , Account::getLoginPassword        , Account::setLoginPassword        , Cursor::getString , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_SERVER_NAME             , Account::getServerName           , Account::setServerName           , Cursor::getString , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_TOTAL_BANDWIDTH_IN_BYTES, Account::getTotalBandwidthInBytes, Account::setTotalBandwidthInBytes, Cursor::getLong   , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_TOTAL_STORAGE_IN_BYTES  , Account::getTotalStorageInBytes  , Account::setTotalStorageInBytes  , Cursor::getLong   , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_ACCOUNT_TYPE            , Account::getAccountType          , Account::setAccountType          , AccountType.class                     ),
                FieldMapping.create(BytesizedContract.AccountEntitySpec.COLUMN_NAME_BOOST_BANDWIDTH_IN_BYTES, Account::getBoostBandwidthInBytes, Account::setBoostBandwidthInBytes, Cursor::getLong   , ContentValues::put)
            )
        );
    }

    @Override
    public List<Account> selectAllWithUserId(
        long userId
    ) {
        return this.selectAllWithParentId(
            BytesizedContract.AccountEntitySpec.COLUMN_NAME_USER_ID,
            userId
        );
    }

    @Override
    public List<Account> selectWithUserIdAndCriteria(
        long userId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectWithParentIdAndCriteria(
            BytesizedContract.AccountEntitySpec.COLUMN_NAME_USER_ID,
            userId,
            whereClause,
            whereParameters,
            sortClause
        );
    }

    @Override
    public Account selectSingleWithUserIdAndCriteria(
        long userId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectSingleWithParentIdAndCriteria(
            BytesizedContract.AccountEntitySpec.COLUMN_NAME_USER_ID,
            userId,
            whereClause,
            whereParameters,
            sortClause
        );
    }
}
