package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.List;

public abstract class QueryHandlerAdapter implements IQueryHandler {
    private String queryName;

    public QueryHandlerAdapter(
        String queryName
    ) {
        if (queryName == null) {
            throw new IllegalArgumentException("The query name must not be NULL.");
        }
        else {
            this.queryName = queryName;
        }
    }

    protected String getQueryName() {
        return this.queryName;
    }

    @Override
    public Cursor select(
        Context context,
        SQLiteDatabase readableDatabase,
        Uri uri,
        List<String> pathSegments,
        String[] projection,
        String selection,
        String[] selectionArgs,
        String sortOrder
    ) {
        throw new UnsupportedOperationException("Query does not support querying: " + this.queryName);
    }

    @Override
    public Uri insert(
        Context context,
        SQLiteDatabase writableDatabase,
        Uri uri,
        List<String> pathSegments,
        ContentValues values
    ) {
        throw new UnsupportedOperationException("Query does not support inserting: " + this.queryName);
    }

    @Override
    public int delete(
        Context context,
        SQLiteDatabase writableDatabase,
        Uri uri,
        List<String> pathSegments,
        String selection,
        String[] selectionArgs
    ) {
        throw new UnsupportedOperationException("Query does not support deleting: " + this.queryName);
    }

    @Override
    public int update(
        Context context,
        SQLiteDatabase writableDatabase,
        Uri uri,
        List<String> pathSegments,
        ContentValues values,
        String selection,
        String[] selectionArgs
    ) {
        throw new UnsupportedOperationException("Query does not support updating: " + this.queryName);
    }
}
