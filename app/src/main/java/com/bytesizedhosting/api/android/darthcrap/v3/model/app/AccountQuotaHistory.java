package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

import org.joda.time.DateTime;

public class AccountQuotaHistory implements IWithId {
    private Long id;
    private Long accountId;
    private DateTime takenAt;
    private Long storageQuotaInBytes;
    private Long bandwidthQuotaInBytes;

    public AccountQuotaHistory() {}

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(
        Long id
    ) {
        this.id = id;
    }

    public Long getAccountId() {
        return this.accountId;
    }

    public void setAccountId(
        Long accountId
    ) {
        this.accountId = accountId;
    }

    public DateTime getTakenAt() {
        return this.takenAt;
    }

    public void setTakenAt(
        DateTime takenAt
    ) {
        this.takenAt = takenAt;
    }

    public Long getStorageQuotaInBytes() {
        return this.storageQuotaInBytes;
    }

    public void setStorageQuotaInBytes(
        Long storageQuotaInBytes
    ) {
        this.storageQuotaInBytes = storageQuotaInBytes;
    }

    public Long getBandwidthQuotaInBytes() {
        return this.bandwidthQuotaInBytes;
    }

    public void setBandwidthQuotaInBytes(
        Long bandwidthQuotaInBytes
    ) {
        this.bandwidthQuotaInBytes = bandwidthQuotaInBytes;
    }
}
