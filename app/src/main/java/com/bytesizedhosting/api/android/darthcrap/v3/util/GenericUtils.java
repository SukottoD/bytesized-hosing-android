package com.bytesizedhosting.api.android.darthcrap.v3.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.GravatarDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.ContentResolverContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountType;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Gravatar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class GenericUtils {
    public static final int GRAVATAR_RADIUS_SHAPE_RECTANGLE = 0;
    public static final int GRAVATAR_RADIUS_SHAPE_ROUNDED_RECTANGLE = 10;
    public static final int GRAVATAR_RADIUS_SHAPE_CIRCLE = 50;

    private static final int BUFFER_SIZE = 4096;

    private static byte[] GRAVATAR_MM_IMAGE_DATA = null;

    private GenericUtils() {}

    public static byte[] readAsset(
        AssetManager assetManager,
        String path
    ) throws IOException {
        try (
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            InputStream seedGravatarInputStream = assetManager.open(path);
        ) {
            byte[] buffer = new byte[GenericUtils.BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = seedGravatarInputStream.read(buffer, 0, buffer.length)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
            }
            byteArrayOutputStream.flush();
            return byteArrayOutputStream.toByteArray();
        }
    }

    public static void applyGravatarToImageView(
        View view,
        long gravatarId,
        int radiusPercentage
    ) {
        if (view instanceof ImageView) {
            GenericUtils.applyGravatarToImageView((ImageView)view,gravatarId,radiusPercentage);
        }
        else {
            throw new IllegalArgumentException("The provided view is not an ImageView");
        }
    }

    public static void applyGravatarToImageView(
        ImageView imageView,
        long gravatarId,
        int radiusPercentage
    ) {
        LogUtils.d(GenericUtils.class,"Applying Gravatar " + gravatarId + " To Image View.");
        GravatarDao gravatarDao = new GravatarDao(ContentResolverContentProviderWrapper.create(imageView));
        Gravatar gravatar = gravatarDao.selectById(gravatarId);
        byte[] imageData;
        if (gravatar != null) {
            imageData = gravatar.getImageData();
        }
        else {
            if (GenericUtils.GRAVATAR_MM_IMAGE_DATA == null) {
                AssetManager assetManager = imageView.getContext().getAssets();
                try {
                    imageData = GenericUtils.readAsset(assetManager, "gravatars/default/mm.jpg");
                }
                catch (IOException exIO) {
                    imageData = null;
                }
            }
            else {
                imageData = GenericUtils.GRAVATAR_MM_IMAGE_DATA;
            }
        }
        if (imageData != null) {
            Bitmap image = BitmapFactory.decodeByteArray(imageData,0,imageData.length);
            Bitmap roundedImage = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(roundedImage);
            BitmapShader shader = new BitmapShader(image, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(shader);

            RectF rect = new RectF(0.0f, 0.0f, image.getWidth(), image.getHeight());
            int radius = (int)(image.getWidth()*(((double)radiusPercentage)/100.0f));
            canvas.drawRoundRect(rect, radius, radius, paint);

            DisplayMetrics metrics = imageView.getContext().getResources().getDisplayMetrics();

            float densityScale = (metrics.density/3.0f);
            int requiredWidth = (int)(densityScale*image.getWidth());
            int requiredHeight = (int)(densityScale*image.getHeight());

            Bitmap scaledImage = Bitmap.createScaledBitmap(roundedImage, requiredWidth, requiredHeight, true);

            imageView.setImageBitmap(scaledImage);
        }
        else {
            imageView.setImageResource(R.drawable.global_image_gravatar_mystery_man_circle);
        }
    }

    public static String getAccountTypeString(Context context, AccountType accountType) {
        int accountTypeNameStringResourceId = R.string.global_account_type_unknown;
        if (accountType != null) {
            switch (accountType) {
                case SHARED_ACCOUNT:
                    accountTypeNameStringResourceId = R.string.global_account_type_shared_account;
                    break;
                case STREAMBOX:
                    accountTypeNameStringResourceId = R.string.global_account_type_streambox;
                    break;
                case VPS_ACCOUNT:
                    accountTypeNameStringResourceId = R.string.global_account_type_vps_account;
                    break;
            }
        }
        return context.getString(accountTypeNameStringResourceId);
    }

    public static void addAllViews(View[] children, ViewGroup parent) {
        for (View child : children) {
            parent.addView(child);
        }
    }

    public static LinearLayout.LayoutParams setLinearLayoutParams(View childView) {
        return GenericUtils.setLinearLayoutParams(childView, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    public static LinearLayout.LayoutParams setLinearLayoutParams(View childView, int height) {
        return GenericUtils.setLinearLayoutParams(childView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    public static LinearLayout.LayoutParams setLinearLayoutParams(View childView, int width, int height) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
        childView.setLayoutParams(layoutParams);
        return layoutParams;
    }

    public static void openLink(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(intent);
    }
}
