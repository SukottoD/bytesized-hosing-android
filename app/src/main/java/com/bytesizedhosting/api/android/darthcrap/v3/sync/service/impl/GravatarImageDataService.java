package com.bytesizedhosting.api.android.darthcrap.v3.sync.service.impl;

import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.IGravatarImageDataService;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GravatarImageDataService implements IGravatarImageDataService {
    private static final int STATUS_NOT_FOUND = 404;
    private static final int STATUS_OK = 200;

    private static final int BUFFER_SIZE = 4096;

    private static final String URL_FORMAT_STRING = "https://www.gravatar.com/avatar/%1$s.png?s=%2$d&d=404&r=pg&_=%3$d";

    @Override
    public Map<String, byte[]> getBulkImageData(
        Collection<String> gravatarDigests,
        int wantedSize
    ) throws IOException {
        Set<String> uniqueGravatarDigests = new HashSet<>(gravatarDigests);
        Map<String,byte[]> gravatarData = new HashMap<>(uniqueGravatarDigests.size());
        for (String gravatarDigest : uniqueGravatarDigests) {
            gravatarData.put(gravatarDigest,this.getImageData(gravatarDigest,wantedSize));
        }
        return gravatarData;
    }

    @Override
    public byte[] getImageData(
        String gravatarDigest,
        int wantedSize
    ) throws IOException {
        String url = String.format(GravatarImageDataService.URL_FORMAT_STRING,gravatarDigest,wantedSize,System.currentTimeMillis());

        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        Integer statusCode = null;
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine != null) {
            statusCode = statusLine.getStatusCode();
        }
        if (statusCode != null && statusCode == GravatarImageDataService.STATUS_NOT_FOUND) {
            return null;
        }
        else if (statusCode != null && statusCode == GravatarImageDataService.STATUS_OK) {
            try (
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                InputStream gravatarInputStream = httpResponse.getEntity().getContent();
            ) {
                byte[] buffer = new byte[GravatarImageDataService.BUFFER_SIZE];
                int bytesRead;
                while ((bytesRead = gravatarInputStream.read(buffer, 0, buffer.length)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                }
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
        }
        else {
            throw new IOException("Unknown status code - " + statusCode);
        }
    }
}
