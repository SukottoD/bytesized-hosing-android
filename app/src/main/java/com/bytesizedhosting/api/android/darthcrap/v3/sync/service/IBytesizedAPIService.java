package com.bytesizedhosting.api.android.darthcrap.v3.sync.service;

import com.bytesizedhosting.api.android.darthcrap.v3.exceptions.ApiErrorException;
import com.bytesizedhosting.api.android.darthcrap.v3.model.external.ExternalUser;

import java.io.IOException;

public interface IBytesizedAPIService {
    boolean isAPIKeyCorrect(String apiKey) throws ApiErrorException, IOException;
    ExternalUser fetch(String apiKey) throws ApiErrorException, IOException;
}
