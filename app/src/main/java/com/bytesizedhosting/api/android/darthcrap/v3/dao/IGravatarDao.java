package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Gravatar;

import java.util.List;

public interface IGravatarDao extends IBasicDao<Gravatar> {
    public List<Gravatar> selectAllWithoutRetrievingImageData();

    public List<Gravatar> selectAllThatHaveNoImageData();

    public List<Gravatar> selectAllThatHaveNeverBeenFetched();
}
