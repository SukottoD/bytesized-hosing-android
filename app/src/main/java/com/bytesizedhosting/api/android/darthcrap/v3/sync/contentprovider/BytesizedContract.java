package com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider;

import android.net.Uri;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.IContentProviderDao;
import com.bytesizedhosting.api.android.darthcrap.v3.util.UriUtils;

// On an Android 4.4 Samsung Galaxy Note 2, if this was an interface and contained the *ySyncAdapterQueryStringParameter methods, I would get a VerifyError.
public class BytesizedContract {
    private BytesizedContract() {
        // Do nothing.
    }

    public static final String AUTHORITY = "com.bytesizedhosting.api.android.darthcrap.v3";

    public static final Uri CONTENT_URI = UriUtils.constructContentBaseUri(BytesizedContract.AUTHORITY);

    public static final String QUERY_STRING_IS_SYNC_ADAPTER_KEY   = "is_sync_adapter";
    public static final String QUERY_STRING_IS_SYNC_ADAPTER_VALUE = "true";

    public static <EntityType> void applySyncAdapterQueryStringParameter(IContentProviderDao<EntityType> dao) {
        dao.setQueryStringParameter(BytesizedContract.QUERY_STRING_IS_SYNC_ADAPTER_KEY, BytesizedContract.QUERY_STRING_IS_SYNC_ADAPTER_VALUE);
    }

    public static Uri applySyncAdapterQueryStringParameter(Uri uri) {
        return uri.buildUpon().appendQueryParameter(BytesizedContract.QUERY_STRING_IS_SYNC_ADAPTER_KEY,BytesizedContract.QUERY_STRING_IS_SYNC_ADAPTER_VALUE).build();
    }

    public static boolean hasSyncAdapterQueryStringParameter(Uri uri) {
        return BytesizedContract.QUERY_STRING_IS_SYNC_ADAPTER_VALUE.equals(uri.getQueryParameter(BytesizedContract.QUERY_STRING_IS_SYNC_ADAPTER_KEY));
    }

    public static interface IdContainingEntityPartialSpec {
        public static final String COLUMN_NAME_ID = "id";
    }

    public static interface ServerIdContainingEntityPartialSpec extends IdContainingEntityPartialSpec {
        public static final String COLUMN_NAME_SERVER_ID = "server_id";
    }

    public static interface UserEntitySpec extends IdContainingEntityPartialSpec {
        public static final String PATH_ENTITY = "entities/users";

        public static final String TABLE_NAME = "users";

        public static final String COLUMN_NAME_API_KEY                   = "api_key";
        public static final String COLUMN_NAME_CREATED_AT                = "created_at";
        public static final String COLUMN_NAME_EMAIL_ADDRESS             = "email_address";
        public static final String COLUMN_NAME_USERNAME                  = "username";
        public static final String COLUMN_NAME_COUNTRY                   = "country";
        public static final String COLUMN_NAME_BALANCE_IN_CENTS          = "balance_in_cents";
        public static final String COLUMN_NAME_GRAVATAR_ID               = "gravatar_id";
        public static final String COLUMN_NAME_LAST_API_DATA_UPDATE_TIME = "last_api_data_update_time";
    }

    public static interface AccountEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY = "entities/users/accounts";

        public static final String TABLE_NAME = "accounts";

        public static final String COLUMN_NAME_USER_ID                   = "user_id";
        public static final String COLUMN_NAME_ACCOUNT_STATE             = "account_state";
        public static final String COLUMN_NAME_DEPLOYED_AT               = "deployed_at";
        public static final String COLUMN_NAME_PAID_UNTIL                = "paid_until";
        public static final String COLUMN_NAME_RENEWAL_PERIOD_IN_MONTHS  = "renewal_period_in_months";
        public static final String COLUMN_NAME_BURST_STORAGE_IN_BYTES    = "burst_storage_in_bytes";
        public static final String COLUMN_NAME_PLAN_NAME                 = "plan_name";
        public static final String COLUMN_NAME_ACCOUNT_IP                = "account_ip";
        public static final String COLUMN_NAME_WEB_UI_URL                = "web_ui_url";
        public static final String COLUMN_NAME_LOGIN_PASSWORD            = "login_password";
        public static final String COLUMN_NAME_SERVER_NAME               = "server_name";
        public static final String COLUMN_NAME_TOTAL_BANDWIDTH_IN_BYTES  = "total_bandwidth_in_bytes";
        public static final String COLUMN_NAME_TOTAL_STORAGE_IN_BYTES    = "total_storage_in_bytes";
        public static final String COLUMN_NAME_ACCOUNT_TYPE              = "account_type";
        public static final String COLUMN_NAME_BOOST_BANDWIDTH_IN_BYTES  = "boost_bandwidth_in_bytes";
    }

    public static interface AccountUserAppEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY = "entities/users/accounts/user-apps";

        public static final String TABLE_NAME = "account_user_apps";

        public static final String COLUMN_NAME_ACCOUNT_ID                = "account_id";
        public static final String COLUMN_NAME_APP_SERVER_ID             = "app_server_id";
        public static final String COLUMN_NAME_ACCOUNT_USER_APP_STATE    = "account_user_app_state";
        public static final String COLUMN_NAME_APP_NAME                  = "app_name";
    }

    public static interface AccountUserAppOptionEntitySpec extends IdContainingEntityPartialSpec {
        public static final String PATH_ENTITY = "entities/users/accounts/user-apps/options";

        public static final String TABLE_NAME = "account_user_app_options";

        public static final String COLUMN_NAME_ACCOUNT_USER_APP_ID       = "account_user_app_id";
        public static final String COLUMN_NAME_NAME                      = "name";
        public static final String COLUMN_NAME_VALUE                     = "value";
    }

    public static interface AccountQuotaHistoryEntitySpec extends IdContainingEntityPartialSpec {
        public static final String PATH_ENTITY = "entities/users/accounts/quota-history";

        public static final String PATH_QUERY_SELECT_CURRENT_QUOTA_FOR_ACCOUNT = "queries/users/accounts/#/quota-history/select_current_quota";

        public static final String TABLE_NAME = "account_quota_history";

        public static final String COLUMN_NAME_ACCOUNT_ID                = "account_id";
        public static final String COLUMN_NAME_TAKEN_AT                  = "taken_at";
        public static final String COLUMN_NAME_STORAGE_QUOTA_IN_BYTES    = "storage_quota_in_bytes";
        public static final String COLUMN_NAME_BANDWIDTH_QUOTA_IN_BYTES  = "bandwidth_quota_in_bytes";
    }

    public static interface GravatarEntitySpec extends IdContainingEntityPartialSpec {
        public static final String PATH_ENTITY = "entities/gravatars";

        public static final String TABLE_NAME = "gravatars";

        public static final String COLUMN_NAME_GRAVATAR_DIGEST           = "gravatar_digest";
        public static final String COLUMN_NAME_LAST_FETCHED_AT           = "last_fetched_at";
        public static final String COLUMN_NAME_IMAGE_DATA                = "image_data";
    }

    /*
    public static interface TicketEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/tickets";

        public static final String TABLE_NAME = "tickets";

        public static final String COLUMN_NAME_IS_CLOSED                 = "is_closed";
        public static final String COLUMN_NAME_IS_PRIVATE                = "is_private";
        public static final String COLUMN_NAME_TITLE                     = "title";
    }

    public static interface TicketPostEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/tickets/posts";

        public static final String TABLE_NAME = "ticket_posts";

        public static final String COLUMN_NAME_TICKET_ID                 = "ticket_id";
        public static final String COLUMN_NAME_ORDER_NUMBER              = "order_number";
        public static final String COLUMN_NAME_BODY                      = "body";
        public static final String COLUMN_NAME_CREATED_AT                = "created_at";
        public static final String COLUMN_NAME_UPDATED_AT                = "updated_at";
        public static final String COLUMN_NAME_USERNAME                  = "username";
        public static final String COLUMN_NAME_GRAVATAR_ID               = "gravatar_id";
    }

    public static interface TicketVisibilityEntitySpec extends IdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/users/ticket_visibility";

        public static final String TABLE_NAME = "ticket_visibility";

        public static final String COLUMN_NAME_USER_ID                   = "user_id";
        public static final String COLUMN_NAME_TICKET_ID                 = "ticket_id";
    }

    public static interface NewsEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/news";

        public static final String TABLE_NAME = "news";

        public static final String COLUMN_NAME_BODY                      = "body";
        public static final String COLUMN_NAME_CREATED_AT                = "created_at";
        public static final String COLUMN_NAME_IS_IMPORTANT              = "is_important";
        public static final String COLUMN_NAME_TITLE                     = "title";
        public static final String COLUMN_NAME_UPDATED_AT                = "updated_at";
    }

    public static interface NewsCommentEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/news/comments";

        public static final String TABLE_NAME = "news_comments";

        public static final String COLUMN_NAME_NEWS_ID                   = "news_id";
        public static final String COLUMN_NAME_BODY                      = "body";
        public static final String COLUMN_NAME_CREATED_AT                = "created_at";
        public static final String COLUMN_NAME_UPDATED_AT                = "updated_at";
        public static final String COLUMN_NAME_USERNAME                  = "username";
        public static final String COLUMN_NAME_GRAVATAR_ID               = "gravatar_id";
    }

    public static interface InvoiceEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/users/invoices";

        public static final String TABLE_NAME = "invoices";

        public static final String COLUMN_NAME_USER_ID                   = "user_id";
        public static final String COLUMN_NAME_CREATED_AT                = "created_at";
        public static final String COLUMN_NAME_DUE_AT                    = "due_at";
        public static final String COLUMN_NAME_IS_PAID                   = "is_paid";
        public static final String COLUMN_NAME_PAID_AT                   = "paid_at";
        public static final String COLUMN_NAME_IS_PRO_FORMA              = "is_pro_forma";
        public static final String COLUMN_NAME_TAX_AMOUNT_IN_CENTS       = "tax_amount_in_cents";
        public static final String COLUMN_NAME_AMOUNT_IN_CENTS           = "amount_in_cents";
        public static final String COLUMN_NAME_NUMBER                    = "number";
        public static final String COLUMN_NAME_DESCRIPTION               = "description";
    }

    public static interface InvoiceLineEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/users/invoices/invoice-lines";

        public static final String TABLE_NAME = "invoice_lines";

        public static final String COLUMN_NAME_INVOICE_ID                = "invoice_id";
        public static final String COLUMN_NAME_BODY                      = "body";
        public static final String COLUMN_NAME_INCREASE_IN_CENTS         = "increase_in_cents";
    }

    public static interface TransactionEntitySpec extends ServerIdContainingEntityPartialSpec {
        public static final String PATH_ENTITY  = "entities/users/transactions";

        public static final String TABLE_NAME = "transactions";

        public static final String COLUMN_NAME_USER_ID                   = "user_id";
        public static final String COLUMN_NAME_CREATED_AT                = "created_at";
        public static final String COLUMN_NAME_EXTERNAL_ID               = "external_id";
        public static final String COLUMN_NAME_FRIENDLY_TYPE             = "friendly_type";
        public static final String COLUMN_NAME_CREDIT_IN_CENTS           = "credit_in_cents";
        public static final String COLUMN_NAME_DEBIT_IN_CENTS            = "debit_in_cents";
    }
    */
}
