package com.bytesizedhosting.api.android.darthcrap.v3.sync.service;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public interface IGravatarImageDataService {
    Map<String,byte[]> getBulkImageData(
        Collection<String> gravatarDigests,
        int wantedSize
    ) throws IOException;

    byte[] getImageData(
        String gravatarDigest,
        int wantedSize
    ) throws IOException;
}
