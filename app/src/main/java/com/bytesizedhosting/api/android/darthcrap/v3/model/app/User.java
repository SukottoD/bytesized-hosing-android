package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

import org.joda.time.DateTime;

public class User implements IWithId {
    private Long id;
    private String apiKey;
    private DateTime createdAt;
    private String emailAddress;
    private String username;
    private String country;
    private Integer balanceInCents;
    private Long gravatarId;
    private DateTime lastApiDataUpdateTime;

    public User() {}

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(
        Long id
    ) {
        this.id = id;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(
        String apiKey
    ) {
        this.apiKey = apiKey;
    }

    public DateTime getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(
        DateTime createdAt
    ) {
        this.createdAt = createdAt;
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public void setEmailAddress(
        String emailAddress
    ) {
        this.emailAddress = emailAddress;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(
        String username
    ) {
        this.username = username;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(
        String country
    ) {
        this.country = country;
    }

    public Integer getBalanceInCents() {
        return this.balanceInCents;
    }

    public void setBalanceInCents(
        Integer balanceInCents
    ) {
        this.balanceInCents = balanceInCents;
    }

    public Long getGravatarId() {
        return this.gravatarId;
    }

    public void setGravatarId(
        Long gravatarId
    ) {
        this.gravatarId = gravatarId;
    }

    public DateTime getLastApiDataUpdateTime() {
        return this.lastApiDataUpdateTime;
    }

    public void setLastApiDataUpdateTime(
        DateTime lastApiDataUpdateTime
    ) {
        this.lastApiDataUpdateTime = lastApiDataUpdateTime;
    }
}
