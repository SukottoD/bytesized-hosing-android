package com.bytesizedhosting.api.android.darthcrap.v3.sync.authenticator;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.ui.activities.ApiKeyManagementActivity;

public class BytesizedAuthenticator extends AbstractAccountAuthenticator {
    private Context context;

    private final Handler handler = new Handler();

    public BytesizedAuthenticator(
        Context context
    ) {
        super(context);
        this.context = context;
    }

    public static String getAccountName(
        Context context
    ) {
        return context.getString(R.string.const_authenticator_account_name);
    }

    private String getAccountName() {
        return BytesizedAuthenticator.getAccountName(this.context);
    }

    public static String getAccountType(
        Context context
    ) {
        return context.getString(R.string.const_authenticator_account_type);
    }

    private String getAccountType() {
        return BytesizedAuthenticator.getAccountType(this.context);
    }

    public static String getApiKeyAuthTokenType(
        Context context
    ) {
        return context.getString(R.string.const_authenticator_account_auth_token_type_api_key);
    }

    private String getApiKeyAuthTokenType() {
        return BytesizedAuthenticator.getApiKeyAuthTokenType(this.context);
    }

    @Override
    public Bundle addAccount(
        AccountAuthenticatorResponse response,
        String accountType,
        String authTokenType,
        String[] requiredFeatures,
        Bundle options
    ) throws NetworkErrorException {
        if (accountType == null || accountType.equals(this.getAccountType())) {
            if (authTokenType == null || authTokenType.equals(this.getApiKeyAuthTokenType())) {
                if (this.checkIfAccountAlreadyExists()) {
                    Bundle errorResult = this.createErrorResult(R.string.service_authenticator_error_an_account_already_exists);
                    handler.post(() -> Toast.makeText(BytesizedAuthenticator.this.context, errorResult.getString(AccountManager.KEY_ERROR_MESSAGE), Toast.LENGTH_LONG).show());
                    return errorResult;
                }
                else {
                    return this.openApiKeyUI(this.getAccountName(), accountType, authTokenType, null, true, response);
                }
            }
            else {
                return this.createErrorResult(R.string.service_authenticator_error_unknown_auth_token_type, authTokenType);
            }
        }
        else {
            return this.createErrorResult(R.string.service_authenticator_error_unknown_account_type, accountType);
        }
    }

    private Bundle openApiKeyUI(
        String accountName,
        String accountType,
        String authTokenType,
        String knownApiKey,
        boolean isForNewAccount,
        AccountAuthenticatorResponse response
    ) {
        if (authTokenType == null) {
            authTokenType = this.getApiKeyAuthTokenType();
        }
        Intent intent = new Intent(this.context, ApiKeyManagementActivity.class);
        intent.putExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_API_KEY, knownApiKey);
        intent.putExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_ACCOUNT_NAME, accountName);
        intent.putExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_ACCOUNT_TYPE, accountType);
        intent.putExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_AUTH_TOKEN_TYPE, authTokenType);
        intent.putExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_IS_ADDING_NEW_ACCOUNT, isForNewAccount);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT,intent);
        return bundle;
    }

    private Bundle createErrorResult(
        int errorStringResourceId,
        Object... errorStringFormatArgs
    ) {
        Bundle bundle = new Bundle();
        bundle.putInt(AccountManager.KEY_ERROR_CODE,errorStringResourceId);
        bundle.putString(AccountManager.KEY_ERROR_MESSAGE,this.context.getString(errorStringResourceId,(Object[])errorStringFormatArgs));
        return bundle;
    }


    private boolean checkIfAccountAlreadyExists() {
        AccountManager accountManager = AccountManager.get(this.context);
        Account[] accounts = accountManager.getAccountsByType(this.getAccountType());
        return (accounts != null && accounts.length >= 1);
    }

    private Bundle createTokenResult(Account account, String apiKey) {
        Bundle bundle = new Bundle();
        bundle.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
        bundle.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
        bundle.putString(AccountManager.KEY_AUTHTOKEN, apiKey);
        return bundle;
    }

    @Override
    public Bundle getAuthToken(
        AccountAuthenticatorResponse response,
        Account account,
        String authTokenType,
        Bundle options
    ) throws NetworkErrorException {
        if (authTokenType.equals(this.getApiKeyAuthTokenType())) {
            // The API key IS the password, so the standard peekAuthToken mechanism is not required.
            AccountManager accountManager = AccountManager.get(this.context);
            String password = accountManager.getPassword(account);
            if (TextUtils.isEmpty(password)) {
                return this.openApiKeyUI(account.name, account.type, authTokenType, null, false, response);
            }
            else {
                return this.createTokenResult(account, password);
            }
        }
        else {
            return this.createErrorResult(R.string.service_authenticator_error_unknown_auth_token_type,authTokenType);
        }
    }

    @Override
    public String getAuthTokenLabel(
        String authTokenType
    ) {
        String authTokenLabel = null;
        if (authTokenType != null) {
            if (authTokenType.equals(this.getApiKeyAuthTokenType())) {
                authTokenLabel = this.context.getString(R.string.service_authenticator_auth_token_type_label_api_key);
            }
        }
        if (authTokenLabel == null) {
            authTokenLabel = this.context.getString(R.string.service_authenticator_auth_token_type_label_unknown,authTokenType);
        }
        return authTokenLabel;
    }

    @Override
    public Bundle updateCredentials(
        AccountAuthenticatorResponse response,
        Account account,
        String authTokenType,
        Bundle options
    ) throws NetworkErrorException {
        if (authTokenType == null || authTokenType.equals(this.getApiKeyAuthTokenType())) {
            AccountManager accountManager = AccountManager.get(this.context);
            String password = accountManager.getPassword(account);
            return this.openApiKeyUI(account.name, account.type, authTokenType, password, false, response);
        }
        else {
            return this.createErrorResult(R.string.service_authenticator_error_unknown_auth_token_type, authTokenType);
        }
    }

    @Override
    public Bundle hasFeatures(
        AccountAuthenticatorResponse response,
        Account account,
        String[] features
    ) throws NetworkErrorException {
        Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT,false);
        return result;
    }

    @Override
    public Bundle confirmCredentials(
        AccountAuthenticatorResponse response,
        Account account,
        Bundle options
    ) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle editProperties(
        AccountAuthenticatorResponse response,
        String accountType
    ) {
        return null;
    }
}
