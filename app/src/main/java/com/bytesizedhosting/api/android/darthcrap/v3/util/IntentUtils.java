package com.bytesizedhosting.api.android.darthcrap.v3.util;

import android.content.Intent;

public class IntentUtils {
    public static final String INTENT_SYNC_STARTED = "com.bytesizedhosting.api.android.darthcrap.v3.intents.SYNC_STARTED";
    public static final String INTENT_SYNC_COMPLETED = "com.bytesizedhosting.api.android.darthcrap.v3.intents.SYNC_COMPLETED";
    public static final String INTENT_SYNC_COMPLETE_EXTRA_WAS_SUCCESSFUL = "was_successful";

    public static Intent createSyncStartedIntent() {
        return new Intent(IntentUtils.INTENT_SYNC_STARTED);
    }

    public static Intent createSyncCompletedIntent(boolean wasSuccessful) {
        Intent intent = new Intent(IntentUtils.INTENT_SYNC_COMPLETED);
        intent.putExtra(IntentUtils.INTENT_SYNC_COMPLETE_EXTRA_WAS_SUCCESSFUL,wasSuccessful);
        return intent;
    }
}
