package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

public enum AccountType {
    STREAMBOX,
    SHARED_ACCOUNT,
    VPS_ACCOUNT,
    UNKNOWN
}
