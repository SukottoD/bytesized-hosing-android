package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.IContentProviderDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.FieldMapping;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.IWithId;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;
import com.bytesizedhosting.api.android.darthcrap.v3.util.UriUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractContentProviderDao<EntityType extends IWithId> implements IContentProviderDao<EntityType> {
    //^(.+?) +(.+?) +(.+?) +(.+?);$
    //FieldMapping.create\\(BytesizedContract.\\1EntitySpec.COLUMN_NAME_\\U\\4\\E, \\1::get\\u\\4\\E, \\1::set\\u\\4\\E, Cursor::get\\3, ContentValues::put\\),

    public static interface IEntityConstructor<EntityType> {
        public EntityType constructEntity();
    }

    private IContentProviderWrapper contentProviderWrapper;
    private IEntityConstructor<EntityType> entityConstructor;
    private String pathForEntityList;
    private String pathForEntityById;
    private List<FieldMapping<EntityType>> fieldMappingsForEquality;
    private List<FieldMapping<EntityType>> fieldMappings;
    private String[] projection;
    private Map<String,String> queryStringSections;

    protected AbstractContentProviderDao(
        IContentProviderWrapper contentProviderWrapper,
        IEntityConstructor<EntityType> entityConstructor,
        String pathForEntity,
        List<FieldMapping<EntityType>> fieldMappingsNotForEquality,
        List<FieldMapping<EntityType>> fieldMappingsForEquality
    ) {
        if (contentProviderWrapper == null) {
            throw new IllegalArgumentException("The content provider wrapper must not be NULL.");
        }
        else if (entityConstructor == null) {
            throw new IllegalArgumentException("The entity constructor must not be NULL.");
        }
        else if (pathForEntity == null) {
            throw new IllegalArgumentException("The entity-list path must not be NULL.");
        }
        else if (fieldMappingsForEquality == null) {
            throw new IllegalArgumentException("The field mappings for equality list must not be NULL.");
        }
        else if (fieldMappingsNotForEquality == null) {
            throw new IllegalArgumentException("The field mappings not for equality list must not be NULL.");
        }
        else if ((fieldMappingsForEquality.size() + fieldMappingsNotForEquality.size()) < 1) {
            throw new IllegalArgumentException("There must be at least one field mapping.");
        }
        else {
            UriUtils.checkIfPathForEntityIsValid(pathForEntity);
            Set<String> fieldMappingColumnNames = new HashSet<>();
            for (FieldMapping<EntityType> fieldMappingForEquality : fieldMappingsForEquality) {
                fieldMappingColumnNames.add(fieldMappingForEquality.getFieldName());
            }
            for (FieldMapping<EntityType> fieldMappingNotForEquality : fieldMappingsNotForEquality) {
                String columnName = fieldMappingNotForEquality.getFieldName();
                if (!fieldMappingColumnNames.add(columnName)) {
                    throw new IllegalArgumentException("Duplicate field mapping column: " + columnName);
                }
            }
            this.pathForEntityList = pathForEntity;
            this.pathForEntityById = pathForEntity + "/#";
            this.contentProviderWrapper = contentProviderWrapper;
            this.entityConstructor = entityConstructor;
            this.fieldMappings = new ArrayList<>(fieldMappingsForEquality.size() + fieldMappingsNotForEquality.size());
            this.fieldMappingsForEquality = new ArrayList<>(fieldMappingsForEquality.size());
            this.fieldMappingsForEquality.addAll(fieldMappingsForEquality);
            this.fieldMappings.addAll(fieldMappingsForEquality);
            this.fieldMappings.addAll(fieldMappingsNotForEquality);
            this.projection = new String[this.fieldMappings.size()];
            for (int index = 0; index < this.projection.length; index++) {
                this.projection[index] = this.fieldMappings.get(index).getFieldName();
            }
            this.queryStringSections = new HashMap<>();
        }
    }

    protected Uri getPathUriForList() {
        return
            this.applyQuerySections(
                UriUtils.constructContentUri(
                    BytesizedContract.CONTENT_URI,
                    this.pathForEntityList
                )
            );
    }

    protected Uri getPathUriForById(
        long entityId
    ) {
        return
            this.applyQuerySections(
                UriUtils.constructContentUri(
                    BytesizedContract.CONTENT_URI,
                    this.pathForEntityById,
                    entityId
                )
            );
    }

    @Override
    public List<EntityType> selectAll() {
        return this.createEntityListFromCursor(
            this.contentProviderWrapper.query(
                this.getPathUriForList(),
                this.projection,
                null,
                null,
                null
            )
        );
    }

    protected List<EntityType> selectAllWithParentId(
        String parentFieldName,
        long parentId
    ) {
        return this.selectWithCriteria(
            this.augmentWhereClauseWithParent(null,parentFieldName,parentId),
            null,
            null
        );
    }

    private String augmentWhereClauseWithParent(String selection, String parentFieldName, long parentId) {
        String newPart = parentFieldName + " = " + parentId;
        if (selection == null) {
            return newPart;
        }
        else {
            return newPart + " AND (" + selection + ")";
        }
    }


    @Override
    public List<EntityType> selectWithCriteria(
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.createEntityListFromCursor(
            this.contentProviderWrapper.query(
                this.getPathUriForList(),
                this.projection,
                whereClause,
                whereParameters,
                sortClause
            )
        );
    }

    protected List<EntityType> selectWithParentIdAndCriteria(
        String parentFieldName,
        long parentId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectWithCriteria(
            this.augmentWhereClauseWithParent(whereClause,parentFieldName,parentId),
            whereParameters,
            sortClause
        );
    }

    @Override
    public EntityType selectSingleWithCriteria(
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        List<EntityType> results = this.selectWithCriteria(whereClause,whereParameters,sortClause);
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() == 1) {
            return results.get(0);
        }
        else {
            throw new IllegalStateException("1 result expected, " + results.size() + " results returned.");
        }
    }

    protected EntityType selectSingleWithParentIdAndCriteria(
        String parentFieldName,
        long parentId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectSingleWithCriteria(
            this.augmentWhereClauseWithParent(whereClause, parentFieldName, parentId),
            whereParameters,
            sortClause
        );
    }

    @Override
    public EntityType selectById(
        long id
    ) {
        return this.createEntityFromCursorAndClose(
            this.contentProviderWrapper.query(
                this.getPathUriForById(
                    id
                ),
                this.projection,
                null,
                null,
                null
            )
        );
    }

    @Override
    public long insert(
        EntityType entity
    ) {
        Uri newItemUri = this.contentProviderWrapper.insert(
            this.getPathUriForList(),
            this.createContentValuesFromEntity(entity)
        );
        List<String> pathSegments = newItemUri.getPathSegments();
        return Long.parseLong(pathSegments.get(pathSegments.size()-1));
    }

    @Override
    public ContentProviderOperation.Builder insertOperation(
        EntityType entity
    ) {
        return ContentProviderOperation
            .newInsert(this.getPathUriForList())
            .withValues(this.createContentValuesFromEntity(entity));
    }

    @Override
    public boolean update(
        EntityType item
    ) {
        return this.update(
            item.getId(),
            item
        );
    }

    @Override
    public ContentProviderOperation.Builder updateOperation(
        EntityType entity
    ) {
        return this.updateOperation(
            entity.getId(),
            entity
        );
    }

    @Override
    public boolean update(
        long id,
        EntityType entity
    ) {
        return this.contentProviderWrapper.update(
            this.getPathUriForById(
                id
            ),
            this.createContentValuesFromEntity(entity),
            null,
            null
        ) >= 1;
    }

    @Override
    public ContentProviderOperation.Builder updateOperation(
        long id,
        EntityType entity
    ) {
        return ContentProviderOperation
            .newUpdate(this.getPathUriForById(id))
            .withValues(this.createContentValuesFromEntity(entity));
    }

    @Override
    public boolean delete(
        long id
    ) {
        return this.contentProviderWrapper.delete(
            this.getPathUriForById(
                id
            ),
            null,
            null
        ) >= 1;
    }

    @Override
    public ContentProviderOperation.Builder deleteOperation(
        long id
    ) {
        return ContentProviderOperation
            .newDelete(this.getPathUriForById(id));
    }

    @Override
    public boolean delete(
        EntityType item
    ) {
        return this.delete(
            item.getId()
        );
    }

    @Override
    public ContentProviderOperation.Builder deleteOperation(
        EntityType entity
    ) {
        return this.deleteOperation(entity.getId());
    }

    @Override
    public int deleteAll() {
        return this.contentProviderWrapper.delete(
            this.getPathUriForList(),
            null,
            null
        );
    }

    @Override
    public boolean checkIfExists(
        long id
    ) {
        return this.selectById(id) != null;
    }

    protected IContentProviderWrapper getContentProviderWrapper() {
        return this.contentProviderWrapper;
    }

    protected List<EntityType> createEntityListFromCursor(
        Cursor cursor
    ) {
        List<EntityType> results = new ArrayList<>();
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    results.add(this.createEntityFromCursor(cursor));
                }
            }
            finally {
                cursor.close();
            }
        }
        return results;
    }

    protected EntityType createEntityFromCursorAndClose(
        Cursor cursor
    ) {
        try {
            return this.createEntityFromCursor(cursor);
        }
        finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private EntityType createEntityFromCursor(
        Cursor cursor
    ) {
        if (cursor == null || cursor.getCount() == 0) {
            return null;
        }
        else {
            if (cursor.isBeforeFirst()) {
                cursor.moveToFirst();
            }
            EntityType entity = this.entityConstructor.constructEntity();
            for (FieldMapping<EntityType> fieldMapping : this.fieldMappings) {
                fieldMapping.readFieldFromCursor(entity, cursor);
            }
            return entity;
        }
    }

    protected ContentValues createContentValuesFromEntity(
        EntityType entity
    ) {
        ContentValues contentValues = new ContentValues();
        for (FieldMapping<EntityType> fieldMapping : this.fieldMappings) {
            fieldMapping.writeEntityFieldToContentValues(entity,contentValues);
        }
        return contentValues;
    }

    protected String[] getProjection() {
        String[] projectionCopy = new String[this.projection.length];
        System.arraycopy(this.projection, 0, projectionCopy, 0, projection.length);
        return projectionCopy;
    }

    protected String getPathForEntityList() {
        return this.pathForEntityList;
    }

    protected String getPathForEntityById() {
        return this.pathForEntityById;
    }

    @Override
    public String getQueryStringParameter(
        String key
    ) {
        return this.queryStringSections.get(key);
    }

    @Override
    public void setQueryStringParameter(
        String key,
        String value
    ) {
        this.queryStringSections.put(key,value);
    }

    @Override
    public Map<String,String> getQueryStringParameter() {
        return Collections.unmodifiableMap(this.queryStringSections);
    }

    @Override
    public void removeQueryStringParameter(
        String key
    ) {
        this.queryStringSections.remove(key);
    }

    public boolean isPartiallyEqual(
        EntityType first,
        EntityType second
    ) {
        boolean equal = true;
        for (FieldMapping<EntityType> fieldMapping : this.fieldMappingsForEquality) {
            Object firstValue = fieldMapping.getValueAsObject(first);
            Object secondValue = fieldMapping.getValueAsObject(second);
            if (firstValue == null) {
                equal = (secondValue == null);
            }
            else {
                equal = firstValue.equals(secondValue);
            }
            if (!equal) {
                break;
            }
        }
        return equal;
    }

    protected Uri applyQuerySections(
        Uri uri
    ) {
        if (this.queryStringSections.isEmpty()) {
            return uri;
        }
        else {
            Uri.Builder uriBuilder = uri.buildUpon();
            for (Map.Entry<String, String> queryStringSection : this.queryStringSections.entrySet()) {
                uriBuilder.appendQueryParameter(queryStringSection.getKey(), queryStringSection.getValue());
            }
            return uriBuilder.build();
        }
    }
}
