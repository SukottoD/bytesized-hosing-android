package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

public interface IContentProviderWrapper {
    public String getType(
        Uri uri
    );

    public Cursor query(
        Uri uri,
        String[] projection,
        String selection,
        String[] selectionArgs,
        String sortOrder
    );

    public Uri insert(
        Uri uri,
        ContentValues values
    );

    public int delete(
        Uri uri,
        String selection,
        String[] selectionArgs
    );

    public int update(
        Uri uri,
        ContentValues values,
        String selection,
        String[] selectionArgs
    );
}
