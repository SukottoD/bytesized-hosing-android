package com.bytesizedhosting.api.android.darthcrap.v3.util.size;

import com.bytesizedhosting.api.android.darthcrap.v3.R;

public enum SizeUnit {
    BYTE          (1, null                      , R.string.global_size_byte_standard_singular         , R.string.global_size_byte_standard_plural         , R.string.global_size_byte_abbreviation_singular         , R.string.global_size_byte_abbreviation_plural         ),
    KILOBYTE_1000 (   SizeUnitSystem. SI_1000, 1, R.string.global_size_kilobyte_1000_standard_singular, R.string.global_size_kilobyte_1000_standard_plural, R.string.global_size_kilobyte_1000_abbreviation_singular, R.string.global_size_kilobyte_1000_abbreviation_plural),
    MEGABYTE_1000 (   SizeUnitSystem. SI_1000, 2, R.string.global_size_megabyte_1000_standard_singular, R.string.global_size_megabyte_1000_standard_plural, R.string.global_size_megabyte_1000_abbreviation_singular, R.string.global_size_megabyte_1000_abbreviation_plural),
    GIGABYTE_1000 (   SizeUnitSystem. SI_1000, 3, R.string.global_size_gigabyte_1000_standard_singular, R.string.global_size_gigabyte_1000_standard_plural, R.string.global_size_gigabyte_1000_abbreviation_singular, R.string.global_size_gigabyte_1000_abbreviation_plural),
    TERABYTE_1000 (   SizeUnitSystem. SI_1000, 4, R.string.global_size_terabyte_1000_standard_singular, R.string.global_size_terabyte_1000_standard_plural, R.string.global_size_terabyte_1000_abbreviation_singular, R.string.global_size_terabyte_1000_abbreviation_plural),
    PETABYTE_1000 (   SizeUnitSystem. SI_1000, 5, R.string.global_size_petabyte_1000_standard_singular, R.string.global_size_petabyte_1000_standard_plural, R.string.global_size_petabyte_1000_abbreviation_singular, R.string.global_size_petabyte_1000_abbreviation_plural),
    EXABYTE_1000  (   SizeUnitSystem. SI_1000, 6, R.string.global_size_exabyte_1000_standard_singular,  R.string.global_size_exabyte_1000_standard_plural,  R.string.global_size_exabyte_1000_abbreviation_singular,  R.string.global_size_exabyte_1000_abbreviation_plural ),
    KIBIBYTE_1024 (   SizeUnitSystem.IEC_1024, 1, R.string.global_size_kibibyte_1024_standard_singular, R.string.global_size_kibibyte_1024_standard_plural, R.string.global_size_kibibyte_1024_abbreviation_singular, R.string.global_size_kibibyte_1024_abbreviation_plural),
    MEBIBYTE_1024 (   SizeUnitSystem.IEC_1024, 2, R.string.global_size_mebibyte_1024_standard_singular, R.string.global_size_mebibyte_1024_standard_plural, R.string.global_size_mebibyte_1024_abbreviation_singular, R.string.global_size_mebibyte_1024_abbreviation_plural),
    GIBIBYTE_1024 (   SizeUnitSystem.IEC_1024, 3, R.string.global_size_gibibyte_1024_standard_singular, R.string.global_size_gibibyte_1024_standard_plural, R.string.global_size_gibibyte_1024_abbreviation_singular, R.string.global_size_gibibyte_1024_abbreviation_plural),
    TEBIBYTE_1024 (   SizeUnitSystem.IEC_1024, 4, R.string.global_size_tebibyte_1024_standard_singular, R.string.global_size_tebibyte_1024_standard_plural, R.string.global_size_tebibyte_1024_abbreviation_singular, R.string.global_size_tebibyte_1024_abbreviation_plural),
    PEBIBYTE_1024 (   SizeUnitSystem.IEC_1024, 5, R.string.global_size_pebibyte_1024_standard_singular, R.string.global_size_pebibyte_1024_standard_plural, R.string.global_size_pebibyte_1024_abbreviation_singular, R.string.global_size_pebibyte_1024_abbreviation_plural),
    EXBIBYTE_1024 (   SizeUnitSystem.IEC_1024, 6, R.string.global_size_exbibyte_1024_standard_singular, R.string.global_size_exbibyte_1024_standard_plural, R.string.global_size_exbibyte_1024_abbreviation_singular, R.string.global_size_exbibyte_1024_abbreviation_plural);

    private long amountOfBytes;
    private SizeUnitSystem sizeUnitSystem;
    private int standardSingularStringResourceId;
    private int standardPluralStringResourceId;
    private int abbreviationSingularStringResourceId;
    private int abbreviationPluralStringResourceId;

    private SizeUnit(long amountOfBytes, SizeUnitSystem sizeUnitSystem, int standardSingularStringResourceId, int standardPluralStringResourceId, int abbreviationSingularStringResourceId, int abbreviationPluralStringResourceId) {
        this.amountOfBytes = amountOfBytes;
        this.sizeUnitSystem = sizeUnitSystem;
        this.standardSingularStringResourceId = standardSingularStringResourceId;
        this.standardPluralStringResourceId = standardPluralStringResourceId;
        this.abbreviationSingularStringResourceId = abbreviationSingularStringResourceId;
        this.abbreviationPluralStringResourceId = abbreviationPluralStringResourceId;
    }

    private SizeUnit(SizeUnitSystem sizeUnitSystem, int count, int standardSingularStringResourceId, int standardPluralStringResourceId, int abbreviationSingularStringResourceId, int abbreviationPluralStringResourceId) {
        this(SizeUnit.calculateAmountOfBytes(sizeUnitSystem, count), sizeUnitSystem, standardSingularStringResourceId, standardPluralStringResourceId, abbreviationSingularStringResourceId, abbreviationPluralStringResourceId);
    }

    private static long calculateAmountOfBytes(SizeUnitSystem sizeUnitSystem, int count) {
        long amountOfBytes = 1;
        for (int index=1; index<=count; index++) {
            amountOfBytes *= sizeUnitSystem.getBase();
        }
        return amountOfBytes;
    }

    public long getAmountOfBytes() {
        return this.amountOfBytes;
    }

    public SizeUnitSystem getSizeUnitSystem() {
        return this.sizeUnitSystem;
    }

    public int getStandardSingularStringResourceId() {
        return this.standardSingularStringResourceId;
    }

    public int getStandardPluralStringResourceId() {
        return this.standardPluralStringResourceId;
    }

    public int getAbbreviationSingularStringResourceId() {
        return this.abbreviationSingularStringResourceId;
    }

    public int getAbbreviationPluralStringResourceId() {
        return this.abbreviationPluralStringResourceId;
    }

    public Size of(int count) {
        return this.ofInner(count,this.sizeUnitSystem,null);
    }

    public Size of(int count, SizeUnitSystem sizeUnitSystem) {
        return this.ofInner(count,sizeUnitSystem,null);
    }

    public Size of(int count, double roundUpModifier) {
        return this.ofInner(count,this.sizeUnitSystem,roundUpModifier);
    }

    public Size of(int count, SizeUnitSystem sizeUnitSystem, double roundUpModifier) {
        return this.ofInner(count,sizeUnitSystem,roundUpModifier);
    }

    private Size ofInner(int count, SizeUnitSystem sizeUnitSystem, Double roundUpModifier) {
        if (this.sizeUnitSystem != null && sizeUnitSystem != null && this.sizeUnitSystem != sizeUnitSystem) {
            throw new IllegalArgumentException("Cannot convert between differing size unit systems");
        }
        else if (count < 0) {
            throw new IllegalArgumentException("The count must not be negative");
        }
        else {
            long amountOfBytesNew = this.amountOfBytes * count;
            if (roundUpModifier == null) {
                return Size.createFromBytes(amountOfBytesNew,sizeUnitSystem);
            }
            else {
                return Size.createFromBytes(amountOfBytesNew,sizeUnitSystem,roundUpModifier);
            }
        }
    }
}