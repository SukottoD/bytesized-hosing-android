package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import com.bytesizedhosting.api.android.darthcrap.v3.ui.activities.ICoordinationActivity;

public abstract class AbstractBaseFragment extends Fragment implements IHasFragmentMenuDetails {
    private ICoordinationActivity coordinationActivity;
    private final String fragmentMenuType;
    private final String[] requiredArgumentKeys;

    public AbstractBaseFragment(
        String fragmentMenuType,
        String... requiredArgumentKeys
    ) {
        if (fragmentMenuType == null) {
            throw new IllegalArgumentException("The fragment menu type must not be NULL.");
        }
        else if (requiredArgumentKeys == null) {
            throw new IllegalArgumentException("The required argument keys list must not be NULL (but it can be empty)");
        }
        else {
            this.fragmentMenuType = fragmentMenuType;
            this.requiredArgumentKeys = new String[requiredArgumentKeys.length];
            System.arraycopy(requiredArgumentKeys,0,this.requiredArgumentKeys,0,requiredArgumentKeys.length);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.requiredArgumentKeys.length > 0) {
            Bundle arguments = this.getArguments();
            if (arguments == null) {
                throw new IllegalArgumentException("No arguments have been provided, but " + this.requiredArgumentKeys.length + " argument(s) are required.");
            }
            else {
                for (String requiredArgumentKey : this.requiredArgumentKeys) {
                    if (!arguments.containsKey(requiredArgumentKey)) {
                        throw new IllegalArgumentException("The argument '" + requiredArgumentKey + "' is required.");
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(
        Activity activity
    ) {
        super.onAttach(activity);
        this.coordinationActivity = this.castActivity(activity,ICoordinationActivity.class);
    }

    protected <T> T castActivity(
        Activity activity,
        Class<T> clazz
    ) {
        try {
            return clazz.cast(activity);
        }
        catch (ClassCastException exCCE) {
            throw new IllegalArgumentException("The Activity must implement " + clazz.getSimpleName() + ".",exCCE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.coordinationActivity = null;
    }

    public ICoordinationActivity getCoordinationActivity() {
        return this.getActivityDependantField(this.coordinationActivity);
    }

    protected <T> T getActivityDependantField(
        T field
    ) {
        if (field == null) {
            throw new IllegalStateException("The fragment is not currently attached to an activity.");
        }
        else {
            return field;
        }
    }

    public Context getContext() {
        return this.getActivity();
    }

    public String getFragmentMenuType() {
        return this.fragmentMenuType;
    }
}
