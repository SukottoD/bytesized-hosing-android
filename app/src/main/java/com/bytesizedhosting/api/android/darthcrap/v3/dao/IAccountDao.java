package com.bytesizedhosting.api.android.darthcrap.v3.dao;

import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Account;

import java.util.List;

public interface IAccountDao extends IBasicDao<Account> {
    List<Account> selectAllWithUserId(
        long userId
    );

    List<Account> selectWithUserIdAndCriteria(
        long userId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );

    public Account selectSingleWithUserIdAndCriteria(
        long userId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    );
}
