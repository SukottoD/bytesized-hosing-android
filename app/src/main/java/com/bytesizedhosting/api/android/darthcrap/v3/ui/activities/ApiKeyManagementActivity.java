package com.bytesizedhosting.api.android.darthcrap.v3.ui.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.exceptions.ApiErrorException;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.IBytesizedAPIService;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.service.impl.BytesizedAPIService;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.syncadapter.BytesizedSyncAdapter;
import com.bytesizedhosting.api.android.darthcrap.v3.util.LogUtils;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

public class ApiKeyManagementActivity extends AccountAuthenticatorActivity {
    private static final String MARKET_URI = "market://details?id=la.droid.qr.priva";

    private static final Pattern PATTERN_API_KEY = Pattern.compile("^[0-9a-f]+$");
    private static final Pattern PATTERN_QR_CODE_EXPECTED_RESULT = Pattern.compile("^([0-9a-f]{40})([0-9a-f]{8})$");

    private static final int API_KEY_REQUIRED_LENGTH = 40;

    private static final int REQUEST_CODE_QR_QR_DROID = 1;

    public static final String INTENT_EXTRA_KEY_API_KEY = "api_key";
    public static final String INTENT_EXTRA_KEY_ACCOUNT_TYPE = "account_type";
    public static final String INTENT_EXTRA_KEY_AUTH_TOKEN_TYPE = "auth_token_type";
    public static final String INTENT_EXTRA_KEY_ACCOUNT_NAME = "account_name";
    public static final String INTENT_EXTRA_KEY_IS_ADDING_NEW_ACCOUNT = "is_adding_new_account";

    private static final String INTERNAL_INTENT_EXTRA_ERROR_MESSAGE = "error_message";
    private static final String INTERNAL_INTENT_EXTRA_ERROR_IS_LINKED_TO_API_KEY = "is_linked_to_api_key";

    private IBytesizedAPIService bytesizedAPIService = new BytesizedAPIService();

    private String accountName;
    private String accountType;
    private String authTokenType;
    private boolean isAddingNewAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.d(this, "Creating Activity");
        this.setContentView(R.layout.activity_api_key_management);

        ActionBar actionBar = this.getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.global_app_title);
        }

        this.findViewById(R.id.btnScanQR).setOnClickListener(btnScanQR -> ApiKeyManagementActivity.this.initiateQRCodeScan());
        this.findViewById(R.id.btnOK).setOnClickListener(btnOK -> ApiKeyManagementActivity.this.startApiKeyCheck());

        Intent intent = this.getIntent();
        if (intent == null) {
            throw new IllegalStateException("This activity MUST be called using an intent.");
        }
        else {
            this.accountName = intent.getStringExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_ACCOUNT_NAME);
            this.accountType = intent.getStringExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_ACCOUNT_TYPE);
            this.authTokenType = intent.getStringExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_AUTH_TOKEN_TYPE);
            this.isAddingNewAccount = intent.getBooleanExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_IS_ADDING_NEW_ACCOUNT, false);

            if (this.accountName == null) {
                throw new IllegalArgumentException("No account name was provided in the intent.");
            }
            else if (this.accountType == null) {
                throw new IllegalArgumentException("No account type was provided in the intent.");
            }
            else if (this.authTokenType == null) {
                throw new IllegalArgumentException("No auth token type was provided in the intent.");
            }
            else {
                String apiKey = intent.getStringExtra(ApiKeyManagementActivity.INTENT_EXTRA_KEY_API_KEY);
                if (apiKey != null) {
                    if (ApiKeyManagementActivity.PATTERN_API_KEY.matcher(apiKey).matches()) {
                        ((TextView)this.findViewById(R.id.txtApiKey)).setText(apiKey);
                    }
                }
            }
        }
    }

    private void displayErrorToast(String errorMessage) {
        Toast.makeText(this.getBaseContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(boolean show) {
        RelativeLayout rlForm = (RelativeLayout)this.findViewById(R.id.rlForm);
        RelativeLayout rlLoading = (RelativeLayout)this.findViewById(R.id.rlLoading);
        this.findViewById(R.id.btnOK).setEnabled(!show);
        this.findViewById(R.id.btnScanQR).setEnabled(!show);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimationLength = this.getResources().getInteger(android.R.integer.config_mediumAnimTime);
            rlForm.setVisibility(View.VISIBLE);
            rlForm.animate().setDuration(shortAnimationLength).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    rlForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            rlLoading.setVisibility(View.VISIBLE);
            rlLoading.animate().setDuration(shortAnimationLength).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    rlLoading.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
        else {
            rlForm.setVisibility(show ? View.GONE : View.VISIBLE);
            rlLoading.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    private void startApiKeyCheck() {
        LogUtils.d(this,"Starting API Key Check.");
        TextView txtApiKey = ((TextView)this.findViewById(R.id.txtApiKey));
        txtApiKey.setError(null);
        String apiKey = txtApiKey.getText().toString().trim();

        new AsyncTask<Void,Void,Intent>() {
            @Override
            protected void onPreExecute() {
                ApiKeyManagementActivity.this.showProgress(true);
            }

            @Override
            protected Intent doInBackground(Void... params) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                    // Delay enough that the animation completes.
                    int shortAnimationLength = ApiKeyManagementActivity.this.getResources().getInteger(android.R.integer.config_mediumAnimTime);
                    try {
                        Thread.sleep(shortAnimationLength);
                    }
                    catch (InterruptedException ignored) {}
                }
                if (TextUtils.isEmpty(apiKey)) {
                    return this.createErrorResultIntent(true, R.string.section_api_key_management_error_api_key_empty);
                }
                else if (!ApiKeyManagementActivity.PATTERN_API_KEY.matcher(apiKey).matches()) {
                    return this.createErrorResultIntent(true, R.string.section_api_key_management_error_api_key_does_not_match_pattern);
                }
                else if (apiKey.length() < ApiKeyManagementActivity.API_KEY_REQUIRED_LENGTH) {
                    return this.createErrorResultIntent(true, R.string.section_api_key_management_error_api_key_too_short);
                }
                else if (apiKey.length() > ApiKeyManagementActivity.API_KEY_REQUIRED_LENGTH) {
                    return this.createErrorResultIntent(true, R.string.section_api_key_management_error_api_key_too_long);
                }
                else {
                    try {
                        boolean apiKeyOK = ApiKeyManagementActivity.this.bytesizedAPIService.isAPIKeyCorrect(apiKey);
                        if (apiKeyOK) {
                            return new Intent();
                        }
                        else {
                            return this.createErrorResultIntent(true, R.string.section_api_key_management_error_incorrect_api_key);
                        }
                    }
                    catch (ApiErrorException exApiError) {
                        return this.createErrorResultIntent(false, R.string.section_api_key_management_error_api_error, exApiError.getExternalError().getErrorMessage());
                    }
                    catch (IOException exIO) {
                        return this.createErrorResultIntent(false, R.string.section_api_key_management_error_io_error);
                    }
                }
            }

            private Intent createErrorResultIntent(boolean isTiedToApiKey, int errorStringResourceId, Object... errorArgs) {
                Intent errorResultIntent = new Intent();
                errorResultIntent.putExtra(ApiKeyManagementActivity.INTERNAL_INTENT_EXTRA_ERROR_MESSAGE, ApiKeyManagementActivity.this.getString(errorStringResourceId,(Object[])errorArgs));
                errorResultIntent.putExtra(ApiKeyManagementActivity.INTERNAL_INTENT_EXTRA_ERROR_IS_LINKED_TO_API_KEY, isTiedToApiKey);
                return errorResultIntent;
            }

            @Override
            protected void onPostExecute(Intent resultIntent) {
                LogUtils.d(this,"Handling API Key Check Finish.");
                ApiKeyManagementActivity.this.showProgress(false);
                String errorMessage = resultIntent.getStringExtra(ApiKeyManagementActivity.INTERNAL_INTENT_EXTRA_ERROR_MESSAGE);
                if (errorMessage != null) {
                    if (resultIntent.getBooleanExtra(ApiKeyManagementActivity.INTERNAL_INTENT_EXTRA_ERROR_IS_LINKED_TO_API_KEY,true)) {
                        ((TextView)ApiKeyManagementActivity.this.findViewById(R.id.txtApiKey)).setError(errorMessage);
                    }
                    else {
                        ApiKeyManagementActivity.this.displayErrorToast(errorMessage);
                    }
                }
                else {
                    ApiKeyManagementActivity.this.handleKeyAccepted(apiKey);
                }
            }

            @Override
            protected void onCancelled() {
                LogUtils.d(this,"Cancelling API Key Check.");
                ApiKeyManagementActivity.this.showProgress(false);
            }
        }.execute();
    }

    private void handleKeyAccepted(String apiKey) {
        LogUtils.d(this,"Handling API Key Accepted");
        Bundle userData = new Bundle();

        AccountManager accountManager = AccountManager.get(this.getBaseContext());
        Account account = new Account(accountName,accountType);

        if (isAddingNewAccount) {
            accountManager.addAccountExplicitly(account, apiKey, userData);
            BytesizedSyncAdapter.initializeSyncForAccount(account);
        }
        else {
            accountManager.setPassword(account, apiKey);
        }
        accountManager.setAuthToken(account, authTokenType, apiKey);

        Intent resultIntent = new Intent();
        Bundle resultBundle = new Bundle();
        resultBundle.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
        resultBundle.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
        resultBundle.putString(AccountManager.KEY_AUTHTOKEN, apiKey);
        resultBundle.putBundle(AccountManager.KEY_USERDATA, userData);
        resultIntent.putExtras(resultBundle);

        this.setAccountAuthenticatorResult(resultBundle);
        this.setResult(Activity.RESULT_OK, resultIntent);
        this.finish();
    }

    private void initiateQRCodeScan() {
        LogUtils.d(this,"Initiating QR Code Scan");
        try {
            Intent qrIntent = new Intent("la.droid.qr.scan");
            qrIntent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            ApiKeyManagementActivity.this.startActivityForResult(qrIntent, ApiKeyManagementActivity.REQUEST_CODE_QR_QR_DROID);
        }
        catch (ActivityNotFoundException exANF2) {
            LogUtils.d(this,"QR Code Scan Activity Not Found");
            new AlertDialog.Builder(ApiKeyManagementActivity.this)
                .setCancelable(true)
                .setMessage(ApiKeyManagementActivity.this.getString(R.string.section_api_key_management_prompt_no_qr_code_app))
                .setNegativeButton(ApiKeyManagementActivity.this.getString(R.string.global_generic_no), (dialog, buttonId) -> dialog.cancel())
                .setPositiveButton(ApiKeyManagementActivity.this.getString(R.string.global_generic_yes), (dialog, buttonId) -> ApiKeyManagementActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApiKeyManagementActivity.MARKET_URI))))
                .create().show();
        }
    }

    @Override
    public void onActivityResult(
        int requestCode,
        int resultCode,
        Intent intent
    ) {
        super.onActivityResult(requestCode,resultCode,intent);
        switch (requestCode) {
            case ApiKeyManagementActivity.REQUEST_CODE_QR_QR_DROID:
                this.handleQRCodeFromQRDroid(resultCode, intent);
                break;
            default:
                // Do nothing - we don't care about this request code.
        }
    }

    private void handleQRCodeFromQRDroid(
        int resultCode,
        Intent intent
    ) {
        LogUtils.d(this,"Handling QR Droid Result With Result Code Set To " + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            this.handleQRCode(intent.getStringExtra("SCAN_RESULT"));
        }
        else if (resultCode != Activity.RESULT_CANCELED) {
            this.displayErrorToast(this.getString(R.string.section_api_key_management_error_qr_code_scan_failed));
        }
    }

    private void handleQRCode(
        String qrCodeContents
    ) {
        LogUtils.d(this,"Handling QR Code '" + qrCodeContents + "'");
        Matcher qrCodeMatcher = ApiKeyManagementActivity.PATTERN_QR_CODE_EXPECTED_RESULT.matcher(qrCodeContents);
        boolean isValid = false;
        if (qrCodeMatcher.matches()) {
            String apiKey = qrCodeMatcher.group(1).toLowerCase();
            CRC32 crc32ChecksumCreator = new CRC32();
            crc32ChecksumCreator.update(apiKey.getBytes());
            String crcOfApiKey = Long.toHexString(crc32ChecksumCreator.getValue()).toLowerCase();
            if (qrCodeMatcher.group(2).toLowerCase().equals(crcOfApiKey)) {
                isValid = true;
                TextView txtApiKey = (TextView)this.findViewById(R.id.txtApiKey);
                txtApiKey.setText(apiKey);
                txtApiKey.setError(null);
                this.displayErrorToast(this.getString(R.string.section_api_key_management_notification_qr_code_accepted));
            }
        }
        if (!isValid) {
            new AlertDialog.Builder(this)
                .setCancelable(true)
                .setMessage(this.getString(R.string.section_api_key_management_error_qr_code_rejected))
                .setNeutralButton(this.getString(R.string.global_generic_ok), (dialog, buttonId) -> dialog.cancel())
                .create().show();
        }
    }

    @Override
    public void onBackPressed() {
        LogUtils.d(this,"Cancelling Due To Back Button Pressed.");
        Intent result = new Intent();
        Bundle bundle = new Bundle();
        result.putExtras(bundle);

        this.setAccountAuthenticatorResult(null);
        this.setResult(RESULT_CANCELED, result);
        this.finish();
    }
}
