package com.bytesizedhosting.api.android.darthcrap.v3.ui.activities;

import android.app.Fragment;

public interface ICoordinationActivity {
    public void displayNewFragment(
        boolean boolReplaceBackStack,
        Fragment newFragment
    );

    public void requestApiDataUpdate(
        boolean forceNewSync
    );
}
