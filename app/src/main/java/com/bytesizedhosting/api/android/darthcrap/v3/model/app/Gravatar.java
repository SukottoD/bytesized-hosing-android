package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

import org.joda.time.DateTime;

public class Gravatar implements IWithId {
    private Long id;
    private String gravatarDigest;
    private DateTime lastFetchedAt;
    private byte[] imageData;

    public Gravatar() {}

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(
        Long id
    ) {
        this.id = id;
    }

    public String getGravatarDigest() {
        return this.gravatarDigest;
    }

    public void setGravatarDigest(
        String gravatarDigest
    ) {
        this.gravatarDigest = gravatarDigest;
    }

    public DateTime getLastFetchedAt() {
        return this.lastFetchedAt;
    }

    public void setLastFetchedAt(
        DateTime lastFetchedAt
    ) {
        this.lastFetchedAt = lastFetchedAt;
    }

    public byte[] getImageData() {
        return Gravatar.copyImageData(this.imageData);
    }

    public void setImageData(
        byte[] imageData
    ) {
        this.imageData = Gravatar.copyImageData(imageData);
    }

    private static byte[] copyImageData(byte[] imageData) {
        if (imageData == null) {
            return null;
        }
        else {
            byte[] imageDataCopy = new byte[imageData.length];
            System.arraycopy(imageData, 0, imageDataCopy, 0, imageData.length);
            return imageDataCopy;
        }
    }
}
