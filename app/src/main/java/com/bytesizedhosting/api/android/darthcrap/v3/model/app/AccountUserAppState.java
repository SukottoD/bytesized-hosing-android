package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

public enum AccountUserAppState {
    INSTALLING,
    INSTALLED,
    DELETED,
    UNKNOWN
}
