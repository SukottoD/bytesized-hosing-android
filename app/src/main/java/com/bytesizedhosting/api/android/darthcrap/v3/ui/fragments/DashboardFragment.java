package com.bytesizedhosting.api.android.darthcrap.v3.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bytesizedhosting.api.android.darthcrap.v3.R;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.AccountQuotaHistoryDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.UserDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.ContentResolverContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.Account;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountQuotaHistory;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountState;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountType;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.User;
import com.bytesizedhosting.api.android.darthcrap.v3.ui.controls.BarredPanelView;
import com.bytesizedhosting.api.android.darthcrap.v3.util.GenericUtils;
import com.bytesizedhosting.api.android.darthcrap.v3.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends AbstractBaseFragment {
    public static final String ARGUMENT_EXTRA_USER_ID = "user_id";
    public static final String FRAGMENT_MENU_TYPE = "DASHBOARD";

    public DashboardFragment() {
        super(
            DashboardFragment.FRAGMENT_MENU_TYPE,
            DashboardFragment.ARGUMENT_EXTRA_USER_ID
        );
    }

    public static DashboardFragment create(long userId) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle arguments = new Bundle();
        arguments.putLong(DashboardFragment.ARGUMENT_EXTRA_USER_ID,userId);
        fragment.setArguments(arguments);
        return fragment;
    }

    public static class AccountWithQuota {
        private final Account account;
        private final AccountQuotaHistory accountQuotaHistory;

        public AccountWithQuota(Account account, AccountQuotaHistory accountQuotaHistory) {
            this.account = account;
            this.accountQuotaHistory = accountQuotaHistory;
        }

        public Account getAccount() {
            return this.account;
        }

        public AccountQuotaHistory getAccountQuotaHistory() {
            return this.accountQuotaHistory;
        }
    }

    private class AccountSummaryAdapter extends RecyclerView.Adapter<AccountSummaryAdapter.ViewHolder> {
        public class ViewHolder extends RecyclerView.ViewHolder {
            private BarredPanelView bpvRoot;
            private TextView lblHeader;
            private TextView lblPlanName;
            private TextView lblDisabled;
            private ProgressBar pbStorage;
            private TextView lblStorage;
            private ViewGroup vgPressForMoreDetails;

            public ViewHolder(
                View rootView
            ) {
                super(rootView);
                this.bpvRoot = (BarredPanelView)rootView;
                this.lblHeader = (TextView)rootView.findViewById(R.id.lblHeader);
                this.lblPlanName = (TextView)rootView.findViewById(R.id.lblPlanName);
                this.lblDisabled = (TextView)rootView.findViewById(R.id.lblDisabled);
                this.pbStorage = (ProgressBar)rootView.findViewById(R.id.pbStorage);
                this.lblStorage = (TextView)rootView.findViewById(R.id.lblStorage);
                this.vgPressForMoreDetails = (ViewGroup)rootView.findViewById(R.id.flPressForMoreDetails);
            }
        }

        private List<AccountWithQuota> accountsWithQuota;
        private LayoutInflater layoutInflater;
        private Context context;

        public AccountSummaryAdapter(
            List<AccountWithQuota> accountsWithQuota
        ) {
            this.accountsWithQuota = new ArrayList<>(accountsWithQuota);
            this.context = DashboardFragment.this.getActivity();
            this.layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public AccountSummaryAdapter.ViewHolder onCreateViewHolder(
            ViewGroup parent,
            int viewType
        ) {
            return new ViewHolder(this.layoutInflater.inflate(R.layout.view_dashboard_account_summary,parent,false));
        }

        @SuppressLint("StringFormatMatches")
        @Override
        public void onBindViewHolder(
            ViewHolder viewHolder,
            int position
        ) {
            AccountWithQuota accountWithQuota = this.accountsWithQuota.get(position);
            Account account = accountWithQuota.getAccount();
            AccountQuotaHistory accountQuotaHistory = accountWithQuota.getAccountQuotaHistory();
            long currentStorageQuota = accountQuotaHistory.getStorageQuotaInBytes();
            Long totalStorage = account.getTotalStorageInBytes();
            double percentage;
            int storageStringResourceId;
            if (totalStorage == null) {
                percentage = 0;
                storageStringResourceId = R.string.section_dashboard_account_summary_storage_with_no_limit;
            }
            else {
                percentage = ((double)currentStorageQuota)/((double)totalStorage);
                storageStringResourceId = R.string.section_dashboard_account_summary_storage_with_limit;
            }
            if (percentage > 1) {
                percentage = 1;
            }
            double percentageForString = (percentage * 100);
            viewHolder.lblStorage.setText(this.context.getString(storageStringResourceId,percentageForString));
            AccountType accountType = account.getAccountType();
            int accountTypeBackgroundColourResourceId = R.color.section_dashboard_account_summary_background_account_type_unknown;
            if (accountType != null) {
                switch (accountType) {
                    case SHARED_ACCOUNT:
                        accountTypeBackgroundColourResourceId = R.color.section_dashboard_account_summary_background_account_type_shared_account;
                        break;
                    case STREAMBOX:
                        accountTypeBackgroundColourResourceId = R.color.section_dashboard_account_summary_background_account_type_streambox;
                        break;
                    case VPS_ACCOUNT:
                        accountTypeBackgroundColourResourceId = R.color.section_dashboard_account_summary_background_account_type_vps_account;
                        break;
                }
            }
            if (AccountState.DISABLED.equals(account.getAccountState())) {
                accountTypeBackgroundColourResourceId = R.color.section_dashboard_account_summary_background_disabled;
            }
            viewHolder.bpvRoot.setBackgroundColour(this.context.getResources().getColor(accountTypeBackgroundColourResourceId));
            viewHolder.lblHeader.setText(this.context.getString(R.string.section_dashboard_account_summary_header,account.getServerName(),GenericUtils.getAccountTypeString(context,account.getAccountType())));
            viewHolder.lblPlanName.setText(account.getPlanName());
            viewHolder.pbStorage.setMax(1000);
            viewHolder.pbStorage.setProgress((int)(percentage*1000));
            viewHolder.lblDisabled.setVisibility(AccountState.DISABLED.equals(account.getAccountState())?View.VISIBLE:View.GONE);
            viewHolder.vgPressForMoreDetails.setOnClickListener(
                view -> DashboardFragment.this.getCoordinationActivity().displayNewFragment(false,AccountDetailsFragment.create(account.getId()))
            );
        }

        @Override
        public int getItemCount() {
            return this.accountsWithQuota.size();
        }
    }

    @Override
    public View onCreateView(
        LayoutInflater layoutInflater,
        ViewGroup container,
        Bundle savedInstanceState
    ) {
        LogUtils.d(this, "Creating View");

        View dashboardView = layoutInflater.inflate(
            R.layout.fragment_dashboard,
            container,
            false
        );

        IContentProviderWrapper context = ContentResolverContentProviderWrapper.create(this);

        LinearLayout llMessageContainer = (LinearLayout)dashboardView.findViewById(R.id.llMessageContainer);
        RecyclerView rvBoxSummaries = (RecyclerView)dashboardView.findViewById(R.id.rvAccountSummaries);

        UserDao userDao = new UserDao(context);
        AccountDao accountDao = new AccountDao(context);
        AccountQuotaHistoryDao accountQuotaHistoryDao = new AccountQuotaHistoryDao(context);

        User user = userDao.selectById(this.getUserId());
        int storageSoftLimitWarningCount = 0;
        int storageHardLimitWarningCount = 0;
        int bandwidthWarningCount = 0;
        int disabledAccountWarningCount = 0;
        List<Account> accounts = accountDao.selectAllWithUserId(user.getId());
        List<AccountWithQuota> accountsWithQuota = new ArrayList<>();
        for (Account account : accounts) {
            AccountQuotaHistory accountQuotaHistory = accountQuotaHistoryDao.selectLatestQuotaForAccount(account.getId());
            long currentStorageQuota = accountQuotaHistory.getStorageQuotaInBytes();
            long currentBandwidthQuota = accountQuotaHistory.getBandwidthQuotaInBytes();
            Long totalStorage = account.getTotalStorageInBytes();
            Long totalBandwidth = account.getTotalBandwidthInBytes();
            Long boostBandwidth = account.getBoostBandwidthInBytes();
            if (totalStorage != null) {
                Long burstStorage = account.getBurstStorageInBytes();
                if (burstStorage != null && currentStorageQuota >= (totalStorage + burstStorage)) {
                    storageHardLimitWarningCount++;
                }
                else if (currentStorageQuota > totalStorage) {
                    storageSoftLimitWarningCount++;
                }
            }
            if (totalBandwidth != null || boostBandwidth != null) {
                long totalBandwidthIncludingBoost = 0;
                if (totalBandwidth != null) {
                    totalBandwidthIncludingBoost += totalBandwidth;
                }
                if (boostBandwidth != null) {
                    totalBandwidthIncludingBoost += boostBandwidth;
                }
                if (currentBandwidthQuota > totalBandwidthIncludingBoost) {
                    bandwidthWarningCount++;
                }
            }
            if (AccountState.DISABLED.equals(account.getAccountState())) {
                disabledAccountWarningCount++;
            }
            accountsWithQuota.add(new AccountWithQuota(account, accountQuotaHistory));
        }
        rvBoxSummaries.setHasFixedSize(false);
        rvBoxSummaries.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rvBoxSummaries.setAdapter(
            new AccountSummaryAdapter(accountsWithQuota)
        );
        if (user.getBalanceInCents() > 0) {
            this.addMessage(
                llMessageContainer,
                R.style.Bytesized_Extra_Widget_TextView_Alert_Warning,
                R.string.section_dashboard_message_balance
            );
        }
        this.addMessagePlural(
            llMessageContainer,
            R.style.Bytesized_Extra_Widget_TextView_Alert_Danger,
            R.plurals.section_dashboard_message_storage_hard_limit_count,
            storageHardLimitWarningCount
        );
        this.addMessagePlural(
            llMessageContainer,
            R.style.Bytesized_Extra_Widget_TextView_Alert_Warning,
            R.plurals.section_dashboard_message_storage_soft_limit_count,
            storageSoftLimitWarningCount
        );
        this.addMessagePlural(
            llMessageContainer,
            R.style.Bytesized_Extra_Widget_TextView_Alert_Warning,
            R.plurals.section_dashboard_message_bandwidth_limit_count,
            bandwidthWarningCount
        );
        this.addMessagePlural(
            llMessageContainer,
            R.style.Bytesized_Extra_Widget_TextView_Alert_Danger,
            R.plurals.section_dashboard_message_account_disabled_count,
            disabledAccountWarningCount
        );

        return dashboardView;
    }

    private void addMessage(
        LinearLayout llMessageContainer,
        int alertStyle,
        String message
    ) {
        TextView textView = new TextView(new ContextThemeWrapper(this.getActivity(),alertStyle));
        textView.setText(message);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, this.getResources().getDimensionPixelSize(R.dimen.section_dashboard_message_margin_bottom));
        textView.setLayoutParams(layoutParams);
        llMessageContainer.addView(textView);
    }

    private void addMessage(
        LinearLayout llMessageContainer,
        int alertStyle,
        int stringResourceId,
        Object... messageArguments
    ) {
        this.addMessage(
            llMessageContainer,
            alertStyle,
            this.getResources().getString(stringResourceId,messageArguments)
        );
    }

    private void addMessagePlural(
        LinearLayout llMessageContainer,
        int alertStyle,
        int pluralResourceId,
        int count,
        Object... messageArgumentsExcludingCount
    ) {
        if (count > 0) {
            if (messageArgumentsExcludingCount == null) {
                messageArgumentsExcludingCount = new Object[0];
            }
            Object[] messageArguments = new Object[messageArgumentsExcludingCount.length+1];
            messageArguments[0] = count;
            System.arraycopy(messageArgumentsExcludingCount,0,messageArguments,1,messageArgumentsExcludingCount.length);
            this.addMessage(
                llMessageContainer,
                alertStyle,
                this.getResources().getQuantityString(pluralResourceId,count,messageArguments)
            );
        }
    }

    private long getUserId() {
        return this.getArguments().getLong(DashboardFragment.ARGUMENT_EXTRA_USER_ID);
    }

    @Override
    public Object getFragmentMenuParameter() {
        return this.getUserId();
    }
}
