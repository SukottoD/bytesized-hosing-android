package com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl;

import android.content.ContentValues;
import android.database.Cursor;

import com.bytesizedhosting.api.android.darthcrap.v3.dao.IAccountUserAppDao;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.IContentProviderWrapper;
import com.bytesizedhosting.api.android.darthcrap.v3.dao.contentprovider.impl.util.FieldMapping;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserApp;
import com.bytesizedhosting.api.android.darthcrap.v3.model.app.AccountUserAppState;
import com.bytesizedhosting.api.android.darthcrap.v3.sync.contentprovider.BytesizedContract;

import java.util.Arrays;
import java.util.List;

public class AccountUserAppDao extends AbstractContentProviderDao<AccountUserApp> implements IAccountUserAppDao {
    public AccountUserAppDao(
        IContentProviderWrapper contentProviderWrapper
    ) {
        super(
            contentProviderWrapper,
            AccountUserApp::new,
            BytesizedContract.AccountUserAppEntitySpec.PATH_ENTITY,
            Arrays.<FieldMapping<AccountUserApp>>asList(
                FieldMapping.create(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ID                    , AccountUserApp::getId                 , AccountUserApp::setId                 , Cursor::getLong  , ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ACCOUNT_ID            , AccountUserApp::getAccountId          , AccountUserApp::setAccountId          , Cursor::getLong  , ContentValues::put)
            ),
            Arrays.<FieldMapping<AccountUserApp>>asList(
                FieldMapping.create(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_SERVER_ID             , AccountUserApp::getServerId           , AccountUserApp::setServerId           , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_APP_SERVER_ID         , AccountUserApp::getAppServerId        , AccountUserApp::setAppServerId        , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_APP_NAME              , AccountUserApp::getAppName            , AccountUserApp::setAppName            , Cursor::getString, ContentValues::put),
                FieldMapping.create(BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ACCOUNT_USER_APP_STATE, AccountUserApp::getAccountUserAppState, AccountUserApp::setAccountUserAppState, AccountUserAppState.class            )
            )
        );
    }

    @Override
    public List<AccountUserApp> selectAllWithAccountId(
        long accountId
    ) {
        return this.selectAllWithParentId(
            BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ACCOUNT_ID,
            accountId
        );
    }

    @Override
    public List<AccountUserApp> selectWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectWithParentIdAndCriteria(
            BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ACCOUNT_ID,
            accountId,
            whereClause,
            whereParameters,
            sortClause
        );
    }

    @Override
    public AccountUserApp selectSingleWithAccountIdAndCriteria(
        long accountId,
        String whereClause,
        String[] whereParameters,
        String sortClause
    ) {
        return this.selectSingleWithParentIdAndCriteria(
            BytesizedContract.AccountUserAppEntitySpec.COLUMN_NAME_ACCOUNT_ID,
            accountId,
            whereClause,
            whereParameters,
            sortClause
        );
    }
}
