package com.bytesizedhosting.api.android.darthcrap.v3.model.app;

public interface IWithServerId {
    public String getServerId();
}
